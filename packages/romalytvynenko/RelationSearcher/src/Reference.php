<?php

namespace Romalytvynenko\RelationSearcher;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;

/**
 * Class Reference
 * @package Romalytvynenko\RelationSearcher
 */
class Reference
{
    /**
     * @var Callable Reference parameters
     */
    private $closure;

    public function __construct(Callable $closure)
    {
        $this->closure = $closure;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function buildCollection(Builder $query, $data)
    {
        return call_user_func($this->closure, $this, $query, $data);
    }

    /**
     * Build concrete type query
     *
     * @param $type
     * @param Builder|JoinClause $query
     * @param $column
     * @param $data
     * @return $this|Builder
     * @throws \Exception
     */
    public function buildQuery($type, $query, $column, $data)
    {
        if(!in_array($type, ['number', 'string'])) {
            throw new \Exception('Unsupported refer type');
        }

        if($type == 'number') {
            if(strpos($data, ':') !== false) {

                list($min, $max) = explode(':', $data);

                return $query->where($column, '>=', $min)
                    ->where($column, '<=', $max);
            }
        }

        if(strpos($data, ',') !== false) {
            return $query->whereIn($column, explode(',', $data));
        }

        return $query->where($column, '=', $data);
    }

    /**
     * Build concrete type query
     *
     * @param $type
     * @param JoinClause $join
     * @param $column
     * @param $data
     * @throws \Exception
     */
    public function buildJoin($type, &$join, $column, $data)
    {
        $join = self::buildQuery($type, $join, $column, $data);
    }
}