<?php

namespace Romalytvynenko\RelationSearcher;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class Searcher
 * @package Romalytvynenko\RelationSearcher
 */
class Searcher
{
    /**
     * @var string Results of this model will be returned
     */
    private $modelName;

    /**
     * @var array Array of query modifiers before search
     */
    private $queryBuilders = [];

    /**
     * @var array Array of references to search in
     */
    private $references = [];

    /**
     * @param $modelName
     */
    public function __construct($modelName)
    {
        $this->modelName = $modelName;
    }

    public function addReference($field, Callable $closure)
    {
        $this->references[$field] = new Reference($closure);
    }

    public function buildQuery(Callable $closure)
    {
        $this->queryBuilders[] = $closure;
    }

    public function search($data, $perRage = null)
    {
        $model = $this->modelName;
        $results = (new $model)->newQuery();

        // define all rules that should be executed before search
        foreach($this->queryBuilders as $builder) {
            $results = $builder($results);
        }

        foreach ($data as $field => $value) {
            if(array_key_exists($field, $this->references) && strlen($value)) {
                /** @var  Reference $reference */
                $reference = $this->references[$field];
                $results = $reference->buildCollection($results, $value);
            }
        }

        $table = (new $model)->getTable();

        $count = false;
        if($perRage && $this->getPageNumber() == 1) {
            $count = DB::table( DB::raw("({$results->select(DB::raw('DISTINCT('.$table . '.id) as id'))->toSql()}) as sub") )
                ->mergeBindings($results->getQuery()) // you need to get underlying Query Builder
                ->count();
        }

        $results = $results->select(DB::raw($table . '.*'));

        if($perRage) {
            $results = $this->paginate($results, $perRage);
        }
        else {
            $results = $results->get();
        }

        return [
            'count' => $count,
            'items' => $results
        ];
    }

    /**
     * Paginate results without counting
     *
     * @param $query
     * @param null $perPage
     * @param string $pageAttrName
     * @return mixed
     */
    private function paginate($query, $perPage = null, $pageAttrName = 'page')
    {
        $page = $this->getPageNumber($pageAttrName);

        return $query->limit($perPage)
            ->offset(($page-1) * $perPage)
            ->get();
    }

    /**
     * Get page number from request
     * @param $pageAttrName
     * @return int
     */
    private function getPageNumber($pageAttrName = 'page')
    {
        $request = app()->request;
        $page = $request->has($pageAttrName)? $request->get($pageAttrName) : 1;

        return ($page >= 1)? (int)$page : 1;
    }
}