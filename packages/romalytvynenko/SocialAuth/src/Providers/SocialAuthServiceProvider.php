<?php

namespace Romalytvynenko\SocialAuth\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Socialite\SocialiteManager;
use Romalytvynenko\SocialAuth\Social\VkProvider;

class SocialAuthServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/socialauth.php' => config_path('socialauth.php'),
        ]);

        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');

        $socialite->extend(
            'vk',
            function ($app) use ($socialite) {
                $config = $app['config']['services.vk'];
                return $socialite->buildProvider(VkProvider::class, $config);
            }
        );
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        require(__DIR__.'/../routes.php');
    }
}