<?php

namespace Romalytvynenko\SocialAuth\Controllers;

use App;
use DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use Validator;
use Socialite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Contracts\Auth\Guard;


class AuthController extends Controller
{
    protected $redirectPath = '/';
    protected $loginPath = '/admin/login';

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * the model instance
     * @var User
     */
    protected $user;
    /**
     * The Guard implementation.
     *
     * @var Authenticator
     */
    protected $auth;

    /**
     * Logout user with csrf check
     *
     * @param $token
     * @return Response
     */
    public function logoutAction($token)
    {
        if($token && $token == csrf_token()) {
            $this->auth->logout();
            session()->flush();
        }

        return redirect('/');
    }

    public function __construct(Guard $auth, User $user)
    {
        $this->user = $user;
        $this->auth = $auth;

        $this->middleware('doNotCacheResponse');
        $this->middleware('guest', ['except' => ['logoutAction']]);
    }

    public function getUser(){
        return view('user.index');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        Auth::logout();
        $user = Socialite::driver('facebook')->user();
        $db_user = DB::table('users')->where('email', $user->email)->first();

        if(!$db_user){
            $password = bcrypt('123456');
            $data = [
                'name' => $user->name,
                'email' => $user->email,
                'password' => $password,
                'role' => 'user',
                'sn' => 'fb',
                'account' => $user->id,
                'icon' => $user->avatar,
            ];

            DB::table('users')->insert($data);
            $db_user = DB::table('users')->where('email', $user->email)->first();
        }

        if($db_user){
            $credentials = [
                'email' => $db_user->email,
                'password' => '123456',
            ];
            if ($this->auth->attempt($credentials))
            {
                Event::fire(new App\Events\UserHasBeenAuthorizedEvent($request, Auth::user()));

                return redirect()->intended($this->redirectPath())->withCookie(cookie()->forget('notice_hash'));
            }
        }
        return redirect()->intended($this->redirectPath());
    }

    /**
     * Redirect the user to the Vk authentication page.
     *
     * @return Response
     */
    public function redirectToVkProvider()
    {
        return Socialite::driver('vk')->redirect();
    }

    /**
     * Obtain the user information from Vk.
     *
     * @return Response
     */
    public function handleVkProviderCallback(Request $request)
    {
        Auth::logout();
        $user = Socialite::driver('vk')->user();

        $db_user = DB::table('users')->where('email', $user->email)->first();
        if(!$db_user){
            $password = bcrypt('123456');
            $data = [
                'name' => $user->name,
                'email' => $user->email,
                'password' => $password,
                'role' => 'user',
                'sn' => 'vk',
                'account' => $user->id,
                'icon' => $user->avatar,
            ];

            DB::table('users')->insert($data);
            $db_user = DB::table('users')->where('email', $user->email)->first();
        }

        if($db_user){
            $credentials = [
                'email' => $db_user->email,
                'password' => '123456',
            ];
            if ($this->auth->attempt($credentials))
            {
                Event::fire(new App\Events\UserHasBeenAuthorizedEvent($request, Auth::user()));

                return redirect()->intended($this->redirectPath())->withCookie(cookie()->forget('notice_hash'));
            }
        }
        return redirect()->intended($this->redirectPath());
    }
}