<?php

Route::group(['prefix' => 'auth'], function () {

    Route::get('fb', 'Romalytvynenko\SocialAuth\Controllers\AuthController@redirectToProvider');
    Route::get('fb/callback', 'Romalytvynenko\SocialAuth\Controllers\AuthController@handleProviderCallback');

    Route::get('vk', 'Romalytvynenko\SocialAuth\Controllers\AuthController@redirectToVkProvider');
    Route::get('vk/callback', 'Romalytvynenko\SocialAuth\Controllers\AuthController@handleVkProviderCallback');

});

Route::get('logout/{token}', 'Romalytvynenko\SocialAuth\Controllers\AuthController@logoutAction');