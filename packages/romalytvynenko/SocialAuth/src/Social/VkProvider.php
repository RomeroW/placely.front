<?php
/**
 * Created by PhpStorm.
 * User: Serhiy
 * Date: 8/9/2015
 * Time: 12:24 AM
 */

namespace Romalytvynenko\SocialAuth\Social;

use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\InvalidStateException;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;
use GuzzleHttp\ClientInterface;
use Log;

class VkProvider extends AbstractProvider implements ProviderInterface
{

    /**
     * VK API endpoint
     *
     * @var string
     */
    protected $apiUrl = 'https://api.vk.com';

    /**
     * VK API actual version
     *
     * @var string
     */
    protected $version = '5.45';

    /**
     * VK API Response language
     * On 'en' all data will be transliterated
     *
     * @var string
     */
    protected $lang = 'ru';

    /**
     * The scopes being requested
     *
     * @var array
     */
    protected $scopes = ['email'];

    /**
     * User additional info fields
     *
     * By default returns: id, first_name, last_name
     *
     * @var array
     */
    protected $infoFields = ['sex', 'photo_100', 'city', 'country', 'verified', 'site', 'nickname', 'screen_name'];

    /**
     * Get the authentication URL for the provider.
     *
     * @param  string $state
     * @return string
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://oauth.vk.com/authorize', $state);
    }

    /**
     * Get the token URL for the provider.
     *
     * @return string
     */
    protected function getTokenUrl()
    {
        return 'https://oauth.vk.com/access_token';
    }

    /**
     * Get the raw user for the given access token.
     *
     * @param  string $token
     * @return array
     */
    protected function getUserByToken($token)
    {
        $access_token = $token['access_token'];

        $userInfoUrl = $this->apiUrl . '/method/users.get?access_token=' . $access_token
            . '&fields=' . implode(',', $this->infoFields) . '&lang=' . $this->lang . '&v=' . $this->version;

        try {
            $response = $this->getHttpClient()->get($userInfoUrl, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'timeout' => 5
            ]);
        }
        catch(\Exception $e) {
            redirect()->to('/auth/vk')->sendHeaders();
            die;
        }

        $userData = json_decode($response->getBody(), true);
        $rawUser = reset($userData['response']);
        $rawUser['email'] = isset($token['email'])?$token['email']:$token['user_id'] . '@vk.com';

        return $rawUser;
    }

    /**
     * Map the raw user array to a Socialite User instance.
     *
     * @param  array $user
     * @return \Laravel\Socialite\User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id'       => $user['id'],
            'nickname' => $user['nickname'],
            'name'     => $user['first_name'] . ' ' . $user['last_name'],
            'email'    => isset($user['email']) ? $user['email'] : $user['id'] . '@vk.com',
            'avatar'   => $user['photo_100']
        ]);
    }

    /**
     * Override default by adding API version
     *
     * @param string $state
     * @return array
     */
    protected function getCodeFields($state = null)
    {
        $codeFields = parent::getCodeFields($state);
        $codeFields['v'] = $this->version;

        return $codeFields;
    }

    public function getAccessToken($code)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';

        try  {
            $response = $this->getHttpClient()->post($this->getTokenUrl(), [
                'headers' => ['Accept' => 'application/json'],
                $postKey => $this->getTokenFields($code),
                'timeout' => 5
                ]);
        } catch (\Exception $e) {
                redirect('/auth/vk')->sendHeaders();die;
            }

        return $this->parseAccessToken($response->getBody());
    }

    /**
     * Override in order to attach 'email' from requested permissions
     *
     * {@inheritdoc}
     */
    public function user()
    {
        if ($this->hasInvalidState()) {
            throw new InvalidStateException;
        }

        $token = $this->getAccessToken($this->getCode());
        $user = $this->mapUserToObject($this->getUserByToken($token));
        return $user->setToken($token['access_token']);
    }

    /**
     * Return all decoded data in order to retrieve additional params like 'email'
     *
     * {@inheritdoc}
     */
    protected function parseAccessToken($body)
    {
        return json_decode($body, true);
    }
}