# Social auth package
Provide ready to use integration of socialite library, and extend it (VK)

## Usage
To use package fill config with your app keys

### URL

- Facebook authorization: ```/auth/fb```
- VK authorization: ```/auth/vk```