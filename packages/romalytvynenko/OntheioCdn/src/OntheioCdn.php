<?php

namespace Romalytvynenko\OntheioCdn;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OntheioCdn implements CdnInterface
{

    public function process($imageTempName)
    {
        $app = config('cdnupload.ontheio.app');
        $secret = config('cdnupload.ontheio.secret');

        $sign = md5($app . file_get_contents($imageTempName) . $secret);
        $url = 'https://i.onthe.io/upload.php?app=' . $app . '&s=' . $sign;
        $post = [ 'file'=> new \CURLFile($imageTempName) ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $image = json_decode(curl_exec($ch), 1);

        return $image['key'];
    }

    public function get($key, $attr = null)
    {
        $modify = isset($attr['modify'])? "." . $attr['modify'] : null;

        $secret = config('cdnupload.ontheio.secret');

        $url = $key . $modify;
        $sign = substr(md5($url . $secret), 0, 8);

        return 'https://i.onthe.io/' . $url . '.' . $sign . '.jpg';
    }

    public function remove($key)
    {
        $url = $this->get($key, [
            'modify' => 'delete'
        ]);

        try {
            json_decode(file_get_contents($url), true);
        }
        catch(\Exception $e) {
            Log::error($e->getMessage());
        }

        return true;

    }
}

