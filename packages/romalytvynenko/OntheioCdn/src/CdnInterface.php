<?php

namespace Romalytvynenko\OntheioCdn;


interface CdnInterface
{
    /**
     * Send image to CDN, and return access data to
     * access this image later.
     *
     * @param string $imageTempName Temporary image name
     * @return mixed
     */
    public function process($imageTempName);

    /**
     * Get image from CDN
     *
     * @param mixed $key Image access data that you stored before
     * @param array $attr Attributes that allow you to resize image
     * @return mixed
     */
    public function get($key, $attr = null);

    /**
     * Remove image from CDN
     *
     * @param $key
     * @return mixed
     */
    public function remove($key);
}