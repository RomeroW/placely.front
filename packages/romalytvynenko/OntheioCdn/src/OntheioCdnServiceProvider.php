<?php

namespace Romalytvynenko\OntheioCdn;

use Illuminate\Support\ServiceProvider;

class OntheioCdnServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/cdnupload.php' => config_path('cdnupload.php'),
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}