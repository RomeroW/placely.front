## Automation
We are using gulp (http://gulpjs.com/) to compose all front end assets together.

All assets located in ```resources/assets``` folder.   

- ```sass/```: "Foundation" sources;  
- ```less/```: application's styles;  
- ```js/```: application's javascript;   

```gulpfile.js``` that located in application's root contain next tasks:  

- *default*: default task, runs next tasks: *initCssAssemble*, *js*, *watch*  
- *sass*: compile all ```**/*.sass``` files into ```foundation.css``` and store it to temp folder.  
- *less*: compile all ```**/*.less``` files into ```project.css``` and store it to temp folder.  
- ***initCssAssemble***: concat all ```*.css``` files from temp folder, after *sass* and *less* tasks finished, and store it to ```public/css/project.min.css```.
- *incrementLessAssemble*: concat all ```*.css``` files from temp folder, after *less* task finished, and store it to ```public/css/project.min.css```.  
- *incrementSassAssemble*: concat all ```*.css``` files from temp folder, after *sass* task finished, and store it to ```public/css/project.min.css```.  
- ***js***: concat and uglify js from ```js``` folder, and store it to ```public/js/app.min.js``` file.  
- ***watch***: watching ```sass```, ```less``` and ```js``` files changes, and if file change run appropriate task: *incrementSassAssemble*, *incrementLessAssemble*, *js*.  

## Architecture
Веб інтерфейс placely складається із сторінко різного типу. І для деяких з них є унікальний js код, який треба виконати. Сторінки ідентифікуються за допомогою атрибуту data-page в елементу body.

Для всього JS існує вхідна точка - ```application.js```. Там визначається який код запустити для сторінки (body[data-page] атрибут). Сам код, що виконуватиметься для сторінки знаходиться в ```pages.js```. Код сторінки це властивості глобального об’єку Pages. В кожної з них має бути метод init(). В ньому і будується вже вся логіка.

В проекті також є плагіни jquery (кастомна карта, автокомпліт, і т.д.) - вони теж винесені в окремі файлики, і знаходяться в js/plugins.
 
Ну і в головному layout(html) підключений один скомпільований js файл (app.min.js), в якому знаходиться конкатенований і мініфікований js проекту.

## Modal windows