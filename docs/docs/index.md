# Placely Web Application
Placely is a beautiful web application that created for people who looking for nice place. We provide the place, where people can left there propose, and let the other people to find the place.

Placely is baked with love, so you are reading this docs now. In this docs you can found all guidelines related to "placely" development, as frontend as backend.

## Requirements
Placely is baked using laravel. And the Laravel Framework has a few system requirements:

- PHP >= 5.5.9
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension

## First Install
1. Clone this repository.
2. Config your web server to look in project's folder.
3. ```composer install``` in project's folder.
4. Import ```database/placely.sql``` in your database.
5. Create .env using .env.example.
6. Make ```storage``` dir writable by server.
7. Make sure that your APP_KEY value in .env is 32 symbols length.
8. ...
9. ...
10. PROFIT!1

## Using PHP built-in server
For quick test without having real web server you can use php built-in web server. Run: 
```sh
php artisan serve
```
For specify port use next command:
```sh
php artisan serve --port=8080
```