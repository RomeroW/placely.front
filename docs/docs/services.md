# Services
Here is the list of all used external services in application. It describes service and its usage. 

## i.onthe.io
We use i.onthe.io for storing of static images. Usage can be found on notice creation page. (```/create```)

@todo: Implement our caching layer between client and i.onthe.io