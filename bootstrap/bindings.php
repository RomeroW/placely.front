<?php

/**
 * @var $app
 */

/*
 * Using this CDN service to store images
 */

$app->singleton(
    Romalytvynenko\OntheioCdn\CdnInterface::class,
    Romalytvynenko\OntheioCdn\OntheioCdn::class
);

/**
 * Get url of versioned asset
 *
 * @param $path
 * @return string
 */
function rev_asset($path)
{
    return \App\Helpers\Asset::url($path);
}

/**
 * Implement word wrapping... Ughhh... why is this NOT done for me!!!
 * OK... I know the algorithm sucks at efficiency, but it's for short messages, okay?
 * Make sure to set the font on the ImagickDraw Object first!
 *
 * @param $image Imagick Image Object
 * @param $draw ImagickDraw Object
 * @param $text string text you want to wrap
 * @param $maxWidth int width in pixels for your wrapped "virtual" text box
 * @return array of lines and line heights
 */
function wordWrapAnnotation(&$image, &$draw, $text, $maxWidth)
{
    $words = explode(" ", $text);
    $lines = array();
    $i = 0;
    $lineHeight = 0;
    while($i < count($words) )
    {
        $currentLine = $words[$i];
        if($i+1 >= count($words))
        {
            $lines[] = $currentLine;
            break;
        }
        //Check to see if we can add another word to this line
        $metrics = $image->queryFontMetrics($draw, $currentLine . ' ' . $words[$i+1]);
        while($metrics['textWidth'] <= $maxWidth)
        {
            //If so, do it and keep doing it!
            $currentLine .= ' ' . $words[++$i];
            if($i+1 >= count($words))
                break;
            $metrics = $image->queryFontMetrics($draw, $currentLine . ' ' . $words[$i+1]);
        }
        //We can't add the next word to this line, so loop to the next line
        $lines[] = $currentLine;
        $i++;
        //Finally, update line height
        if($metrics['textHeight'] > $lineHeight)
            $lineHeight = $metrics['textHeight'];
    }
    return array($lines, $lineHeight);
}