<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AttachmentSession
 * @package App
 *
 * @property int id
 * @property int attachment_id
 * @property string hash
 */
class AttachmentSession extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attachment_session';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['attachment_id', 'hash'];
}
