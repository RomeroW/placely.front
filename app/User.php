<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;
use Romalytvynenko\Blog\Writer;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, Writer;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'role', 'sn', 'password', 'email', 'account', 'icon'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'sn', 'remember_token'];

    public function notices()
    {
        return $this->hasMany(Notice::class, 'user_id', 'id');
    }

    public function getSocialName()
    {
        $accounts = [
            'vk' => 'VK',
            'fb' => 'Facebook'
        ];

        if(array_key_exists($this->sn, $accounts)) {
            return $accounts[$this->sn];
        }

        return $this->sn;
    }

    /**
     * Get available contacts for user
     *
     * @return array
     */
    public function getAvailableContacts()
    {
        $availableContacts = [
            'phone' => '+38 (***) *** ** **',
            'skype' => '************'
        ];

        $contacts = [];

        if($this->account) {
            $contacts[] = (object)[
                'title' => $this->getSocialName(),
                'type' => 'sn',
                'mask' => '************'
            ];
        }

        foreach($availableContacts as $title => $mask) {
            if($val = $this->{$title}) {
                $contact = (object)[
                    'title' => trans('general.' . $title),
                    'type' => $title,
                    'mask' => $mask
                ];

                $contacts[] = $contact;
            }
        }

        return $contacts;
    }

    /**
     * Determine that this user has no contacts
     *
     * @return bool
     */
    public function hasNoContacts()
    {
        return !$this->phone && !$this->skype;
    }

    /**
     * Return formatted phone, if phone is set
     *
     * @return bool|string
     */
    public function getPhone()
    {
        if(!$this->phone) {
            return false;
        }

        return '+38 (' . substr($this->phone, 0, 3) . ') '
            . substr($this->phone, 3, 3) . ' '
            . substr($this->phone, 6, 2) . ' '
            . substr($this->phone, 8, 2);
    }

    /**
     * Generate social account link
     *
     * @return string
     */
    public function getAccountLink()
    {
        $accountLinks = [
            'vk' => 'https://vk.com/id',
            'fb' => 'https://facebook.com/'
        ];

        if(array_key_exists($this->sn, $accountLinks)) {
            return $accountLinks[$this->sn] . $this->account;
        }

        return '#';
    }

    public static function getGuestNoticesCount()
    {
        $request = app()->request;

        if(!Auth::check()) {
            if($hash = $request->cookie('notice_hash')) {
                $notices = NoticeSession::where('hash', '=', $hash)->count();
                if($notices > 0) {
                    return $notices;
                }
            }
        }

        return false;
    }

    public static function getAuthNoticesCount()
    {
        if(Auth::check()) {
            return Notice::where('user_id', '=', Auth::user()->id)->count();
        }
        else {
            if($notices = self::getGuestNoticesCount()) {
                return $notices;
            }
        }

        return false;
    }
}
