<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Romalytvynenko\OntheioCdn\CdnInterface;

/**
 * Class Attachment
 * @package App
 *
 * @property string hash
 */
class Attachment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attachments';

    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Return image url from CDN
     *
     * @param string|null $modify
     */
    public function getImageUrl($modify = null)
    {
        $attrs = $modify? ['modify' => $modify] : null;
        return app(CdnInterface::class)->get($this->hash, $attrs);
    }

    /**
     * Delete from CDN and delete model.
     *
     * @return bool
     * @throws \Exception
     */
    public function delete()
    {
        return app(CdnInterface::class)->remove($this->hash) && parent::delete();
    }
}
