<?php

namespace App\Events;

use App\Events\Event;
use App\User;
use Illuminate\Http\Request;

class UserHasBeenAuthorizedEvent extends Event
{
    public $request;
    public $user;

    public function __construct(Request $request, User $user)
    {
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
