<?php

namespace App;

use App\Database\Eloquent\Model;

class Highlight extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'higlights';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Return JSON string of all items
     *
     * @return string
     */
    public static function allJSON()
    {
        $highlights = Highlight::all();
        return json_encode(array_map(function($item){
            return (object)[
                'id' => (int)$item['id'],
                'value' => $item['title'],
                'type' => $item['icon'] // @todo: rename to TYPE!
            ];
        }, $highlights->toArray()));
    }
}