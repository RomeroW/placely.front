<?php

namespace App\Database\Eloquent;

use Illuminate\Http\Request;

class Model extends \Illuminate\Database\Eloquent\Model
{
    /**
     * Fill model's data from request
     *
     * @param Request $request
     * @param array $fields
     * @return $this
     */
    public static function fromRequest(Request $request, array $fields, $save = false)
    {
        $attributes = [];

        foreach ($fields as $field => $value) {
            if(is_numeric($field)) {
                if($request->has($value)) {
                    $attributes[$value] = $request->get($value);
                }
            }
            else {
                $attributes[$field] = $value;
            }
        }

        $model = new static($attributes);

        if($save) {
            $model->save();
        }

        return $model;
    }
}