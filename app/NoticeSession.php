<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NoticeSession
 * @package App
 *
 * @property int id
 * @property int notice_id
 * @property string hash
 */
class NoticeSession extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notice_session';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['notice_id', 'hash'];
}
