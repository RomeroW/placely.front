<?php

namespace App\Listeners;

use App\Events\UserHasBeenAuthorizedEvent;
use App\Notice;
use App\NoticeSession;
use DB;

class UserHasBeenAuthorizedListener
{
    /**
     * Handle the event.
     *
     * @param  UserHasBeenAuthorizedEvent $event
     * @return void
     */
    public function handle(UserHasBeenAuthorizedEvent $event)
    {
        if($hash = $event->request->cookie('notice_hash')) {

            $ownNotices = NoticeSession::where('hash', '=', $hash)->get()->map(function($item){
                return $item->notice_id;
            });

            DB::table('notices')
                ->whereIn('id', $ownNotices->all())
                ->update([
                    'user_id' => $event->user->id,
                    'status' => Notice::STATUS_PENDING
                ]);

            DB::table('notice_session')->whereIn('notice_id', $ownNotices->all())->delete();
        }
    }
}
