<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetroStation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'metro';

    /**
     * All json items
     *
     * @return array
     */
    public static function allJSON()
    {
        $metros = MetroStation::all();
        return json_encode(array_map(function($item){
            return (object)[
                'id' => (int)$item['id'],
                'value' => $item['title'],
                'type' => $item['icon'] // @todo: rename to TYPE!
            ];
        }, $metros->toArray()));
    }

    /**
     * Prepare model data for generating search form
     */
    public static function prepareForSearch()
    {
        $result = [
            'branches' => [
                'red' => 'Червона',
                'green' => 'Зелена',
                'blue' => 'Синя',
            ],
            'stations' => []
        ];

        array_map(function($item) use (&$result){
            $result['stations'][$item['icon']][$item['id']] = $item['title'];
        }, MetroStation::all()->toArray());

        return $result;
    }
}
