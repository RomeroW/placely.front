<?php

namespace App\Searchers;

use App\District;
use App\DistrictNotice;
use App\HighlightNotice;
use App\MetroNotice;
use App\MetroStation;
use App\Notice;
use Romalytvynenko\RelationSearcher\Reference;
use Romalytvynenko\RelationSearcher\Searcher;

/**
 * Class NoticeSearcher
 * @package App\Searchers
 *
 * @var $referrer Reference
 */
class NoticeSearcher extends Searcher
{
    public function __construct()
    {
        parent::__construct(Notice::class);

        $this->buildQuery(function($query) {
            return $query
                ->where('status', '=', Notice::STATUS_PUBLISHED)
                ->with(Notice::getEagleLoad())
                ->groupBy('id')
                ->orderBy('updated_at', 'desc');
        });

        $this->addReference('p', function(Reference $referrer, $query, $data) {
            return $referrer->buildQuery('number', $query, 'price', $data);
        });

        $this->addReference('rooms', function(Reference $referrer, $query, $data) {
            return $referrer->buildQuery('number', $query, 'rooms', $data);
        });

        /**
         * Metro station search by ids
         */
        $this->addReference('m', function(Reference $referrer, $query, $data) {
            $mn = (new MetroNotice)->getTable();

            return $query->join($mn, function ($join) use($referrer, $mn, $data) {
                $join->on('notices.id', '=', $mn . '.notice_id');
                $referrer->buildJoin('string', $join, 'metro_id', $data);
            });
        });

        /**
         * Districts search by ids
         */
        $this->addReference('d', function(Reference $referrer, $query, $data) {
            $dt = (new DistrictNotice)->getTable();

            return $query->join($dt, function ($join) use($referrer, $dt, $data) {
                $join->on('notices.id', '=', $dt . '.notice_id');
                $referrer->buildJoin('string', $join, 'district_id', $data);
            });
        });

        /**
         * Search notices by amenities
         */
        $this->addReference('a', function(Reference $referrer, $query, $data) {
            $ht = (new HighlightNotice)->getTable();

            return $query->join($ht, function ($join) use($referrer, $ht, $data) {
                $join->on('notices.id', '=', $ht . '.notice_id');
                $referrer->buildJoin('string', $join, 'higlight_id', $data);
            });
        });

        /**
         * Search notices by apartment type
         */
        $this->addReference('t', function(Reference $referrer, $query, $data) {
            $data = explode(',', $data);

            $type = [];
            if(in_array('0', $data)) {
                $type[] = 'room';
                if(count($data) > 1) {
                    $type[] = 'apartment';
                }
            } else {
                $type[] = 'apartment';
            }
            if(count($type)) {
                $query = $query->whereIn('type', $type);
            }

            if(in_array('apartment', $type)) {
                $query = $query->where(function($query) use ($data){
                    if(count(array_intersect([0,1,2,3], $data))) {
                        $query->whereIn('rooms', array_intersect([0, 1,2,3], $data));
                    }

                    if(in_array(4, $data)) {
                        $query->orWhere('rooms', '>=', 4);
                    }
                });
            }

            return $query;
        });
    }
}