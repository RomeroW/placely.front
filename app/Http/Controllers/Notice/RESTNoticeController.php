<?php

namespace App\Http\Controllers\Notice;

use App\Helpers\Storyteller;
use App\Http\Controllers\Controller;
use App\Notice;
use App\NoticeSession;
use Auth;
use App\Searchers\NoticeSearcher;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RESTNoticeController extends Controller
{
    /**
     * Return list of notices
     */
    public function index($page = 1)
    {
        $perPage = config('placely.notices_per_page');

        return response()->json(
            Notice::prepareAPIList(Notice::where('status', '=', Notice::STATUS_PUBLISHED)
                ->orderBy('updated_at', 'desc')
                ->with('stations', 'street', 'attachments')
                ->take($perPage)
                ->skip(($page - 1) * $perPage)
                ->get())
        );
    }

    /**
     * Return list of user's notices
     */
    public function my(Request $request, $page = 1)
    {
        $perPage = config('placely.notices_per_page');

        $notices = new Collection();

        if(Auth::check()) {
            $user = Auth::user();

            $notices = Notice::where('user_id', '=', $user->id)
                ->orderBy('updated_at', 'desc')
                ->with('stations', 'street', 'attachments')
                ->take($perPage)
                ->skip(($page - 1) * $perPage)
                ->get();
        }
        else {
            if($hash = $request->cookie('notice_hash')) {

                $ownNotices = NoticeSession::where('hash', '=', $hash)->get()->map(function($item){
                    return $item->notice_id;
                });

                $notices = Notice::whereIn('id', $ownNotices->all())
                    ->orderBy('updated_at', 'desc')
                    ->with('stations', 'street', 'attachments')
                    ->take($perPage)
                    ->skip(($page - 1) * $perPage)
                    ->get();
            }
        }

        return response()->json(
            Notice::prepareAPIList($notices)
        );
    }

    /**
     * Implements search
     *
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        $results = (new NoticeSearcher)->search($request->all(), config('placely.notices_per_page'));

        return response()->json([
            'count' => $results['count'],
            'items' => Notice::prepareAPIList($results['items'])
        ]);
    }

    public function story(Request $request)
    {
        if(!Auth::check() || Auth::user()->role != 'admin')
            throw new NotFoundHttpException;

        if(!$request->has('id')) return;

        $notice = Notice::findOrFail($request->get('id'));

        return response()->json([
            'id' => $notice->id,
            'story' => (new Storyteller($notice))->write()
        ]);
    }

    public function stories(Request $request)
    {
        if(!Auth::check() || Auth::user()->role != 'admin')
            throw new NotFoundHttpException;

        $notices = Notice::where('status', Notice::STATUS_PUBLISHED)->get();

        $data = [];
        foreach($notices as $notice) {
            $data[] = [
                'id' => $notice->id,
                'story' => (new Storyteller($notice))->write() . "\n" . $notice->getPermalink()
            ];
        }

        return response()->json($data);
    }
}
