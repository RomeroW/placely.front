<?php

namespace App\Http\Controllers\Notice;

use App\Attachment;
use App\District;
use App\DistrictNotice;
use App\Highlight;
use App\HighlightNotice;
use App\Http\Controllers\Controller;
use App\MetroNotice;
use App\MetroStation;
use App\Notice;
use App\NoticeSession;
use App\Searchers\NoticeSearcher;
use App\Street;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Cache;
use Validator;

class NoticeController extends Controller
{
    public function getMicroJSON()
    {
        $json = Cache::remember('microJSON', 60*24, function(){
            $settings = new Setting;

            return [
                "@context" => "http://schema.org",
                "@type" => "Website",
                "name" => $settings->where('property', 'md:name')->first()->content,
                "image" => $settings->where('property', 'md:image')->first()->content,
                "url" => $settings->where('property', 'md:url')->first()->content,
                "description" => $settings->where('property', 'md:description')->first()->content
            ];
        });

        return json_encode($json);
    }

    public function getStaticMeta($og = true)
    {
        $settings = new Setting;

        if ($og) {
            $data['og'] = Cache::remember('ogStaticMeta', 60*24, function() use ($settings){
                return $settings->where('type', 'opengraph')->get();
            });
        }

        $data['general'] = Cache::remember('generalStaticMeta', 60*24, function() use ($settings){
            return $settings->where('type', 'meta')->get();
        });

        return $data;
    }


    /**
     * Application's front page
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $notices = Notice::where('status', '=', Notice::STATUS_PUBLISHED)
            ->with(Notice::getEagleLoad())
            ->orderBy('updated_at', 'desc')
            ->limit(config('placely.notices_per_page'))
            ->get();

        $view = view('app.index', [
            'noticesTitle' => trans('general.index_titles.index'),
            'notices' => $notices,
            'micro' => $this->getMicroJSON(),
            'meta' => $this->getStaticMeta()
        ]);

        return $view;
    }

    /**
     * Process search params and return results
     *
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $results = (new NoticeSearcher)->search($request->all(), config('placely.notices_per_page'));

        $view = view('app.index', [
            'noticesTitle' => trans('general.index_titles.search'),
            'asyncSearch' => true,
            'isSearch' => true,
            'searchString' => '/notice/search?' . $request->getQueryString() . '&page=',
            'count' => $results['count'],
            'micro' => $this->getMicroJSON(),
            'notices' => $results['items']
        ]);

        return $view;
    }

    /**
     * Process one notice request
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function oneAction(Request $request, $id)
    {
        /** @var Notice $notice */
        $notice = Notice::with('stations', 'street', 'attachments')
            ->findOrFail($id);

        if (!$notice->isAvailable() && !$notice->belongsToAuthorized()) {
            throw new NotFoundHttpException;
        }

        if(!$notice->belongsToAuthorized() && $notice->status == Notice::STATUS_PUBLISHED) {
            $notice->views++;
            $notice->save(['timestamps' => false]);
        }

        return view('app.one', [
            'notice' => $notice,
            'attachments' => $notice->attachments,
            'microdata' => $this->getMicroJSON(),
            'meta' => $this->getStaticMeta(false)
        ]);
    }

    public function previewAction(Request $request, $id)
    {
        /** @var Notice $notice */
        $notice = Notice::with(Notice::getEagleLoad())
            ->findOrFail($id);

        if($preview = $notice->mainAttachment) {
            $preview = $preview->getImageUrl('r900x400');
        }
        else {
            $preview = [
                'http://www.ukraine-hotel.kiev.ua/assets/photo/ukraine-hotel/ukraine-hotel-kiev-01.jpg',
                'http://www.kievregion.net/main_kiev.jpg',
                'http://visittoukraine.com/uploads/images/gallery/independence_square3_kiev.jpg',
                'https://en.yeed.me/system/pictures/images/000/042/722/original/obolon_district_kiev.jpg?1417986422'
            ][rand(0, 3)];
        }

        return view('app.notice.preview')
            ->with('notice', $notice)
            ->with('preview', $preview);
    }

    /**
     * User's notices
     *
     * @param Request $request
     * @return Response
     */
    public function myAction(Request $request)
    {
        $notices = [];

        if(Auth::check()) {
            $user = Auth::user();

            $notices = Notice::where('user_id', '=', $user->id)
                ->orderBy('id', 'desc')
                ->limit(config('placely.notices_per_page'))
                ->with(Notice::getEagleLoad())
                ->get();
        }
        else {
            if($hash = $request->cookie('notice_hash')) {

                $ownNotices = NoticeSession::where('hash', '=', $hash)->get()->map(function($item){
                    return $item->notice_id;
                });

                $notices = Notice::whereIn('id', $ownNotices->all())
                    ->orderBy('id', 'desc')
                    ->with(Notice::getEagleLoad())
                    ->limit(config('placely.notices_per_page'))
                    ->get();
            }
        }

        $view = view('app.index', [
            'noticesTitle' => trans('general.index_titles.my'),
            'notices' => $notices,
            'searchString' => '/notice/my/'
        ]);

        return $view;
    }

    /**
     * Notice creation page
     *
     * @return \Illuminate\View\View
     */
    public function createAction(Request $request)
    {
        if($request->isMethod('post')) {
            $request->flash();

            $this->validate($request, [
                'price' => 'required',
                'description' => 'required_without:brief|max:2500',
                'brief' => 'max:140',
                'street' => 'required',
            ]);

            /** @var \Illuminate\Support\MessageBag $errors */

            $response = $this->processNewNotice($request);

            if($response) {
                return $response;
            }
        }

        $view = view('app.create', [
            'createSessionHash' => str_random(32),
            'metros' => MetroStation::allJSON(),
            'facilities' => Highlight::allJSON(),
            'amenities' => Highlight::all(),
            'districts' => District::allJSON(),
            'streets' => Street::allJSON()
        ]);

        return $view;
    }

    /**
     * Process request and create notice from it
     *
     * @param Request $request
     * @return Response
     */
    public function processNewNotice(Request $request)
    {
        $response = false;

        $notice = Notice::fromRequest($request, [
            'type',
            'price',
            'rooms',
            'total_space',
            'effective_space',
            'living_space',
            'street_id',
            'description',
            'brief',
            'building'
        ], true);

        $notice->status = Auth::check() ? Notice::STATUS_PENDING : Notice::STATUS_UNAUTHORIZED;

        // Process photos
        if($request->has('photos')) {
            foreach($request->get('photos') as $photoId) {
                $attachment = Attachment::find($photoId);
                $attachment->notice_id = $notice->id;
                $attachment->save();

                if($request->has('mainPreview') && $request->get('mainPreview') == $attachment->id) {
                    $notice->main_attachment_id = $attachment->id;
                }
            }
        }

        // Save coordinates
        $notice->map_lat = $request->get('mapLat');
        $notice->map_lng = $request->get('mapLng');

        // Save metro stations with timing
        if($request->has('metro')) {
            foreach ($request->get('metro') as $stationId => $timing) {
                $metroNotice = new MetroNotice;

                $metroNotice->foot = isset($timing['foot']) ? $timing['foot'] : null;
                $metroNotice->transport = isset($timing['transport']) ? $timing['transport'] : null;
                $metroNotice->notice_id = $notice->id;
                $metroNotice->metro_id = $stationId;

                $metroNotice->save();
            }
        }

        // Save highlights
        if($request->has('highlight')) {
            foreach ($request->get('highlight') as $higlightId) {
                $highlightNotice = new HighlightNotice;

                $highlightNotice->notice_id = $notice->id;
                $highlightNotice->higlight_id = $higlightId;

                $highlightNotice->save();
            }
        }

        // Save districts
        if($request->has('districts')) {
            foreach ($request->get('districts') as $districtId) {
                $districtNotice = new DistrictNotice;

                $districtNotice->notice_id = $notice->id;
                $districtNotice->district_id = $districtId;

                $districtNotice->save();
            }
        }

        /**
         * This part is responsible for attaching unauthorized users to their notices
         *
         * We create hash, store it to user's cookies, and create relation between
         * created notice and hash. So in that way we link unauthorized user with notice.
         */
        $cookies = [];

        if(Auth::check() && $user = Auth::user()) {
            $notice->user_id = $user->id;
        }
        else {
            // if cookie isn't set set it
            if(!$cookie = $request->cookie('notice_hash')) {
                $cookie = md5(rand(0,999) . time());
                $cookies['notice_hash'] = $cookie;
            }

            $noticeSession = new NoticeSession([
                'notice_id' => $notice->id,
                'hash' => $cookie
            ]);
            $noticeSession->save();
        }

        if($notice->save()) {
            $response = redirect()->to('/' . $notice->id);
            foreach ($cookies as $key => $value) {
                $response->withCookie(cookie()->forever($key, $value));
            }
        }

        return $response;
    }

    /**
     * Return json of user's contacts
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function getContacts(Request $request, $id)
    {
        /** @var Notice $notice */
        $notice = Notice::findOrFail($id);

        if($notice->status != Notice::STATUS_PUBLISHED) {
            throw new NotFoundHttpException;
        }

        /** @var User $user */
        $user = $notice->user;

        $availableContacts = [
            'phone' => 'getPhone',
            'skype' => null
        ];

        $contacts = [];

        if($user->sn) {
            $contacts['sn'] = '<a href="' . $user->getAccountLink() . '" target="_blank">перейти</a>';
        }

        foreach($availableContacts as $title => $accessorMethod) {
            if($accessorMethod
                && method_exists($user, $accessorMethod)
                && $val = $user->{$accessorMethod}()) {

                $contacts[$title] = $val;
            }
            elseif($val = $user->{$title}) {
                $contacts[$title] = $val;
            }
        }

        $notice->contact_views++;
        $notice->save(['timestamps' => false]);

        return response()->json($contacts);
    }


    /**
     * Make notice booked
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function makeItBooked(Request $request, $id)
    {
        /** @var Notice $notice */
        $notice = Notice::findOrFail($id);

        if($notice->status != Notice::STATUS_PUBLISHED || $notice->user->id != Auth::user()->id) {
            return redirect($notice->getPermalink());
        }

        $notice->status = Notice::STATUS_BOOKED;
        $notice->save();

        return redirect($notice->getPermalink());
    }

    /**
     * Mark notice as ready to publish again
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function makeItPublished(Request $request, $id)
    {
        /** @var Notice $notice */
        $notice = Notice::findOrFail($id);

        if($notice->status != Notice::STATUS_BOOKED || $notice->user->id != Auth::user()->id) {
            return redirect($notice->getPermalink());
        }

        $notice->status = Notice::STATUS_REPENDING;

        $notice->views = 0;
        $notice->contact_views = 0;

        $notice->save();

        return redirect($notice->getPermalink());
    }
}
