<?php

namespace App\Http\Controllers\User;

use App\Notice;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Cache;
use Validator;

class UserController extends Controller
{
    /**
     * User's settings page
     *
     * @param Request $request
     * @return $this
     * @throws NotFoundHttpException
     */
    public function settingsAction(Request $request)
    {
        if(!Auth::check()) {
            throw new NotFoundHttpException;
        }

        $saved = false;

        if($request->isMethod('post')) {

            $validator = Validator::make($request->all(), []);

            $phone = false;
            if($request->has('phone')) {
                $phone = str_replace(['+38', ' ', '(', ')', '-'], '', $request->get('phone'));
            }

            $skype = false;
            if($request->has('skype')) {
                $skype = trim($request->get('skype'),' ');
            }

            $validator->after(function($validator) use ($phone, $skype) {
                if ($phone && !preg_match('/[0-9]/', $phone)) {
                    $validator->errors()->add('phone', 'Тут має бути вказатний номер телефону');
                }

                if (($skype || strlen($skype)) && !preg_match("/^[a-zA-Z][a-zA-Z0-9\\/.,\\/-_]{5,31}$/", $skype)) {
                    $validator->errors()->add('skype', 'Вкажіть вірний формат логіну Skype');
                }
            });

            if ($validator->fails()) {
                return redirect('settings')
                    ->withErrors($validator)
                    ->withInput();
            }

            $user = Auth::user();

            $user->phone = $phone;
            $user->skype = $skype;

            $saved = $user->save();
        }

        return view('app.settings')->with('saved', $saved);
    }
}