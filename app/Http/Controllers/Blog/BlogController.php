<?php

namespace App\Http\Controllers\Blog;

use App\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Image;
use Intervention\Image\ImageManager;
use View;
use Cache;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogController extends Controller
{
    /**
     * List of articles
     *
     * @param Request $request
     * @return $this
     * @throws NotFoundHttpException
     */
    public function listAction(Request $request)
    {
        $posts = Post::query()
            ->where('status', '=', Post::STATUS_PUBLISHED)
            ->orderBy('id', 'desc')
            ->get();
        return view('app.blog.list')->with('posts', $posts);
    }
    /**
     * Blog one action
     *
     * @param Request $request
     * @return $this
     * @throws ModelNotFoundException
     */
    public function oneAction(Request $request, $slug)
    {
        if(!$post = Post::where('slug', $slug)->first()) {
            // try to find post from pages
            if(View::exists('pages.' . $slug)) {
                return view('app.blog')->with('page', $slug);
            }
            throw new ModelNotFoundException;
        }

        if($post->status != Post::STATUS_PUBLISHED) {
            // @todo: fix for non-privileged users
            if(!Auth::check() || (!in_array(Auth::user()->role, ['admin', 'moderator']) && Auth::user()->id != $post->user_id)) {
                throw new ModelNotFoundException;
            }
        }

        $post->views++;
        $post->save();

        return view('app.blog.one')->with('post', $post);
    }

    /**
     * Generate blog preview for og:image
     *
     * @param Request $request
     * @param $slug
     */
    public function preview(Request $request, $slug)
    {
        if(!$post = Post::where('slug', $slug)->first()) {
            // try to find post from pages
            throw new ModelNotFoundException;
        }

        if(!$request->has('update')) {
            if(Cache::has('preview-' . $slug)) {
                header('Content-Type: image/jpg');
                echo Cache::get('preview-' . $slug);
                die;
            }
        }

        $str = $post->title;

        $fontSize = 36;
        $fromTop = 75;

        $img = Image::canvas(800, 420, '#fff');

        /** @var \Imagick $imagick */
        $image = $img->getCore();

        $imageDraw = new \ImagickDraw();
        $imageDraw->setFont(base_path() . '/resources/assets/fonts/OpenSans-Bold.ttf');
        $imageDraw->setFontSize($fontSize);

        list($lines, $lineHeight) = wordWrapAnnotation($image, $imageDraw, $str, 670);

        $img->insert(base_path() . '/resources/assets/images/logon2.png', 'top-left', 20, 25);

        $textHeight = $lineHeight * count($lines);
        $img->rectangle(30, 25 + $fromTop, 37, 25 + $textHeight + 35 + $fromTop, function ($draw) {
            $draw->background('#4782fa');
        });
        $image = $img->getCore();

        for($i = 0; $i < count($lines); $i++)
            $image->annotateImage($imageDraw, 60, $fromTop + 40 + $fontSize + $i*$lineHeight, 0, $lines[$i]);


        $image->setImageFormat('jpg');
        $image->setImageCompressionQuality(100);

        Cache::forever('preview-' . $slug, $image->getImageBlob());

        header('Content-Type: image/'.$image->getImageFormat());
        echo $image;
    }
}