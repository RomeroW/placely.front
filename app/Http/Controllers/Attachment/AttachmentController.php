<?php

namespace App\Http\Controllers\Attachment;

use App\Attachment;
use App\AttachmentSession;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Romalytvynenko\OntheioCdn\CdnInterface;

/**
 * Class AttachmentController
 * @package App\Http\Controllers\Attachment
 *
 * @property $app
 */
class AttachmentController extends Controller
{
    /**
     * Upload image to CDN, and store it's hash to database
     * as Attachment model
     * only via POST
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function createImage(Request $request)
    {
        if($request->file() && $request->has('createSessionHash'))
        {
            $sessionHash = $request->get('createSessionHash');
            $image = $request->file('file');

            /*
             * Resize image before upload to cdn (@todo: need it?)
             */
            //$filename  = time() . '.' . $image->getClientOriginalExtension();
            //$path = public_path('images/' . $filename);
            //Image::make($image->getRealPath())->resize(800, null, function ($constraint) {
            //
                // /** @var \Intervention\Image\Constraint $constraint */
                //$constraint->aspectRatio();
            //
            //})->save($path);

            // Upload resized image to CDN
            $hash = app(
                CdnInterface::class
            )->process($image->getPathname());

            $attachment = new Attachment;
            $attachment->hash = $hash;
            if($attachment->save()) {
                $hashSaved = (new AttachmentSession([
                    'attachment_id' => $attachment->id,
                    'hash' => $sessionHash
                ]))->save();

                if($hashSaved) {
                    return JsonResponse::create([
                        'success' => true,
                        'data' => [
                            'id' => $attachment->id,
                            'url' => $attachment->getImageUrl('r95x95')
                        ]
                    ]);
                }

                return JsonResponse::create([
                    'success' => false,
                    'message' => 'Unable to save attachment session'
                ]);
            }
        }

        return JsonResponse::create([
            'success' => false,
            'message' => 'No file, or error while saving attachment'
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function removeImage(Request $request)
    {
        if($request->has('createSessionHash') && $request->has('attachmentId')) {
            $attachmentSession = AttachmentSession::where([
                'hash' => $request->get('createSessionHash'),
                'attachment_id' => $request->get('attachmentId')
            ]);

            if(!$attachmentSession) {
                return JsonResponse::create([
                    'success' => false,
                    'message' => 'No attachment session'
                ]);
            }

            $attachment = Attachment::find($request->get('attachmentId'));
            if(!$attachment) {
                return JsonResponse::create([
                    'success' => false,
                    'message' => 'No attachment'
                ]);
            }

            if($attachmentSession->delete() && $attachment->delete()) {
                return JsonResponse::create([
                    'success' => true,
                    'message' => 'Attachment deleted!'
                ]);
            }
        }

        return JsonResponse::create([
            'success' => false,
            'message' => 'Something went wrong!'
        ]);
    }
}