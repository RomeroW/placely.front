<?php

namespace App\Http\Cache;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\ResponseCache\CacheProfiles\CacheAllSuccessfulGetRequests;
use Symfony\Component\HttpFoundation\Response;

class PlacelyCache extends CacheAllSuccessfulGetRequests
{
    public $cached = [
        'index',
        'one',
        'bloglist',
        'blogitem'
    ];

    /**
     * Determine if the given request should be cached;.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public function shouldCacheRequest(Request $request)
    {
        if($request->hasCookie('notice_hash')) {
            return false;
        }

        return in_array($request->route()->getName(), $this->cached);

        //@todo: esi
        if($request->route()->getName() == 'preview') {
            return false;
        }

        if ($request->ajax()) {
            return false;
        }

        if ($this->isRunningInConsole()) {
            return false;
        }

        return $request->isMethod('get');
    }

    /**
     * Determine if the given response should be cached.
     *
     * @param \Symfony\Component\HttpFoundation\Response $response
     *
     * @return bool
     */
    public function shouldCacheResponse(Response $response)
    {
        return !Auth::check() && $response->isSuccessful();
    }
}