<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => [
    'App\Http\Middleware\BeforeMiddleware',
    'App\Http\Middleware\AfterMiddleware']], function() {

    Route::get('/', [
        'as' => 'index',
        'uses' => 'Notice\NoticeController@indexAction']);

    Route::get('/{id}', [
        'as' => 'one',
        'uses' => 'Notice\NoticeController@oneAction'
    ])->where('id', '[0-9]+');

    Route::get('/{id}/preview', [
        'as' => 'preview',
        'uses' => 'Notice\NoticeController@previewAction',
        'middleware' => \App\Http\Middleware\FilterIP::class
    ]);

    Route::get('/my', [
        'as' => 'my',
        'uses' => 'Notice\NoticeController@myAction'
    ]);

    Route::get('/search', 'Notice\NoticeController@searchAction', ['as' => 'search']);

    /*
     * RESTFull API Interface
     */

    /*
     * Notice
     */
    Route::get('/notice/search', 'Notice\RESTNoticeController@search');
    Route::get('/notice/story', 'Notice\RESTNoticeController@story');
    Route::get('/notice/stories', 'Notice\RESTNoticeController@stories');
    Route::get('/notice/my/{page?}', [
        'as' => 'my',
        'uses' => 'Notice\RESTNoticeController@my'
    ]);
    Route::get('/notice/{page?}', 'Notice\RESTNoticeController@index');
    Route::resource('/notice', 'Notice\RESTNoticeController',
        ['only' => [
            'show'
        ]]);

    // show contacts
    Route::post('/notice/{id}/contacts', [
//        'before'=>'csrf',
        'as' => 'noticecontacts',
        'uses' => 'Notice\NoticeController@getContacts'
    ]);
    // send notice to booked
    Route::post('/notice/{id}/booked', [
        'before'=>'csrf',
        'as' => 'noticebooked',
        'uses' => 'Notice\NoticeController@makeItBooked'
    ]);
    // send notice to published
    Route::post('/notice/{id}/publish', [
        'before'=>'csrf',
        'as' => 'noticepublish',
        'uses' => 'Notice\NoticeController@makeItPublished'
    ]);

    /*
     * Blog
     */
    Route::get('/blog', [
        'as' => 'bloglist',
        'uses' => 'Blog\BlogController@listAction'
    ]);
    Route::get('/blog/preview/{hash}.jpg', [
        'as' => 'blogpreview',
        'uses' => 'Blog\BlogController@preview'
    ]);
    Route::get('/blog/{permalink}', [
        'as' => 'blogitem',
        'uses' => 'Blog\BlogController@oneAction'
    ]);

    /*
     * User
     */
    Route::match(['get', 'post'], '/settings', [
        'as' => 'settings',
        'uses' => 'User\UserController@settingsAction'
    ]);
});

Route::match(['get', 'post'], 'create', 'Notice\NoticeController@createAction');

Route::post('attachment', 'Attachment\AttachmentController@createImage');
Route::delete('attachment', 'Attachment\AttachmentController@removeImage');
