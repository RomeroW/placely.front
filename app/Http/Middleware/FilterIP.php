<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FilterIP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($allowedIps = config('placely.ip.' . $request->route()->getName())) {
            if(!in_array($request->getClientIp(), $allowedIps)) {
                throw new NotFoundHttpException;
            }
        }
        return $next($request);
    }
}
