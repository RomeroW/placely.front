<?php

namespace App;

use SirTrevorJs;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;

class Post extends \Romalytvynenko\Blog\Post
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    protected $fillable = [
        'title',
        'content',
        'status'
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get brief
     *
     * @return string
     */
    public function getShort()
    {
        if($this->description) {
            return $this->description;
        }

        $firstTextBlock = SirTrevorJs::first($this->content, 'text', 'array');
        if(!is_array($firstTextBlock)) {
            return '';
        }

        return explode("\n\n", $firstTextBlock[0]['text'])[0];
    }

    public function getPermalink()
    {
        return url('/blog/' . $this->slug);
    }
}
