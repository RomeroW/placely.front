<?php

namespace App;

use App\Database\Eloquent\Model;
use App\Searchers\NoticeSearcher;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Jenssegers\Date\Date;
use Auth;
use Romalytvynenko\OntheioCdn\CdnInterface;
use Romalytvynenko\RelationSearcher\Reference;
use Romalytvynenko\RelationSearcher\Searcher;

class Notice extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_REPENDING = 're-pending';
    const STATUS_UNAUTHORIZED = 'unauthorized';
    const STATUS_PUBLISHED = 'published';
    const STATUS_BOOKED = 'booked';
    const STATUS_TRASH = 'trash';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function getEagleLoad()
    {
        return [
            'stations',
            'street',
            'districts',
            'mainDistrict',
            'attachments',
            'mainAttachment',
            'highlights'
        ];
    }

    public function save(array $options = [])
    {
        if(isset($options['timestamps']) && !$options['timestamps']) {
            $this->timestamps = false;
        }
        $saved = parent::save($options);

        if(isset($options['timestamps']) && !$options['timestamps']) {
            $this->timestamps = true;
        }

        return $saved;
    }

    public function mainAttachment()
    {
        return $this->hasOne(Attachment::class, 'id', 'main_attachment_id');
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'notice_id', 'id');
    }

    public function street()
    {
        return $this->hasOne(Street::class, 'id', 'street_id');
    }

    public function highlights()
    {
        return $this->belongsToMany(Highlight::class, 'higlights_notices', 'notice_id', 'higlight_id');
    }

    public function previewAmenities()
    {
        return $this->highlights()->where('weight', '>', 0)->orderBy('weight', 'desc')->limit(5);
    }

    public function stations()
    {
        return $this->belongsToMany(MetroStation::class, 'metro_notices', 'notice_id', 'metro_id')
            ->withPivot("foot", "transport");
    }

    public function districts()
    {
        return $this->belongsToMany(District::class, 'districts_notices', 'notice_id', 'district_id');
    }

    public function mainDistrict()
    {
        return $this->districts()->where('type', '=', 'district');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getImageForSocialNetworks($modify = null)
    {
        if($this->preview_hash) {
            $attrs = $modify? ['modify' => $modify] : null;
            return  app(CdnInterface::class)->get($this->preview_hash, $attrs);
        }

        return ($preview = $this->mainAttachment)? $preview->getImageUrl('r800x420') : false;
    }

    /**
     * Simple implementation of Querying notices
     *
     * @todo: Move this to other class
     *
     * @param array $args
     * @return mixed
     */
    public static function nQuery(array $args = [])
    {
        $defaults = [
            'status' => Notice::STATUS_PUBLISHED,
            'limit' => 3
        ];
        $args = array_merge($defaults, $args);

        $query = self::query();

        if(array_key_exists('rand', $args) && $rand = $args['rand']) {
            $query = $query->orderByRaw('RAND()');
            unset($args['rand']);
        }
        else {
            $query = $query->orderBy('id', 'desc');
        }

        if(array_key_exists('limit', $args) && $limit = $args['limit']) {
            $query = $query->limit($limit);
            unset($args['limit']);
        }

        foreach($args as $key => $value) {
            if(is_array($value)) {
                $query = $query->where($value[0], $value[1], $value[2]);
            }
            else {
                $query = $query->where($key, '=', $value);
            }
        }

        return $query->get();
    }

    /**
     * @return array
     */
    public function switchTypes()
    {
        return [
            'apartment' => trans('general.apartment'),
            'room' => trans('general.room'),
        ];
    }

    public function getPermalink()
    {
        return env('HOST') . '/' . $this->id;
    }

    /**
     * Get title for notice
     *
     * @return string
     */
    public function getTitle()
    {
        return implode(', ', [
            $this->street->real_name . ' ' .  $this->building,
            mb_strtolower($this->getType($this->type))
        ]);
    }

    /**
     * Get localized and formatted date
     *
     * @return mixed
     */
    public function getDate()
    {
        $str = str_replace('назад', 'тому', $this->updated_at->diffForHumans());
        if(strpos($str, 'секу') !== false) {
            return 'щойно';
        }
        return $str;
    }

    /**
     * Get W3C formated update date
     */
    public function getSitemapTime()
    {
        return Date::make($this->updated_at)->format('Y-m-d\TH:i:sP');
    }

    /**
     * Get price with currency
     *
     * @return string
     */
    public function getPrice()
    {
        // @todo: currency to config or to DB
        return $this->price . ' грн';
    }

    /**
     * Return html formatted description
     */
    public function getDescription()
    {
        $separator = '</p><p>';
        return '<p>' . implode($separator, explode("\n", strip_tags($this->description))) . '</p>';
    }

    public function getInfo()
    {
        $method = 'get' . ucfirst($this->type) . 'Info';
        if(method_exists($this, $method)) {
            return $this->$method();
        }

        return implode(', ', [
            $this->getPrice(),
            $this->total_space . trans('general.m_sq')
        ]);
    }

    public function getApartmentInfo()
    {
        $data = [
            $this->getPrice()
        ];

        if($this->rooms) {
            if($this->rooms == 1) {
                $data[] = $this->rooms . ' кімната';
            }
            else {
                $data[] = $this->rooms . ' кімнати';
            }
        }

        if($this->total_space
            || $this->effective_space
            || $this->living_space) {

            $data[] = implode('/', [
                $this->total_space?:'-', //. trans('general.m_sq'),
                $this->effective_space?:'-', // . trans('general.m_sq'),
                $this->living_space?:'-', // . trans('general.m_sq')
            ]);
        }

        return implode(', ', $data);
    }

    public function getRoomInfo()
    {
        $data = [
            $this->getPrice()
        ];

        if($this->total_space) {
            $data[] = $this->total_space . trans('general.m_sq');
        }

        return implode(', ', $data);
    }

    /**
     * Get stations data
     *
     * @param bool $string
     * @return array
     */
    public function getStations($string = false)
    {
        $result = [];
        foreach($this->stations as $station) {
            $data = [
                'title' => $station->title,
                'type' => $station->icon,
                'foot' => $station->pivot->foot,
                'transport' => $station->pivot->transport,
            ];

            if($string) {
                $postfix = (($data['foot'] > 0)? trans('general.foot') . ' ' . $data['foot'] . ' хвилин' : '')
                    . (($data['transport'] > 0)? ', ' . trans('general.transport') . ' ' . $data['transport'] . ' хвилин' : '');

                if($postfix) $postfix = ' - ' . $postfix;

                $data['title'] = $data['title']
                    . $postfix;
            }
            else {
                $merge = [];

                if($data['foot'] > 0) {
                    $merge[] = trans('general.foot') . ' ' . $data['foot'] . ' хвилин';
                }

                if($data['transport'] > 0) {
                    $merge[] = trans('general.transport') . ' ' . $data['transport'] . ' хвилин';
                }

                $data['info'] = implode('<br/>', $merge);
            }

            $result[] = $data;
        }


        return $result;
    }

    public function getShortDescription()
    {
        return $this->getDescription();
    }

    /**
     * Return short info for google maps
     *
     * @return string
     */
    public function getShortInfo()
    {
        $str = '';

        if($this->type == 'apartment') {
            $str .= $this->rooms . 'к. ' . mb_strtolower($this->getType($this->type));
        }
        else {
            $str .= $this->getType($this->type);
        }

        return $str . ', ' . $this->getPrice();
    }

    public function isModerating()
    {
        return $this->status == self::STATUS_PENDING || $this->status == self::STATUS_REPENDING;
    }

    public function isUnauthorized()
    {
        return $this->status == self::STATUS_UNAUTHORIZED;
    }

    /**
     * Notice is available to see by direct link
     *
     * @return bool
     */
    public function isAvailable() {
        return in_array($this->status, [
            self::STATUS_PUBLISHED,
            self::STATUS_REPENDING,
            self::STATUS_BOOKED
        ]);
    }

    /**
     * Get human type of notice
     *
     * @param $key
     * @return string
     */
    public function getType($key)
    {
        if(array_key_exists($key, $this->switchTypes())) {
            return $this->switchTypes()[$key];
        }

        return strtoupper($key);
    }

    /**
     * Check notice belongs to user that create it
     *
     * @return bool
     */
    public function belongsToAuthorized()
    {
        if(Auth::check()) {
            return $this->user_id == Auth::user()->id;
        }
        else {
            if($hash = app()->request->cookie('notice_hash')) {
                $session = NoticeSession::where('notice_id', '=', $this->id)
                    ->first();

                if($session) {
                    return $session->hash == $hash;
                }

            }
        }
        return false;
    }

    public function generateBrief($extended = false)
    {
        $brief = [];

        $district = $this->mainDistrict;
        if($district->count()) {
            $district = $district[0];
            $brief[] = $district->title . ' район';
        }

        $stations = $this->stations->count();
        if($stations > 0) {
            switch($stations) {
                case 1:
                    $plural = 'одна станція';
                    break;
                case 2:
                    $plural = 'дві станції';
                    break;
                case 3:
                    $plural = 'три станції';
                    break;
                default:
                    $plural = $stations . ' станції';
            }
            $brief[] = 'поряд є ' . $plural . ' метро';
        }



        for($i = 0; $i < count($brief); $i++) {
            if($i == 0
                || ($i > 0 && $brief[$i-1][mb_strlen($brief[$i-1]) - 1] == '.')) {

                $brief[$i] = mb_strtoupper(mb_substr($brief[$i], 0, 1, 'UTF-8'), 'UTF-8') .
                    mb_substr($brief[$i], 1, mb_strlen($brief[$i]), 'UTF-8');
            }
        }

        if ($extended) {
            $str = implode(', ', $brief);

            // Apartment type
            $str = $this->getShortType(true) . ' в Києві, ' . $str;

            // Metro Part
            $metroPart = '';
            $stations = $this->getStations();
            if (count($stations) > 0) {
                $metroPart .= ': ';
                foreach ($stations as $station) {
                    $timing = ($station['foot'] != '0') ? ' (' . $station['foot'] . ' хв. пішки)' : '';
                    $metroPart .= $station['title'] . $timing;
                    $metroPart .= (end($stations) == $station) ? '.' : ', ';
                }
            }

            return $str . $metroPart;

        }

        return implode(', ', $brief) . (count($brief)?'.':'');
    }

    /**
     * CARD FEED METHODS SECTION
     */
    public function getBrief()
    {
        if($this->brief) {
            return $this->brief;
        }

        return $this->generateBrief();
    }

    public function getAddress()
    {
        if($this->street) {
            return trim(str_replace('вул', '', $this->street->real_name)) . ', ' . $this->building;
        }

        return 'Київ';
    }

    public function getShortType($modify = false)
    {
        if(array_key_exists($this->type, $this->switchTypes())) {
            $type = mb_strtolower($this->switchTypes()[$this->type]);
        }
        else {
            $type = strtoupper($this->type);
        }

        $typeName = ($modify) ? '-кімнатна ' : 'к. ';

        if($this->type == 'apartment' && $this->rooms) {
            $type = $this->rooms . $typeName . $type;
        }

        return $type;
    }

    public function getMetaName()
    {
        $district = $this->mainDistrict;
        $brief = '';
        if($district->count()) {
            $district = $district[0];
            $brief = ', ' . $district->title . ' район';
        }

        return 'Placely.io: ' . $this->getShortType(true) .
        ' в Києві' . $brief;
    }

    /**
     * Prepare notices collection for API (AJAX)
     *
     * @param Collection $models
     * @return static
     */
    public static function prepareAPIList(Collection $models)
    {
        return $models->map(function($item) {

            /** @var $item Notice */
            return (object)[
                'id' => $item->id,
                'permalink' => $item->getPermalink(),
                'price' => $item->price,
                'og_image' => $item->getImageForSocialNetworks(),
                'brief' => $item->getBrief(),
                'address' => $item->getAddress(),
                'type' => $item->getShortType(),
                'short_info' => $item->getShortInfo(),
                'lat' => $item->map_lat,
                'lng' => $item->map_lng,
                'created_at' => $item->getDate(),
                'metro_stations' => $item->getStations(),
                'amenities' => $item->previewAmenities,
                'photos' => $item->attachments->count(),
                'mainPhoto' => ($preview = $item->mainAttachment)?
                        $preview->getImageUrl('r500x260') : false,

                'status' => $item->belongsToAuthorized() && app()->request->route()->getName() == 'my'? [
                            'name' => $item->status,
                            'title' => trans('notice.status.'.$item->status.'.title'),
                            'description' => trans('notice.status.'.$item->status.'.description'),
                        ] : false
            ];
        });
    }

}
