<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use App\Notice;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sitemap generator';

    protected $staticRoutes = ['index'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getBlogUrls()
    {
        $posts = Post::where('status', Post::STATUS_PUBLISHED)->get();

        $urls = [];

        foreach ($posts as $item) {
            /** @var $item Post */
            array_push($urls, ['url' => $item->getPermalink(), 'date' => $item->updated_at->toW3cString()]);
        }

        return $urls;
    }

    public function getNoticeUrls()
    {
        $notices = Notice::where('status', 'published')->get();

        $urls = [];

        foreach ($notices as $item) {
            /** @var $item Notice */
            array_push($urls, ['url' => $item->getPermalink(), 'date' => $item->getSitemapTime()]);
        }

        return $urls;
    }

    public function getStaticUrls()
    {
        $urls = [];
        foreach ($this->staticRoutes as $route) {
            array_push($urls, ['url' => URL::route($route), 'date' => date('Y-m-d\TH:i:sP')]);
        }

        return $urls;
    }

    public function getDynamicUrls()
    {
        $urls = array_merge(
            $this->getBlogUrls(),
            $this->getNoticeUrls()
        );

        return $urls;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data['urls'] = array_merge(
            $this->getStaticUrls(),
            $this->getDynamicUrls()
        );
        $content = View::make('app.xml.sitemap', $data)->render();

        File::put(public_path() . '/sitemap.xml', $content);

        $this->info('Sitemap successfully updated!');

        /*
         * https://support.google.com/webmasters/answer/183669?hl=en
         */
        $googlePingUrl = 'http://google.com/ping?sitemap=' . url('sitemap.xml');
        $this->line('Pinging google to update sitemap... ('.$googlePingUrl.')');

        if(file_get_contents($googlePingUrl)) {
            $this->info('Google successfully pinged!');
        }
    }
}
