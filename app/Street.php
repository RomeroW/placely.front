<?php

namespace App;

use App\Database\Eloquent\Model;

class Street extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Return JSON string of all items
     *
     * @return string
     */
    public static function allJSON()
    {
        $streets = Street::all();
        return json_encode(array_map(function($item){
            return (object)[
                'id' => (int)$item['id'],
                'name' => $item['name'],
                'value' => $item['real_name']
            ];
        }, $streets->toArray()));
    }
}