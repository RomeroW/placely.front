<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 4/29/2016
 * Time: 12:15 AM
 */

namespace App\Helpers;


use App\Highlight;
use App\Notice;

class Storyteller
{
    private $notice;

    private $tautology;

    const KIND_GENITIVE = 'genitive';

    public function __construct(Notice $notice)
    {
        $this->notice = $notice;

        $this->tautology = new Tautology;
        $this->tautology->setTautologies([
            'є',
            'наявн',
            'розташован',
            'знаходит',
            'поряд',
            'поруч',
            'недалеко',
            'близько'
        ]);
    }

    public function write()
    {
        $text = $this->prepareSentences(array_merge(
            $this->introDescription(),
            $this->districtDescription(),
            $this->metrosDescription(),
            $this->amenitiesDescription(),

            $this->interestingStatistic()
        ));

//        print_r($text);

        return $text;
    }

    /**
     * First sentence, describe object
     *
     * @return array
     */
    private function introDescription()
    {
        /*
        Здається невеличка однокімнатна квартира на вулиці Петра Ганжа 17.
         */

        $words = [];

        // intro
        $introWords = [
            'здається' => .4,
            'ось' => .2,
            '' => .4
        ];
        $words[] = $this->getRand($introWords);

        // size
        if($this->notice->total_space  &&
            ($this->notice->total_space <= 24 + 12*$this->notice->rooms)) {
            $smallWords = [
                'невеличка',
                'невелика',
            ];

            $words[] = $this->getRand($smallWords);
        }

        // type (кімната, однокімнатна, т.д)
        $words[] = $this->getNoticeType();

        //street
        if($street = $this->notice->street) {
            $linkWords = [
                'на' => .8,
                ',' => .2
            ];
            $streetPart = $linkWord = $this->getRand($linkWords);
            $streetPart .= ' ' . $this->getStreetName($street->real_name, ($linkWord == 'на'));

            if($buildingNumber = $this->notice->building) {
                $streetPart .= ' ' . $buildingNumber;
            }

            $words[] = $streetPart;
        }

        return [$this->prepareSentence($words)];
    }

    /**
     * Sentence, that describe district
     *
     * @return array
     */
    private function districtDescription()
    {
        /*
         * Will start new tautologies array
         */
        $this->tautology->startNewSentence();

        if(!($district = $this->notice->mainDistrict()->first())) return [];

        $words = [];

        $introWords = [
            'це' => .15,
            'це у' => .15,
            'розташована у' => .35,
            'знаходиться у' => .35
        ];
        $words[] = $intro = $this->getRand($introWords);
        $locative = !($intro == 'це');

        // district name
        $districtName = $district->title;
        if($locative) {
            $districtName = str_replace('кий', 'кому', $districtName);
        }
        $words[] = $districtName;

        $outroWords = [
            'район',
            'районі'
        ];
        $words[] = $outroWords[intval($locative)];

        return [$this->prepareSentence($words)];
    }

    /**
     * Generate metro sentence
     *
     * @return array
     */
    private function metrosDescription()
    {
        /*
         * Will start new tautologies array
         */
        $this->tautology->startNewSentence();

        if(!$this->notice->stations->count())
            return [$this->getRand([
                'будинок не розташований біля метрополітену',
                'поряд з домом немає станцій метро',
            ])];

        $metros = $this->notice->stations;

        $stationsCount = $metros->count() > 3? 4 : $metros->count();
        $words = [];

        // intro for metro description
        $introWords = [['поряд з домом', 'поруч з будинком', 'недалеко від будинку', 'близько до будинку'],
            ($stationsCount > 1)?['розташовані', 'знаходяться']:['розташована', 'знаходиться']
        ];
        foreach($introWords as $introWord) {
            $words[] = $this->getRand($introWord);
        }

        // count
        $count = [
            1 => 'станція метро',
            2 => 'дві станції метро:',
            3 => 'три станції метро:',
            4 => 'більше ніж три станції метро:'
        ];
        $words[] = $count[$stationsCount];

        // meat
        $words[] = $this->prepareStationsDescription();

        return [$this->prepareSentence($words)];
    }

    /**
     * Amenities sentence
     *
     * @return array
     */
    private function amenitiesDescription()
    {
        /*
         * Will start new tautologies array
         */
        $this->tautology->startNewSentence();

        if(!($amenities = $this->notice->highlights)) return [];

        $words = [];

        $data = [];
        foreach($amenities as $amenity) {
            $data[$amenity->id] = mb_strtolower($amenity->title);
        }

        $special = [
            5 => [
                'є вихід на балкон',
                'наявний вихід на балкон'
            ], // balcony,
            29 => [
                'поряд знаходиться парковка',
                'поряд розташована парковка',
                'поряд є паркінг',
            ], //parking
            11 => [
                'підходить для проживання родиною',
                'підійде для проживання родиною',
                'чудово підійде для проживання родиною',
                'чудове місце для життя родиною',
            ], //family
            17 => [
                'з ' . $this->getNoticeShortType(self::KIND_GENITIVE) . ' відкривається чудовий вид',
                'чудовий вид з ' . $this->getNoticeShortType(self::KIND_GENITIVE),
            ]//view
        ];

        $specialSentences = [];
        foreach(array_keys($data) as $id) {
            if(array_key_exists($id, $special)) {
                $this->tautology->startNewSentence();
                $specialSentences[] = $this->getRand($special[$id]);
                unset($data[$id]);
            }
        }

        $places = [
            21,
            25,
            34 //school
        ];
        $placesParts = [];
        foreach(array_keys($data) as $id) {
            if(in_array($id, $places)) {
                $placesParts[] = $data[$id];
                unset($data[$id]);
            }
        }
        if(count($placesParts)) {
            $this->tautology->startNewSentence();
            $plWords = [];
            $introWords = [['поряд з домом', 'поруч з будинком', 'недалеко від будинку', 'близько до будинку']];
            foreach($introWords as $introWord) {
                $plWords[] = $this->getRand($introWord);
            }
            $plWords[] = $this->prepareMultipleInstances($placesParts);
            $specialSentences[] = $this->prepareSentence($plWords);
        }

        if(count($data)) {
            $this->tautology->startNewSentence();
            $words[] = $this->getRand([
                'є',
                'наявні',
                'в комплекті'
            ]);

            $words[] = $this->prepareMultipleInstances(array_slice($data, 0, 5));
        }

        return array_merge(
            [$this->prepareSentence($words)],
            $specialSentences
        );
    }

    private function interestingStatistic()
    {
        $data = Notice::where('type', $this->notice->type)
            ->where('rooms', $this->notice->rooms)
            ->orderBy('price', 'asc')
            ->get()
            ->map(function($item){
                return [
                    'id' => $item->id,
                    'price' => $item->price
                ];
            });

        $position = 0;
        $same = 0;
        for($i = 0; $i < count($data); $i++) {
            $obj = $data[$i];
            if($obj['id'] == $this->notice->id) {
                $position = $i + 1;
                continue;
            }
            if($obj['price'] == $this->notice->price) {
                $same++;
            }
        }

        $position -= $same;
        $percent = round((100 * (count($data) - $position))/count($data));

        if($percent > 50) {
            $sentence = 'ця {current} дешевша за {percent}% всіх {all} на placely';

            $data = [
                '{current}' => $this->getNoticeShortType(),
                '{percent}' => $percent,
                '{all}' => str_replace([
                    'тна',
                    'шка',
                    'ата',
                ],[
                    'тних',
                    'шок',
                    'ат',
                ],$this->getNoticeType(false))
            ];

            return ["\n\n" .
                $this->ucfirst(str_replace(array_keys($data), array_values($data),$sentence))
            ];
        }

        return [];
    }

    /**
     * Prepare full stations description
     *
     * @return string
     */
    public function prepareStationsDescription()
    {
        $parts = [];
        $count = $this->notice->stations->count();

        foreach ($this->notice->stations as $metro) {
            $part = [];

            if($count > 1) {
                $part[] = $this->getRand([
                    'станція' => .2,
                    'метро' => .2,
                    'станція метро' => .1,
                    '' => .5
                ]);
            }

            $part[] = $metro->title;

            if($foot = $metro->pivot->foot) {
                $part[] = '('.$foot.' хвилин пішки)';
            }

            $parts[] = $this->prepareSentence($part);
        }

        return $this->prepareMultipleInstances($parts);
    }

    /**
     * Cool displaying of countable data
     *
     * @param $parts
     * @return string
     */
    private function prepareMultipleInstances($parts)
    {
        if(count($parts) == 1) {
            return $parts[0];
        }
        if(!count($parts)) {
            return '';
        }

        $finalGlue = [
            'і',
            'та'
        ];

        $lastItem = $parts[count($parts) - 1];
        $items = array_slice($parts, 0, count($parts) - 1);

        return implode(', ', $items) . ' ' .
            $this->getRand($finalGlue). ' ' . $lastItem;
    }

    /**
     * Get street name with locative
     *
     * @param $name
     * @param bool|false $locative
     * @return string
     */
    private function getStreetName($name, $locative = false)
    {
        $name = str_replace([
            ' / ',
            ' проспект',
            ' ул',
            ' бульвар',
        ], [
            '/',
            ' просп',
            ' вул',
            ' бульв',
        ], $name);

        if($locative) {
            $name = str_replace('ка ', 'кій ', $name);
        }

        $parts = array_reverse(explode(' ', $name));

        $streetTypes = [
            'пров' => [
                'провулок',
                'провулку'
            ],
            'вул' => [
                'вулиця',
                'вулиці'
            ],
            'просп' => [
                'проспект',
                'проспекті'
            ],
            'бульв' => [
                'бульвар',
                'бульварі'
            ],
            'пл' => [
                'площа',
                'площі'
            ],
            'пер' => [
                'провулок',
                'провулку'
            ],
            'узвіз' => [
                'узвіз',
                'узвозі'
            ],
        ];

        if(array_key_exists($parts[0], $streetTypes)) {
            $parts[0] = $streetTypes[$parts[0]][intval($locative)];
        }

        return implode(' ', $parts);
    }

    /**
     * Prepare sentence
     *
     * @param $words
     * @return mixed
     */
    private function prepareSentence($words)
    {
        return
            trim(
                preg_replace('/\s+/', ' ', str_replace([' ,', ' ('], [',', '('], implode(' ', $words)))
            );
    }

    private function getNoticeShortType($kind = '')
    {
        return [
            'room' => [
                '' => 'кімната',
                self::KIND_GENITIVE => 'кімнати'
            ],
            'apartment' => [
                '' => 'квартира',
                self::KIND_GENITIVE => 'квартири'
            ]
        ][$this->notice->type][$kind];
    }

    /**
     * Human likable notice type (однушка, однікімнатна)
     *
     * @param bool|true $fullName
     * @return string
     */
    private function getNoticeType($fullName = true, $short = false)
    {
        if($short) {
            return [
                'room' => 'кімнаті',
                'apartment' => 'квартирі'
            ][$this->notice->type];
        }

        $str = '';

        if($this->notice->type == 'room') {
            return 'кімната';
        }
        elseif ($this->notice->type == 'apartment') {
            $isFormal = mt_rand(1, 100) > 50;

            if($this->notice->rooms) {


                $rooms = $isFormal?[
                    1 => 'однокімнатна',
                    2 => 'двокімнатна',
                    3 => 'трикімнатна',
                    4 => 'багатокімнатна'
                ]:[
                    1 => 'однушка',
                    2 => 'двушка',
                    3 => 'трикімнатна',
                    4 => 'багатокімнатна'
                ];

                $count = ($this->notice->rooms > 4)? 4 : $this->notice->rooms;

                $str = $rooms[$count];
            }

            if(($isFormal && $fullName) || !$this->notice->rooms) {
                $str = $str . ' квартира';
            }
        }

        return trim($str);
    }

    /**
     * Implode sentences by lang rules
     *
     * @param $sentences
     * @return string
     */
    private function prepareSentences($sentences)
    {
        return implode('. ', array_map([$this, 'ucfirst'], array_filter($sentences))) . '.';
    }

    /**
     * Capitalize first UTF-8 letter in string
     *
     * @param $string
     * @return string
     */
    private function ucfirst($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
    }

    /**
     * Get random element from array with probability.
     * If no probability passed method return element from array
     * with equal probability.
     *
     * @param array $set
     * @param int $length
     * @return int|null|string
     */
    private function getRand(array $set, $length=10000)
    {
        if(array_keys($set) === range(0, count($set) - 1)) {
            $newSet = [];
            foreach($set as $word) {
                $newSet[$word] = 1/count($set);
            }
            $set = $newSet;
        }

        $set = $this->tautology->prepareSet($set);
//        var_dump($set);

        $left = 0;
        foreach($set as $num=>$right)
        {
            $set[$num] = $left + $right*$length;
            $left = $set[$num];
        }
        $test = mt_rand(1, $length);
        $left = 1;
        foreach($set as $num=>$right)
        {
            if($test>=$left && $test<=$right)
            {
                $this->tautology->addChosen($num);
//                print_r($num . '<br>');
                return $num;
            }
            $left = $right;
        }
        return array_keys($set)[array_rand(array_keys($set))];//debug, no event realized
    }
}