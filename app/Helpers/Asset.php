<?php

namespace App\Helpers;

class Asset
{
    /**
     * Get manifest data
     *
     * @return bool|mixed
     */
    public static function getManifestData()
    {
        $manifestPath = base_path('resources' . DIRECTORY_SEPARATOR . 'assets')
            . DIRECTORY_SEPARATOR . 'rev-manifest.json';

        if(!file_exists($manifestPath)) {
            return false;
        }

        return json_decode(file_get_contents($manifestPath), true);
    }

    /**
     * Get versioned asset
     *
     * @param $path
     * @return string
     */
    public static function url($path)
    {
        $manifest = self::getManifestData();
        if(env('APP_ENV') != 'local' && $manifest && is_array($manifest) && array_key_exists($path, $manifest)) {
            return url($manifest[$path]);
        }
        return url($path);
    }
}