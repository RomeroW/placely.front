<?php

namespace App\Helpers;


use App\Highlight;
use App\Notice;

class Tautology
{
    private $collectedWords = [];
    private $collectingWords = [];

    private $tautologies = [];

    public function setTautologies(array $words)
    {
        $this->tautologies = $words;
    }

    public function startNewSentence()
    {
        $this->collectedWords = $this->collectingWords;
        $this->collectingWords = [];
    }

    public function prepareSet($set)
    {
        $newSet = [];
        foreach($set as $word => $probability) {
            $probability = (bool)$this->checkTautology($word, $this->collectedWords)? .01 : $probability;
            $newSet[$word] = $probability;
        }

        return $newSet;
    }

    public function addChosen($word)
    {
        /*
         * In case this is log string
         */
        $words = explode(' ', $word);

        if($foundWord = $this->checkTautologies($words)) {
            $this->collectingWords[] = $foundWord;
        }
    }

    private function checkTautologies($words)
    {
        foreach($words as $stringWords) {
            if($tautology = $this->checkTautology($stringWords)) {
                return $tautology;
            }
        }
        return false;
    }

    private function checkTautology($word, array $check = null)
    {
        if(!is_array($check)) $check = $this->tautologies;

        foreach($check as $tautology) {
            similar_text($tautology, $word, $percent);
//            print_r(">>> $word => $tautology, {$percent}%<br>");
            if($percent > 90) {
                return $tautology;
            }
        }

        return false;
    }
}