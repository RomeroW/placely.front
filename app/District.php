<?php

namespace App;

use App\Database\Eloquent\Model;

class District extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * @return array
     */
    public static function switchTypes()
    {
        return [
            'district' => trans('general.district'),
            'microdistrict' => trans('general.microdistrict'),
        ];
    }

    /**
     * Get human type of district
     *
     * @param $key
     * @return string
     */
    public static function getType($key)
    {
        if(array_key_exists($key, self::switchTypes())) {
            return self::switchTypes()[$key];
        }

        return strtoupper($key);
    }

    public static function allJSON()
    {
        $districts = District::all();

        $data = array_map(function($item){
            return [
                'id' => (int)$item['id'],
                'value' => $item['title'],
                'type' => self::getType($item['type'])
            ];
        }, $districts->toArray());

        $result = [];

        foreach ($data as $item) {
            $result[$item['type']][] = (object)[
                'id' => (int)$item['id'],
                'value' => $item['value'],
            ];
        }


        return json_encode($result);
    }


    /**
     * Prepare model data for generating search form
     */
    public static function prepareForSearch()
    {
        $result = [
            'types' => [
                'district' => 'Адміністративний',
                'microdistrict' => 'Мікрорайон'
            ],
            'items' => []
        ];

        array_map(function($item) use (&$result){
            $result['items'][$item['type']][$item['id']] = $item['title'];
        }, District::all()->toArray());

        return $result;
    }
}