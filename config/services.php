<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id'         =>  env('FACEBOOK_APP_ID', '1511437799176954'),
        'client_secret'     =>  env('FACEBOOK_APP_SECRET', '7cf0b84e0ce9331b3f712fb2be9425b3'),
        'redirect'          =>  env('HOST') . '/auth/fb/callback',
    ],

    'vk' => [
        'client_id' => '5025816',
        'client_secret' => '4QMN3GqH5YIHOKznbt2t',
        'redirect' => env('HOST') . '/auth/vk/callback'
    ],


];
