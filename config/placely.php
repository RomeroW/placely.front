<?php

return [
    'notices_per_page' => 15,

    /*
     * List of allowed IPs for different type of actions that should be protected
     */
    'ip' => [
        'preview' => [
            env('INTERNAL_IP')?:'127.0.0.1'
        ]
    ]
];