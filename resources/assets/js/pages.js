var Pages = {
    openedInfowindow: null,
    updateMarkersCallback: null,

    all: {
        init: function() {
            $('#open-modal-auth').click(function(){
                var $authWindow = Utils.Modal.open('auth');
            });

            $('.resp-menu').click(function(e){
                if(!$(e.target).is('.resp-menu-holder') && !$(e.target).is('.resp-menu-holder *')) {
                    var $body = $('body');
                        $menu = $('.resp-menu');

                    if($body.hasClass('menu-opened')) {
                        $body.removeClass('menu-opened');
                        $menu.animate({
                            opacity: '0' // animate slideUp
                        }, 'fast', 'linear',function(){
                            $(this).removeClass('active');
                        });
                        $menu.find('.resp-menu-holder').animate({
                            left: '-100vw' // animate slideUp
                        }, 'fast', 'easeOutExpo');
                        $('.nav__btn_open_menu').removeClass('active');
                    }
                }
            });

            $('#mobile-menu').click(function(){
                var $body = $('body'),
                    $menu = $('.resp-menu');

                if(!$body.hasClass('menu-opened')) {
                    $body.addClass('menu-opened');
                    $menu.animate({
                        opacity: '1' // animate slideUp
                    }, 'fast', 'linear').addClass('active');
                    $menu.find('.resp-menu-holder').animate({
                        left: '0' // animate slideUp
                    }, 'fast', 'easeOutExpo');
                    $(this).addClass('active');
                }
                else {
                    $body.removeClass('menu-opened');
                    $menu.animate({
                        opacity: '0' // animate slideUp
                    }, 'fast', 'linear',function(){
                        $(this).removeClass('active');
                    });
                    $menu.find('.resp-menu-holder').animate({
                        left: '-100vw' // animate slideUp
                    }, 'fast', 'easeOutExpo');
                    $(this).removeClass('active');
                }
            });

            window.onmousewheel = function(){};

            GA.init();
            FacebookPopup.init();
        }
    },
    index: {
        infiniteScroll: function() {
            var $feedList = $('.feed__list');

            if(window.scrollEnabled
                && (window.scrollTimer == null && $(window).scrollTop() + $(window).height() >= $feedList.height() + $feedList.offset().top - 600)) {

                scrollTimer = 1;
                if (window.searchString === undefined) searchString = '/notice/';

                $.get(searchString + scrollPage, function (data) {
                    if(typeof data.items !== 'undefined') {
                        data = data.items;
                    }

                    for (var i in data) {
                        var item = data[i];

                        if($('.masonry-wrap').length) {
                            $('.masonry-wrap').append(UINotice.render(item));
                        }
                        else {
                            $('.feed__list').append(UINotice.render(item));
                        }

                        IndexMap.fixMap();
                        $('.feed__list').masonry();
                    }

                    if(!data.length) {
                        window.scrollEnabled = false;
                        // {show.footer}
                    }
                    else {
                        IndexMap.updateFeedMarkerCallback();

                        if(Pages.updateMarkersCallback) {
                            Pages.updateMarkersCallback();
                        }
                    }

                    scrollPage++;
                    scrollTimer = setTimeout(function() { scrollTimer = null; }, 500);
                });
            }
        },
        initInfiniteScroll: function(){
            window.scrollTimer = null;
            window.scrollPage = 2;
            window.scrollEnabled = true;

            $(window).scroll(this.infiniteScroll);
        },
        init: function() {
            this.initInfiniteScroll();
            Search.init();

            $(window).on("resize", function() {
                $('.feed__list').masonry();
            }).trigger("resize");

            var $header = $('header'),
                options = {
                addClass: 'overmap shadowed',
                $collapseAnchor: $('.feed__holder')
            };

            if($header.data('index')) {
                options.removeClass = 'transparent';
                options.addClass = 'fixed overmap shadowed';
                options.holder = $header.data('index');
            }

            $header.fixedHeader(options);

            google.maps.event.addDomListener(window, 'load', IndexMap.init);
        }
    },
    form: {
        initAutocomplete: function() {
            if (window.metros === undefined) metros = [];
            if (window.streets === undefined) streets = [];

            /**
             * Autocomplete for metro stations input
             */
            var $metroList = $('.metro__list');
            var $metroErrorMessage = $('.modal__station .fields__validation_error-metro');

            /*
             * Define callbacks for metro ac
             */
            var metroItemTimerClick = function(){
                var $stationModal = Utils.Modal.open('station'),
                    $modalHeader = $stationModal.find('.modal__head span'),
                    $modalDone = $stationModal.find('.btn__fine'),
                    $modalInputFoot = $stationModal.find('.modal__body .modalOnFoot'),
                    $modalInputTransport = $stationModal.find('.modal__body .modalOnTransport');


                var item = $(this).parent(),
                    type = item.attr('class'),
                    name = item.find('.txt').text(),
                    onFoot = item.find('.onFoot').val(),
                    onTransport = item.find('.onTransport').val();

                $modalInputFoot.val('');
                $modalInputTransport.val('');

                $modalInputFoot.val(onFoot);
                $modalInputTransport.val(onTransport);

                $modalInputFoot.keydown(metroInputRemoveError);
                $modalInputTransport.keydown(metroInputRemoveError);

                $modalHeader.attr('class', 'station__small-' + type);
                $modalHeader.text(name);

                $stationModal.find('.fields__row').removeClass('fields-error');

                $modalDone.unbind('click');
                $modalDone.click(function(){
                    var status = FormValidation.preventMetroSubmit();

                    if (status) {
                        item.find('.onFoot').val($modalInputFoot.val());
                        item.find('.onTransport').val($modalInputTransport.val());

                        Utils.Modal.close('station');
                    } else {

                        $metroErrorMessage.fadeTo(0, 1);
                    }
                });
            };
            var metroItemCrossClick = function(){
                $(this).parent().remove();
            };

            var metroInputRemoveError = function () {
                var $fieldHolder = $(this).parent().parent();

                if($fieldHolder.length && $fieldHolder.hasClass('fields-error')) {
                    $fieldHolder.removeClass('fields-error');

                    $metroErrorMessage.fadeTo(0, 0)
                }
            };

            /*
             * Put callbacks on outputted stations
             */
            $metroList.find('li').each(function(){
                $(this).find('.timer').click(metroItemTimerClick);
                $(this).find('.cross').click(metroItemCrossClick);
            });

            $('#metro').acomplete({
                source: metros,
                $resultsHolder: $metroList,
                name: "metro",
                hintItem: function( ul, item ) {
                    return $( "<li>" )
                        .append( "<a class='" + item.type + "'>" + item.label + "</a>" )
                        .appendTo( ul );
                },
                resultItem: function(event, ui) {
                    return '<li class="'+ui.item.type+'">' +
                        '<div class="txt">'+ui.item.label+'</div>' +
                        '<div class="timer"></div>' +
                        '<div class="cross"></div>' +
                        '<input type="hidden" class="stationId" name="'+this.name+'['+ui.item.id+'][id]" value="'+ui.item.id+'"/>' +
                        '<input type="hidden" class="onFoot" name="'+this.name+'['+ui.item.id+'][foot]"/>' +
                        '<input type="hidden" class="onTransport" name="'+this.name+'['+ui.item.id+'][transport]"/>' +
                        '</li>';
                },
                helper: {
                    init: function (src) {
                        this.updateCallbacks();
                    },
                    updateCallbacks: function () {
                        var $deleteButtons = $('.metro__list li .cross');
                        var $timerButtons = $('.metro__list li .timer');

                        $deleteButtons.unbind('click').click(metroItemCrossClick);
                        $timerButtons.unbind('click').click(metroItemTimerClick);
                    }
                }
            });

            /**
             * Autocomplete for highlights input
             */

            var $highlightsList = $('.highlights__list');

            /*
             * Define callbacks for highlight ac
             */
            var highlightItemCrossClick = function(){
                $(this).parent().remove();
            };

            /*
             * Put callbacks on outputted stations
             */
            $highlightsList.find('li').each(function(){
                $(this).find('.cross').click(highlightItemCrossClick);
            });

            AmenitiesSelect.init();

            /**
             * Autocomplete for streets input
             */
            $('#street').simpleComplete({
                source: streets,
                hintItem: function( ul, item ) {
                    return $( "<li>" )
                        .append( "<a>" + item.label + "</a>" )
                        .appendTo( ul );
                },
                select: function(event, ui) {
                    $('#streetId').val(ui.item.id);
                    Pages.form.updateMainMapMarker(14, "Kyiv, " + ui.item.value);
                }
            });

        },
        initSwitch: function() {

            var $formSelectType = $('.form__select_item');

            /*
             * Load switch state, if it set before
             */
            if (window.switchType != undefined) {
                $('.fields__row[data-id]').hide();
                $('.fields__row[data-id="'+window.switchType+'"]').show();

                $formSelectType.removeClass('active');
                $('.form__select_item[data-id="'+window.switchType+'"]').addClass('active');

                $('[data-target="switch"]').prop('disabled', true);
                $('[data-id="'+window.switchType+'"] [data-target="switch"]').prop('disabled', false);

                $('#type').val(window.switchType);
            }

            $formSelectType.click(function(){
                if($(this).hasClass('active')) return;

                var id = $(this).data('id');

                $('.fields__row[data-id]').hide();
                $('.fields__row[data-id="'+id+'"]').show();

                $('.form__select_item').removeClass('active');
                $(this).addClass('active');

                $('[data-target="switch"]').prop('disabled', function(i, v) { return !v; });

                $('#type').val(id);
            });
        },
        initMaps: function() {
            /**
             * Initialize maps
             */

            $('#new-map-canvas').googleMaps({
                center: new google.maps.LatLng(44.5403, -78.5463),
                zoom: 12,
                scrollwheel: false
            });

            $('#map-modal').googleMaps({
                center: new google.maps.LatLng(44.5403, -78.5463),
                zoom: 12,
                scrollwheel: false
            });

            if (window.mapPosition === undefined) {
                this.updateMainMapMarker(12, "Kyiv");
            }
            else {
                this.updateMainMapMarker(14, "Kyiv", new google.maps.LatLng(window.mapPosition.lat, window.mapPosition.lng));
            }

            /**
             * Update map location on select house number
             */
            var finalMapUpdate = function(){
                Pages.form.updateMainMapMarker(14, "Kyiv, " + $('#street').val() + ", " + $(this).val());
            };

            $('#house')
                .focusout(finalMapUpdate)
                .keypress(function(e){
                    if(e.keyCode == '13') {
                        e.preventDefault();
                        Pages.form.updateMainMapMarker(14, "Kyiv, " + $('#street').val() + ", " + $(this).val());
                    }
                });
        },
        updateMainMapMarker: function(zoomLevel, address, point) {
           if(typeof point !== 'undefined') {
               window.mapCenter = point;
               Pages.form.updateMainMap();
           }
           else {
               var geocoder = new google.maps.Geocoder();

               geocoder.geocode({
                   "address": address
               }, function(results, status){

                   window.mapCenter = results[0].geometry.location;
                   Pages.form.updateMainMap();
               });
           }
        },
        updateDistrictSelect: function() {
            var $district = $('#districts'),
                $districtsList = $('.districts__list');

            if($districtsList.find('li').length == 2) {
                $district.hide();
            }
            else {
                $district.show();
            }
        },
        initDistricts: function() {
            if (window.districts === undefined) districts = [];

            var $district = $('#districts'),
                $districtsList = $('.districts__list');

            /*
             * Fill districts select
             */
            $.each(districts, function (i, item) {
                var $optgroup = $('<optgroup>', {
                    label : i
                });

                $district.append($optgroup);

                $.each(item, function (j, district) {
                    var $option = $('<option>', {
                        value: district.id,
                        text: district.value
                    });
                    $option.attr('data-district', i);

                    $optgroup.append($option);
                });
            });

            /*
             * Add callbacks on existed districts and hide used districts groups
             */
            var crossClick = function(){
                $district.find('optgroup[label="'+$(this).parent().data('district')+'"]').show();
                $(this).parent().remove();
                Pages.form.updateDistrictSelect();
            };
            $districtsList.find('.cross').click(crossClick);

            $districtsList.find('li').each(function(){
                var $optgroup = $district.find('optgroup[label="'+$(this).data('district')+'"]');
                $optgroup.hide();
            });

            this.updateDistrictSelect();

            /*
             * Event handling on district selection
             */
            $district.change(function(){
                var $item = $(this).find('option:selected');

                var $optgroup = $district.find('optgroup[label="'+$item.data('district')+'"]');
                $optgroup.hide();

                var $elem = $('<li data-district="'+$item.data('district')+'">' +
                    '<div class="txt">' + $item.text() + ' <span>(' + $item.data('district') + ')</span>' + '</div>' +
                    '<div class="cross"></div>' +
                    '<input type="hidden" name="districts[]" value="'+$(this).val()+'"/>' +
                    '</li>');

                $elem.find('.cross').click(crossClick);

                $('.districts__list').append($elem);
                $(this).val($(this).find('option:first').val());

                Pages.form.updateDistrictSelect();
            });

        },
        updateMainMap: function() {
            var $mapDiv = $('#new-map-canvas'),
                fullMap = $mapDiv.data('map');

            fullMap.setCenter(window.mapCenter);
            fullMap.setZoom(14);

            if($mapDiv.data('marker')) {
                $mapDiv.data('marker').setMap(null);
            }

            $mapDiv.data('marker', new google.maps.Marker({
                position: window.mapCenter,
                map: fullMap
            }));

            $('#mapLat').val(window.mapCenter.lat());
            $('#mapLng').val(window.mapCenter.lng())
        },

        validationFields: function() {
            FormValidation.init();
        },

        init: function() {

            $('header').fixedHeader({
                addClass: 'overmap shadowed',
                $collapseAnchor: $('.form__general')
            });

            this.initSwitch();
            this.initMaps();
            this.initAutocomplete();
            this.initDistricts();

            DescriptionEditor.init();
            FloatingLabels.init();
            FloatingLabels.validationErrors();

            this.validationFields();

            if(typeof config != undefined) {
                $('.photo__holder').placelyDropzone({
                    url: config.attachment,
                    deleteUrl: config.attachment,
                    previewsContainer: '.photo__list',
                    $mainPreview: $('input[name="mainPreview"]')
                });
            }

            $('#correct-map').click(function(){
                Utils.Modal.open('map');

                var fullMap = $('#new-map-canvas').data('map'),
                    modalMap = $('#map-modal').data('map');

                google.maps.event.trigger(modalMap, 'resize');

                modalMap.setCenter(window.mapCenter);
                modalMap.setZoom(14);

                var marker = new google.maps.Marker({
                    position: window.mapCenter,
                    map: modalMap
                });

                google.maps.event.addListener(modalMap, 'click', function(event) {
                    marker.setMap(null);
                    placeMarker(event.latLng);
                });

                function placeMarker(location) {
                    marker = new google.maps.Marker({
                        position: location,
                        map: modalMap
                    });
                }

                $('.modal__map .btn__fine').click(function(){
                    window.mapCenter = marker.position;
                    marker.setMap(null);
                    Utils.Modal.close('map');

                    Pages.form.updateMainMap();
                });
            });
        }
    },
    settings: {
        init: function(){
            var $info = $('.data-stored');

            if($info.length) {
                $info.delay(1000).fadeTo(500, 0);
            }

            FloatingLabels.init();
            FloatingLabels.validationErrors();

            $('[name="phone"]').mask("+38 (999) 999 99 99");
        }
    },
    one: {
        init: function() {
            if (window.oneLat === undefined) oneLat = 44.5403;
            if (window.oneLng === undefined) oneLng = -78.5463;

            var $map = $('#new-map-canvas');

            $map.googleMaps({
                center: new google.maps.LatLng(oneLat, oneLng),
                zoom: 15,
                scrollwheel: false
            });

            $map.data('marker', new google.maps.Marker({
                position: new google.maps.LatLng(oneLat, oneLng),
                map: $map.data('map')
            }));

            $('header').fixedHeader({
                addClass: 'overmap shadowed',
                $collapseAnchor: $('.top')
            });

            $('.photos__nav').perfectScrollbar();

            var $notices = $('.data__notices--masonry');
            if($notices.length) {
                $(window).on("resize", function() {
                    $notices.masonry();
                }).trigger("resize");
            }

            HiddenContacts.init();

            $('.gallery__overlay, .gallery__navigation, .gallery__title').click(function(){
                var params = {
                    padding: 0,
                    margin: 20,
                    helpers:  {
                        thumbs : {
                            width: 50,
                            height: 50
                        }
                    },
                    nextEffect: 'none',
                    prevEffect: 'none'
                };

                if(Device.isMobile()) {
                    params.margin = 10;
                }

                $.fancybox.open($('.fancybox'), params);
            });

            FacebookInstance.init();
            FacebookInstance.onload(function(){
                FB.Event.subscribe('xfbml.render', function(response) {
                    if(!Device.isMobile()) {
                        $('.fb-page-holder').animate({
                            'opacity': 1
                        }, 'slow');
                    }
                });
            });
        }
    },
    blogItem: {
        init: function() {
            FacebookInstance.init();

            SocialInstance.shares();
            //SocialInstance.onload(function(){
            //    VK.init({
            //        apiId: 5025816,
            //        onlyWidgets: true
            //    });
            //
            //    VK.Widgets.Like("vk-like", {      // Добавление кнопки "мне нравится" для элемента #DIVID
            //        type: "button"
            //    });
            //});
        }
    }
};