var Utils = {
    Modal: {
        init: function() {
            $('.modal__head .cross').click(function(){
                Utils.Modal.hide($(this).parent().parent());
            });
        },
        open: function (id) {
            var $modal = $('.modal__' + id);
            if(!$modal.length) {
                return false;
            }

            $('.modal').show();
            $modal.show();

            $('body').css('overflow', 'hidden');

            return $modal;
        },
        close: function(id) {
            var $modal = $('.modal__' + id);
            if(!$modal.length) {
                return false;
            }

            $modal.hide();
            $('.modal').hide();

            $('body').css('overflow', 'auto');

            return true;
        },
        hide: function($modal) {
            if(!$modal.length) {
                return false;
            }

            $modal.hide();
            $('.modal').hide();

            $('body').css('overflow', 'auto');

            return true;
        },

        confirm: {
            msg: false,
            yesCallback: false,
            noCallback: function() {
                if(Utils.Modal.confirm.$modal.length) {
                    Utils.Modal.hide(Utils.Modal.confirm.$modal);
                }
            },

            $modal: false,

            init: function(msg, yesCallback, noCallback) {
                if(msg) this.msg = msg;
                if(yesCallback) this.yesCallback = yesCallback;
                if(noCallback) this.noCallback = noCallback;

                $('#yes').click(this.yesCallback);
                $('#no, .modal__confirm .cross').click(this.noCallback);
            },

            run: function() {
                Utils.Modal.confirm.$modal = Utils.Modal.open('confirm');
            }
        }
    }
};