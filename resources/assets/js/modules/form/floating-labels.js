var FloatingLabels = {
    init: function() {
        var $input = $('.floating-input');

        $('[data-only-digits]').keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                    // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('[data-only-time]').keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                    // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $input.keydown(function (e) {
            var $fieldHolder = $(this).parent(),
                $validationErrorMessage = $('.fields__validation_error');
            if($fieldHolder.length && $fieldHolder.hasClass('floating-error')) {
                $fieldHolder.removeClass('floating-error');

                if($validationErrorMessage.length)
                    $validationErrorMessage.fadeTo(0, 0);
            }
        });

        var getLabel = function($el) {
            var label = '';
            if($el.data('label')) {
                return $('#' + $el.data('label'));
            }

            return $el.parent().find('.floating-label');
        };

        $input.each(function(){
            $(this).data('holder', $(this).attr('placeholder'));
            if($(this).val().length != 0) {
                getLabel($(this)).css('opacity', 1);
            }
        });

        $input.focusin(function(){
            $(this).attr('placeholder', '');
            getLabel($(this)).css('opacity', 1);
        });

        $input.focusout(function(){
            $(this).attr('placeholder', $(this).data('holder'));
            if($(this).val().length == 0) {
                getLabel($(this)).css('opacity', 0);
            }
        });
    },

    validationErrors: function(){
        if(window.validationErrors != undefined) {
            for(var key in window.validationErrors) {
                var msg =window.validationErrors[key];
                var $fieldHolder = $('[data-item="'+key+'"]');

                if($fieldHolder.length) {
                    $fieldHolder.addClass('floating-error');
                    $fieldHolder.find('.floating-validation').text(msg);
                }
            }
        }
    }
};