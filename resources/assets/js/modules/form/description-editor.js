var DescriptionEditor = {

    $editor: $('.de'),
    $brief: $('#ta-brief'),
    $description: $('#ta-description'),

    /*
     * Module entry point
     */
    init: function () {
        this.bindUIActions();
    },

    /*
     * All UI related stuff here
     */
    bindUIActions: function () {
        this.$brief.autoGrow();
        this.$description.autoGrow();

        this.initCounter(this.$brief);
        this.initCounter(this.$description);

        this.briefActions();
        this.descriptionActions();

        FormValidation.addValidator(DescriptionEditor.validate);

        $('.de__info').mousedown(function(e){
            $(this).parent().find('textarea')[0].focus();
            if(document.activeElement.id == 'ta-brief' || document.activeElement.id == 'ta-description') {
                e.preventDefault();
            }
        });
    },

    valid: function($el) {
        var result = true;

        if($el.data('max')) {
            result = result && ($el.val().length <= $el.data('max'));
        }

        return result;
    },

    setError: function(msg) {
        DescriptionEditor.$editor.addClass('non-valid floating-error');
        $('[data-item="description"]').addClass('floating-error');
        var $error = DescriptionEditor.$editor.next();
        $error.html('&nbsp;' + msg);
        $error.css('opacity', 1);
    },

    removeError: function() {
        if(this.$editor.hasClass('non-valid')) {
            this.$editor.removeClass('non-valid floating-error');
            this.$editor.next().html('&nbsp;');
            this.$editor.next().css('opacity', 0);
            $('[data-item="description"]').removeClass('floating-error');
            $('.form__general .fields__validation_error').css('opacity', 0);
        }
    },

    validate: function() {
        var validated = DescriptionEditor.valid(DescriptionEditor.$brief)
            && DescriptionEditor.valid(DescriptionEditor.$description);

        if(!validated) {
            DescriptionEditor.setError('Перевищений ліміт символів');
            return false;
        }

        if((DescriptionEditor.$brief.val().trim().length == 0
            && DescriptionEditor.$description.val().trim().length == 0)) {
            DescriptionEditor.setError('Поле обов\'язкове для заповнення');
            return false;
        }

        return true;
    },

    initCounter: function($el) {
        var $counter = $('[data-count="' + $el.attr('id') + '"]');
        if(!$counter.length) return;

        var $info = $counter.parent();

        var listen = function(e) {
            $counter.find('span').text($(this).val().length);

            if($el.val().length > $el.data('max')) {
                if(!$info.hasClass('non-valid')) {
                    $info.addClass('non-valid');
                }
            }
            else {
                if($info.hasClass('non-valid')) {
                    $info.removeClass('non-valid');
                }
            }

            DescriptionEditor.removeError();
        };

        $(document).ready(function(){
            $counter.find('span').text($el.val().length);

            if($el.val().length > $el.data('max')) {
                if(!$info.hasClass('non-valid')) {
                    $info.addClass('non-valid');
                }
            }
            else {
                if($info.hasClass('non-valid')) {
                    $info.removeClass('non-valid');
                }
            }
        })
        $el.on('change keyup paste', listen);
    },

    briefActions: function() {

        var check = function(){
            if(document.activeElement.id == 'ta-brief' || document.activeElement.id == 'ta-description'
                || DescriptionEditor.$brief.val().length > 0 || DescriptionEditor.$description.val().length) {
                DescriptionEditor.$brief.attr('placeholder', '');
            }
            else {
                DescriptionEditor.$brief.attr('placeholder', $('#description-label').text());
            }
        };

        this.$brief.focusin(check);
        this.$brief.focusout(check);
        this.$description.focusin(check);
        this.$description.focusout(check);
    },

    descriptionActions: function() {

    }
};