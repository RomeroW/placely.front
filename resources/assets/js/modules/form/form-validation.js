var FormValidation = {

    /**
     * Custom validators
     */
    validators: [],

    init: function () {
        this.bindUIActions();
    },

    bindUIActions: function () {
        $('.btn__submit-notice').click(function (event) {
            FormValidation.preventSubmit(event);
        });
    },

    addValidator: function(mthd){
        if(typeof mthd === "function") {
            this.validators.push(mthd);
        }
        else {
            console.error('Custom validator is not a function', mthd);
        }
    },

    preventMetroSubmit: function () {
        var $timingFields = $('[data-val-timing]'),
            validators = FormValidation.validationRules;

        return FormValidation.validateFields(validators, $timingFields);
    },

    preventSubmit: function (event) {
        if(FormValidation.validators.length) {
            FormValidation.validators.forEach(function(item, i){
                if(!item()) event.preventDefault();
            });
        }

        var $required = $('[data-val-required]'),
            $optional = $('[data-val-optional]'),
            validators = FormValidation.validationRules;

        var requiredStatus = FormValidation.validateFields(validators, $required),
            optionalStatus = FormValidation.validateFields(validators, $optional);

        if (!(requiredStatus && optionalStatus)) {
            event.preventDefault();
            FormValidation.scrollErrorField();
        }
    },

    validateFields: function (validators, $fields) {
        var status = true;

        for (var key in validators) {
            if (!validators.hasOwnProperty(key)) {
                continue;
            }

            for (var i = 0; i < $fields.length; i++) {
                if (!validators[key]($fields.eq(i))) {
                    $fields.splice(i, 1);

                    $('.form__general .fields__validation_error').fadeTo(200, 1);

                    status = false;
                    i--;
                }
            }
        }

        return status;
    },

    validationRules: {

        fieldLength: function ($element) {
            if (!$element.data('valLength')) {
                return true;
            }

            var message,
                minLength = $element.data('valLength')[0],
                maxLength = $element.data('valLength')[1],
                $elementInput = $element.find('input').length > 0 ?
                        $element.find('input') : $element.find('textarea');

            var minCondition = (minLength) ? $elementInput.val().length < minLength : false,
                maxCondition = (maxLength) ? $elementInput.val().length > maxLength : false;

            if (minCondition) {
                if ($element.hasClass('floating-data')) {
                    $element.addClass('floating-error');

                    message = "Поле обов’язкове для заповнення";
                    $element.find('.floating-validation').text(message);
                } else {
                    $element.addClass('fields-error')
                }

                return false

            }
            else if (maxCondition) {
                if ($element.hasClass('floating-data')) {
                    $element.addClass('floating-error');

                    message = "Максимальна довжина поля - " + maxLength.toString();
                    $element.find('.floating-validation').text(message);
                } else {
                    $element.addClass('fields-error')
                }

                return false
            } else {
                $element.removeClass('floating-error');
                $element.removeClass('fields-error');

                return true
            }

        },

        fieldAutocomplete: function($element) {
            if ($element.data('valAutocomplete') != "") {
                return true;
            }

            var message,
                $elementValue = $element.find('input[type="hidden"]');

            if ($elementValue.val().length == 0) {
                $element.addClass('floating-error');

                message = "Виберіть вулицю із випадаючого списку";
                $element.find('.floating-validation').text(message);

                return false
            } else {
                $element.removeClass('floating-error');

                return true
            }
        },

        fieldOnlyDigits: function($element) {
            if ($element.children('.floating-input').data('onlyDigits') === undefined) {
                return true;
            }

            var message,
                $elementInput = $element.find('input');

            if (isNaN($elementInput.val()) || ($elementInput.val().length > 0 && $elementInput.val() < 1)) {
                $element.addClass('floating-error');

                message = 'Введіть дійсне число';
                $element.find('.floating-validation').text(message);

                return false;
            } else {
                $element.removeClass('floating-error');

                return true
            }
        }
    },

    scrollErrorField: function () {
        $('html, body').animate({
            scrollTop: $('.floating-error').offset().top - 80
        }, 500)
    }

};