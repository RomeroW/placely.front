var AmenitiesSelect = {

    /*
     * Module entry point
     */
    init: function () {
        this.bindUIActions();
    },

    /*
     * All UI related stuff here
     */
    bindUIActions: function () {

        $('#am__select').click(AmenitiesSelect.modalClicked);
        $('.am__item').click(AmenitiesSelect.actionAmenity);
        $('.highlights__list li .cross').click(AmenitiesSelect.removeAmenityDom);

        var $modalAm = $('.modal__am');
        $modalAm.find('.btn__fine').click(AmenitiesSelect.modalClosed);
        $modalAm.find('.cross').click(AmenitiesSelect.modalClosed);

    },

    modalClicked: function () {
        Utils.Modal.open('am');
        AmenitiesSelect.refreshCounter();
    },

    modalClosed: function () {
        Utils.Modal.close('am');
    },

    actionAmenity: function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            AmenitiesSelect.removeAmenityDom($(this).attr('data-id'));
        } else {
            $(this).addClass('active');
            AmenitiesSelect.resultAmenity($(this));
        }
    },

    refreshCounter: function () {
        var $container = $('.description');

        var amenityCounter = $('.am__item.active').length;

        if (amenityCounter == 0) {
            $container.hide();
        } else {
            $container.show();
        }

        $container.text('(' + amenityCounter.toString() + ' обрано)');
    },

    removeAmenityDom: function (id) {

        id = ((typeof id !== undefined) && (typeof id != 'object')) ? id : $(this).parent().find('input').val();
        var $element = $('.highlights__list').find($('input[value="' + id + '"]'));

        $('.am__item[data-id="' + id + '"]').removeClass('active');

        $element.parent().remove();
        AmenitiesSelect.refreshCounter();
    },

    resultAmenity: function ($amenity) {
        var container = $('.highlights__list');

        var id = $amenity.data('id'),
                icon = $amenity.find('i').attr('class'),
                title = $amenity.find('span').text(),
                description = $amenity.attr('title');

        var result = '<li title="' + description + '">' +
                '<div class="txt"><i class="' + icon + '"></i><span>' + title + '</span></div>' +
                '<span class="cross"></span>' +
                '<input type="hidden" name="highlight[]" value="' + id + '"/>' +
                '</li>';

        container.append(result);

        $('input[value="' + id + '"]').prev().click((function (id) {
            return function () {
                AmenitiesSelect.removeAmenityDom(id)
            };
        })(id));

        AmenitiesSelect.refreshCounter();
    }
};