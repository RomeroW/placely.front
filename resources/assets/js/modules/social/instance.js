var SocialInstance = {

    inited: false,

    vkCallbacks: [],

    /*
     * Module entry point
     */
    init: function () {
        if(!this.inited) {
            this.inited = true;

            (function(w, d) {
                var u = ['//vk.com/js/api/openapi.js?121'],

                    i = u.length,
                    n = 'script',
                    f = d.createDocumentFragment(),
                    e = d.createElement(n),
                    t;

                while (i--) {
                    t = e.cloneNode(false);
                    t.async = t.src = u[i];
                    f.appendChild(t);
                }

                (t = d.getElementsByTagName(n)[0]).parentNode.insertBefore(f, t);
            }(this, document));

            window.vkAsyncInit = SocialInstance.runVkCallbacks;
        }
    },

    /**
     * Add function to onload (run when sn is ready)
     *
     * @param func
     */
    onload: function(func) {
        SocialInstance.vkCallbacks.push(func);
    },

    /**
     * Run callback when fb is loaded
     */
    runVkCallbacks: function() {
        SocialInstance.vkCallbacks.forEach(function(item){
            if(typeof item == "function") {
                item();
            }
        });
    },

    shares: function(){
        $('a.share').click(function(e){
            e.preventDefault();
            SocialInstance.popup($(this).attr('href'));
        });
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }

};