var FacebookInstance = {

    inited: false,

    callbacks: [],

    /*
     * Module entry point
     */
    init: function () {
        if(!this.inited) {
            this.inited = true;

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/uk_UA/sdk.js#xfbml=1&version=v2.5&appId=570945146390812";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            window.fbAsyncInit = FacebookInstance.runCallbacks;
        }
    },

    /**
     * Add function to onload (run when fb is ready)
     *
     * @param func
     */
    onload: function(func) {
        FacebookInstance.callbacks.push(func);
    },

    /**
     * Run callback when fb is loaded
     */
    runCallbacks: function() {
        FacebookInstance.callbacks.forEach(function(item){
            if(typeof item == "function") {
                item();
            }
        });
    }

};