var FacebookPopup = {

    $elem: $('.fb__popup'),
    $close: $('.fb__popup .cross'),
    $like: $('.fb__popup .fb-like'),
    cname: 'fb_popup_closed',
    cvisits: 'fb_pages_visited',

    /*
     * Module entry point
     */
    init: function () {
        this.bindUIActions();
        if(!Cookie.check(this.cvisits)) {
            Cookie.set(this.cvisits, 0);
        }
        Cookie.set(this.cvisits, parseInt(Cookie.get(this.cvisits)) + 1);
    },

    fbInit: function() {
        FacebookInstance.init();

        /**
         * Bind UI when FB SDK is ready
         */
        FacebookInstance.onload(function() {
            FacebookPopup.bindFBActions();
        });
    },

    /*
     * All UI related stuff here
     */
    bindUIActions: function () {
        if(!Cookie.check(this.cname) && !Device.isMobile() && this.visitedEnough()) {

            // popup will be shown there
            this.fbInit();

            this.$close.click(function(){
                if(!Cookie.check(FacebookPopup.cname)) {
                    Cookie.set(FacebookPopup.cname, 1, 4);
                }
                FacebookPopup.$elem.animate({
                    opacity: 0, // animate slideUp
                    right: '-300px'
                }, 'slow', 'easeOutCubic', function(){
                    $(this).hide();
                });
                ga('send', 'event', 'FB Popup', 'Close');
            });
        }
    },

    /*
     * All UI related stuff here
     */
    bindFBActions: function () {
        FB.Event.subscribe('edge.create', function(url, html_element) {
            Cookie.set(FacebookPopup.cname, 1, 30);
            ga('send', 'event', 'FB Popup', 'Like');
        });

        FB.Event.subscribe('edge.remove', function(url, html_element) {
            ga('send', 'event', 'FB Popup', 'Unlike');
        });

        if(!Cookie.check(this.cname) && !Device.isMobile() && this.visitedEnough()) {
            FB.Event.subscribe('xfbml.render', function(response) {
                FacebookPopup.$elem.show().animate({
                    opacity: 1, // animate slideUp
                    right: '50px'
                }, 'slow', 'easeOutCubic');
                ga('send', 'event', 'FB Popup', 'Show');
            });
        }
    },

    visitedEnough: function() {
        return parseInt(Cookie.get(this.cvisits)) + 1 >= 3;
    }
};