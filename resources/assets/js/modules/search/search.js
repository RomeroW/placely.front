var Search = {

    settings: {},

    /**
     * Data for search
     */
    data: {},

    initialData: {},

    /*
     * Module entry point
     */
    init: function() {
        this.bindUIActions();

        this.initialData =  JSON.parse(JSON.stringify(this.data));
    },

    /*
     * All UI related stuff here
     */
    bindUIActions: function() {
        SearchUISwitchers.init();
        SearchUIInputs.init();
        SearchUIFull.init();

        if(window.asyncSearch) {
            $('.search-submit-btn').click(Search.asyncSearch);
        }
    },

    /**
     * Set search data
     * @param key
     * @param value
     */
    setData: function(key, value) {
        if(typeof value === 'undefined' || value === null) {
            if(this.data.hasOwnProperty(key)) {
                delete this.data[key];
            }
        }
        else {
            this.data[key] = value;
        }

        $(this).trigger('searchDataChanged');
    },

    /**
     * Get search data
     * @param key
     * @param type Used for separating types of returned data
     */
    getData: function(key, type) {
        if(!this.data.hasOwnProperty(key))
            return null;

        switch(type) {
            case "info":
                if(key === 'p') {
                    var p = this.data['p'].split(':');
                    return SearchUIInputs.price.getInfo(p[0], p[1]);
                }
                else {
                    return (this.data[key] instanceof Array)? this.data[key].length : this.data[key];
                }
                break;
            case "searchString":
                return (this.data[key] instanceof Array)? this.data[key].join(',') : this.data[key];
            default:
                return this.data[key];
        }
    },

    asyncSearch: function(e) {
        e.preventDefault();

        if(!$(this).hasClass('active')) {
            return;
        }

        SearchUIFull.showAsyncProgress();
        Search.updateSearchString($(this));

        $.get(window.location.href.split("/")[0] + $(this).attr('async-action') + '?' + Search.getRequestString(),
            Search.processResponse);
    },

    /**
     * Check that search params is empty
     * @returns {boolean}
     */
    emptySearch: function() {
        return !this.getRequestString().length;
    },

    /**
     * Check that search filters was changed after last search
     * @returns {boolean}
     */
    dataHasChanged: function() {
        return !equals(Search.initialData, Search.data);
    },

    /**
     * Get request string
     * @returns {Array}
     */
    getRequestString: function(){
        var queryString = [];

        for(var i in Search.data) {
            if(Search.data.hasOwnProperty(i)) {
                queryString.push(i+'='+Search.getData(i, 'searchString'));
            }
        }

        queryString = queryString.join('&');

        return queryString;
    },

    setResultsCount: function(count) {
        $('.results-count').text(count);
    },

    /**
     * Process server response
     * @param data
     */
    processResponse: function (data) {
        var $feedHolder = $('.feed__list'),
            $feedTitle = $('.feed__title'),
            $feedEmpty = $('.feed__empty');
        $feedHolder.html('');

        $feedTitle.text($feedTitle.data('title-search'));

        Search.initialData = JSON.parse(JSON.stringify(Search.data));
        $('.search-submit-btn').removeClass('active');

        Search.setResultsCount(data.count);
        data = data.items;

        if(!data.length) {
            $feedEmpty.addClass('feed__empty-visible');
        }
        else {
            data.forEach(function(item, i) {
                if($('.masonry-wrap').length) {
                    $('.masonry-wrap').append(UINotice.render(item));
                }
                else {
                    $('.feed__list').append(UINotice.render(item));
                }

                $('.feed__list').masonry();
            });
            $feedEmpty.removeClass('feed__empty-visible');
        }

        IndexMap.fitSize();
        IndexMap.updateMarkers();
        IndexMap.updateFeedMarkerCallback();

        if(Pages.updateMarkersCallback) {
            Pages.updateMarkersCallback();
        }

        SearchUIFull.hideAsyncProgress();
    },

    updateSearchString: function($form) {
        window.scrollTimer = null;
        window.scrollPage = 2;
        window.scrollEnabled = true;

        window.searchString = $form.attr('async-action')
            + '?' + Search.getRequestString() + '&page=';

        if(typeof window.history !== 'undefined') {
            window.history.pushState("index.html", document.title, window.location.href.split("?")[0] +
                window.searchString.split("notice/search")[1].split("&page=")[0]);
        }
    }
};