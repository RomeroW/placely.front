var SearchUISwitchers = {

    settings: {
        related_tabs: [
            ['metro', 'district'],
            ['metro-red', 'metro-green', 'metro-blue'],
            ['district-district', 'district-microdistrict']
        ],
        all: []
    },

    /*
     * Module entry point
     */
    init: function() {
        SearchUISwitchers.settings.related_tabs.forEach(function(el, i){
            el.forEach(function(obj){
                SearchUISwitchers.settings.all[obj] = i;
            })
        });

        this.bindUIActions();
    },

    bindUIActions: function() {
        $('[data-s-target]').click(function(){
            if($(this).hasClass('active')) return;

            var target = $(this).data('s-target'),
                index = SearchUISwitchers.settings.all[target];

            if(index != undefined) {
                SearchUISwitchers.settings.related_tabs[index].forEach(function(el){
                    if(el != target) {
                        $('[data-s-listen="'+el+'"], [data-s-target="'+el+'"]').removeClass('active');
                    }
                    else {
                        $('[data-s-listen="'+el+'"], [data-s-target="'+el+'"]').addClass('active');
                    }
                });
            }
        });
    }
};