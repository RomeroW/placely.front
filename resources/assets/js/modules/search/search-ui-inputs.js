/**
 * Search inputs
 *
 * Search inputs builds by next HTML classes
 *
 * Full input holder: .search__input_holder-{{name}}
 * Dropdown trigger: .trigger-{{name}}
 * Dropdown itself: #search-modal-{{name}}
 *
 * Structure:
 * .search__input_holder-{{name}}
 *     .trigger-{{name}}
 *     .search-modal-{{name}}
 *
 * @type {{settings: {modals: string[]}, mouseUpSet: {}, mouseState: {}, init: Function, bindUIActions: Function, bindDropdowns: Function}}
 */
var SearchUIInputs = {

    settings: {
        modals: [
            'location',
            'price',
            'specify'
        ]
    },

    mouseUpSet: {},

    timeoutData: {},

    dropdowns: {},

    /*
     * Module entry point
     */
    init: function() {
        SearchUIInputs.settings.modals.forEach(function(el){
            SearchUIInputs.dropdowns[el] = $('#' + el);
        });

        this.bindUIActions();
    },

    bindUIActions: function() {
        this.bindDropdowns();

        this.mobile.init();

        this.location.init();
        this.price.init();
        this.specify.init();
        this.type.init();
        this.amenity.init();
        this.submit.init();
    },

    bindDropdowns: function() {

        SearchUIInputs.settings.modals.forEach(function(id){
            var $trigger = $('.trigger-' + id),
                $holder = $('.search__input_holder-' + id);

            if(!$trigger.length || !$holder.length) return;

            SearchUIInputs.addMouseHolderCallbacks(id, $holder);
            SearchUIInputs.addTriggerCallback(id, $trigger, $holder);
        });
    },

    /**
     * Add callback on mouse in and out from the drop down holder. This
     * function implements dropdown fade out when mouse is out
     * over 1.25 second
     *
     * @param id
     * @param $holder
     */
    addMouseHolderCallbacks: function(id, $holder) {
        var $dropdown = SearchUIInputs.dropdowns[id];

        $holder.mouseenter(function(){
            if(SearchUIInputs.timeoutData[id] != undefined) {
                window.clearTimeout(SearchUIInputs.timeoutData[id]);
            }
        })
            .mouseleave(function(){
                if(!$dropdown.is(':visible')) return;

                SearchUIInputs.timeoutData[id] = window.setTimeout(function () {
                    $dropdown.fadeOut(100);
                }, 1250);
            });
    },

    /**
     * Add callback on input click
     *
     * @param id
     * @param $trigger
     */
    addTriggerCallback: function(id, $trigger) {
        $trigger.click(function(){
            var $holder = $(this).parent();

            var $dropdown = SearchUIInputs.dropdowns[id];
            $dropdown.appendTo($holder);

            if(!$dropdown.is(':visible')) {
                $dropdown.show();
                if(($dropdown.offset().top - $(window).scrollTop()) + $dropdown.height() > $(window).height()) {
                    $dropdown.data('resized', '1');
                    $dropdown.css({
                        'height': $(window).height() - ($dropdown.offset().top - $(window).scrollTop()) - 10,
                        'overflow-x': 'hidden',
                        'overflow-y': 'auto'
                    });
                }

                if(SearchUIInputs.mouseUpSet[id] == undefined) {
                    $(document).mouseup(function (e) {
                        var outOfDropdownClicked = !$dropdown.is(e.target)
                                && $dropdown.has(e.target).length === 0,
                            triggerClicked = $trigger.is(e.target)
                                || $trigger.has(e.target).length;

                        if(triggerClicked) return;

                        if(outOfDropdownClicked) {
                            $dropdown.hide();
                            if($dropdown.data('resized') == '1') {
                                $dropdown.css({
                                    'height': '',
                                    'overflow-x': '',
                                    'overflow-y': ''
                                });
                            }
                        }
                    });

                    SearchUIInputs.mouseUpSet[id] = true;
                }
            }
            else {
                $dropdown.hide();
                if($dropdown.data('resized') == '1') {
                    $dropdown.css({
                        'height': '',
                        'overflow-x': '',
                        'overflow-y': ''
                    });
                }
            }
        });
    },

    mobile: {
        init: function() {
            $('.search__fake_holder').click(function(){
                Utils.Modal.open('search');
            });
        }
    },

    /**
     * Location search field actions
     */
    location: {
        data: {},

        init: function() {
            var $item = $('[data-search]'),
                loc = SearchUIInputs.location,
                dataTitles = {};

            $('[data-search].active').each(function(i, el){
                var searchStr = $(this).data('search'),
                    thisData = searchStr.split(':'),
                    ns = thisData[0],
                    val = thisData[1];

                if(loc.data[ns] == undefined) {
                    loc.data[ns] = [];
                }
                if($.inArray(val, loc.data[ns]) == -1) {
                    loc.data[ns].push(val);
                    dataTitles[searchStr] = $(this).text();
                }

                Search.setData(ns, loc.data[ns]);
            });
            SearchUIInputs.location.rebuildString(loc.data, dataTitles);

            $item.click(function(){
                var searchStr = $(this).data('search'),
                    thisData = searchStr.split(':'),
                    ns = thisData[0],
                    val = thisData[1];

                if($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    var index = $.inArray(val, loc.data[ns]);
                    if(index != -1) {
                        loc.data[ns].splice(index, 1);
                    }
                    if(loc.data[ns].length == 0) {
                        delete loc.data[ns];
                    }
                    delete dataTitles[searchStr];
                }
                else {
                    $(this).addClass('active');
                    if(loc.data[ns] == undefined) {
                        loc.data[ns] = [];
                    }
                    if($.inArray(val, loc.data[ns]) == -1) {
                        loc.data[ns].push(val);
                        dataTitles[searchStr] = $(this).text();
                    }
                }

                Search.setData(ns, loc.data[ns]);

                SearchUIInputs.location.rebuildString(loc.data, dataTitles);
            });
        },

        remove: function(ns) {
            $('[data-'+ns+'].active').removeClass('active');
            delete this.data[ns];
            this.rebuildString(this.data);
        },

        rebuildString: function(data, dataTitles) {
            var map = {
                    'm': 'метро',
                    'd': 'район'
                },
                parts = [];

            if(typeof dataTitles === 'undefined') {
                dataTitles = [];
                $('[data-search].active').each(function(){
                    dataTitles[$(this).data('search')] = $(this).text();
                });
            }

            $.each(data, function(key, value){
                var part = '';

                if(data['m'] != undefined && data['d'] != undefined) {
                    part += map[key] + ': ' + value.length + ' обрано';
                }
                else {
                    var titles = [];
                    value.forEach(function(el){
                        titles.push(dataTitles[key + ':' + el]);
                    });
                    part += titles.join(', ');
                }

                parts.push(part);
            });
            var str = parts.join('; ').replace(/ +(?= )/g,''),
                $holder = $('.trigger-location span span');

            if(!str.length) {
                $holder.removeClass();
                $holder.addClass('placeholder');
                $holder.text($holder.data('placeholder'));
                $holder.parent().parent().attr('title', '');
            }
            else {
                $holder.removeClass();
                $holder.addClass('data');
                $holder.parent().parent().attr('title', str);
                $holder.text(str);
            }

            $('#location').data('value', JSON.stringify(data));
        }
    },


    specify: {
        data: {},

        init: function() {
            var $item = $('[data-search-s]'),
                spec = SearchUIInputs.specify,
                dataTitles = {};

            $('[data-search-s].active').each(function(i, el){
                var searchStr = $(this).data('search-s'),
                    thisData = searchStr.split(':'),
                    ns = thisData[0],
                    val = thisData[1];

                if(spec.data[ns] == undefined) {
                    spec.data[ns] = [];
                }
                if($.inArray(val, spec.data[ns]) == -1) {
                    spec.data[ns].push(val);
                    dataTitles[searchStr] = $(this).text();
                }

                Search.setData(ns, spec.data[ns]);
            });
            SearchUIInputs.specify.rebuildString(spec.data, dataTitles);

            $item.click(function(){
                var searchStr = $(this).data('search-s'),
                    thisData = searchStr.split(':'),
                    ns = thisData[0],
                    val = thisData[1];

                if($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    var index = $.inArray(val, spec.data[ns]);
                    if(index != -1) {
                        spec.data[ns].splice(index, 1);
                    }
                    if(spec.data[ns].length == 0) {
                        delete spec.data[ns];
                    }
                    delete dataTitles[searchStr];
                }
                else {
                    $(this).addClass('active');
                    if(spec.data[ns] == undefined) {
                        spec.data[ns] = [];
                    }
                    if($.inArray(val, spec.data[ns]) == -1) {
                        spec.data[ns].push(val);
                        dataTitles[searchStr] = $(this).text();
                    }
                }

                Search.setData(ns, spec.data[ns]);

                SearchUIInputs.specify.rebuildString(spec.data, dataTitles);
            });
        },

        remove: function(ns) {
            $('[data-'+ns+'].active').removeClass('active');
            delete this.data[ns];
            this.rebuildString(this.data);
        },

        rebuildString: function(data, dataTitles) {
            var map = {
                    't': 'тип',
                    'a': 'зручність'
                },
                parts = [];

            if(typeof dataTitles === 'undefined') {
                dataTitles = [];
                $('[data-search-s].active').each(function(){
                    dataTitles[$(this).data('search')] = $(this).text();
                });
            }

            $.each(data, function(key, value){
                var part = '';

                if(data['t'] != undefined && data['a'] != undefined) {
                    part += map[key] + ': ' + value.length + '';
                }
                else {
                    var titles = [];
                    value.forEach(function(el){
                        titles.push(dataTitles[key + ':' + el]);
                    });
                    part += titles.join(', ');
                }

                parts.push(part);
            });

            var str = parts.join('; ').replace(/ +(?= )/g,''),
                $holder = $('.trigger-specify span span');

            if(!str.length) {
                $holder.removeClass();
                $holder.addClass('placeholder');
                $holder.text($holder.data('placeholder'));
                $holder.parent().parent().attr('title', '');
            }
            else {
                $holder.removeClass();
                $holder.addClass('data');
                $holder.parent().parent().attr('title', str);
                $holder.text(str);
            }

            $('#specify').data('value', JSON.stringify(data));
        }
    },

    price: {
        init: function() {
            var data = '';

            var from = $('[data-price-input="from"]').val(),
                to = $('[data-price-input="to"]').val();

            SearchUIInputs.price.rebuildString(from, to);

            $('[data-price]').click(function(){
                data = $(this).data('price');

                $('[data-price]').removeClass('active');
                $(this).addClass('active');

                var dataSplit = data.split(':'),
                    from = !!dataSplit[0]? dataSplit[0] : false,
                    to = !!dataSplit[1]? dataSplit[1] : false;

                $('[data-price-input="from"]').val(from? from : '');
                $('[data-price-input="to"]').val(to? to : '');

                SearchUIInputs.price.rebuildString(from, to);
            });

            $('[data-price-input]').keyup(function(e){
                this.value = this.value.replace(/[^0-9\.]/g,'');

                var from = $('[data-price-input="from"]').val(),
                    to = $('[data-price-input="to"]').val(),
                    $cb = $('[data-price="' + from + ':' + to + '"]');

                $('[data-price]').removeClass('active');
                if($cb.length) {
                    $cb.addClass('active');
                }

                SearchUIInputs.price.rebuildString(from, to);
            });
        },

        remove: function() {
            $('[data-price-input="from"]').val('');
            $('[data-price-input="to"]').val('');
            $('[data-price]').removeClass('active');
            this.rebuildString();
        },

        rebuildString: function(from, to){
            var str = this.getInfo(from, to),
                $holder = $('.trigger-price span span');

            if(!str.length) {
                $holder.removeClass();
                $holder.addClass('placeholder');
                $holder.text($holder.data('placeholder'));
                $holder.parent().parent().attr('title', '');
            }
            else {
                $holder.removeClass();
                $holder.addClass('data');
                $holder.parent().parent().attr('title', str);
                $holder.text(str);
            }

            var value = '';
            if(!!from || !!to) {
                value = (!!from?from:'') + ':' + (!!to?to:'')
                Search.setData('p', value);
            }
            else {
                Search.setData('p', null);
            }

            $('#price').data('value', value);
        },

        getInfo: function(from, to) {
            var str = '';

            if(!!from && !!to) {
                str = from + ' \u2014 ' + to;
            }
            else if(!!to) {
                str = 'до ' + to;
            }
            else if(!!from){
                str = 'від ' + from;
            }

            if(str.length)
                str += ' грн';

            return str;
        }
    },

    amenity: {
        init: function() {
            var $amenityCheck = $('[name="a"]'),
                updateData = function(){
                    var amenityData = $('[name="a"]:checked').map(function() {
                        return this.value;
                    }).get();

                    if(!amenityData.length) {
                        amenityData = null;
                    }

                    Search.setData('a', amenityData)
                };


            updateData();

            $amenityCheck.change(updateData);
        },

        remove: function() {
            $('[name="a"]:checked').attr('checked', false);
        }
    },

    type: {
        init: function() {
            var $typeCheck = $('[name="t"]'),
                updateData = function() {
                    var typeData = $('[name="t"]:checked').map(function() {
                        return this.value;
                    }).get();

                    if(!typeData.length) {
                        typeData = null;
                    }

                    Search.setData('t', typeData)
                };

            updateData();

            $typeCheck.change(updateData);
        },

        remove: function() {
            $('[name="t"]:checked').attr('checked', false);
        }
    },

    submit: {
        init: function() {
            $('.search-submit-btn').click(function(e){
                if(window.asyncSearch) {
                    e.preventDefault();
                    return;
                }
                e.preventDefault();

                var queryString = Search.getRequestString();
                if(!queryString.length || !Search.dataHasChanged()) return;

                window.location.href = "/search?" + queryString;
            });
        }
    }
};