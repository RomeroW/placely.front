var SearchUIFull = {

    settings: {},

    objects: {
        $mask: $('.search__layout'),
        $modalHolder: $('.stick__holder'),
        $staticHolder: $('.search__full-form'),
        $form: $('.search__full'),
        $info: $('.search__stick .search__info-holder'),
        $cancelButton: $('.btn-cancel'),
        $mobileInfoHolder: $('.search__info-mobile'),
        $fields: $('.search-fields'),
        $buttonsHolder: $('.search__full_start'),
        $stick: $('.search__stick')
    },

    state: '',

    /*
     * Module entry point
     */
    init: function() {
        this.bindUIActions();
    },

    /*
     * All UI related stuff here
     */
    bindUIActions: function() {
        var $holder = $('.search__full');

        if(!$holder.length) {
            return;
        }

        SearchUIFull.filtersClick();
        SearchUIFull.stickInit();
        SearchUIFull.allHiddenFiltersClick();
    },

    /**
     * Initialize stick window
     */
    stickInit: function() {
        var $header = $('header'),
            $stick = $('.search__stick'),
            $mobileStick = $('.search__info-mobile .search__info-holder'),
            $form = $('.search__full-form'),
            getTop = function(){return ($header.css('position') == 'fixed')? $header.height() : 0},
            stickShouldBeVisible = function(){
                return SearchUIFull.objects.$mask.is(':visible')
                    || $(document).scrollTop() > ($form.offset().top + $form.height());
            },
            calcStick = function(){
                $stick.css({
                    width: $stick.parent().width() + 'px',
                    top: getTop()
                });
            },
            showStick = function(){
                $stick.css({
                    position: 'fixed',
                    top: getTop()
                });
                $stick.fadeIn(200);
            },
            hideStick = function(){
                $stick.fadeOut(200);
            },
            fixMobilePosition = function() {
                if($mobileStick.parent().is(':visible')) {
                    console.log(this.scrollTop);
                    if($(window).scrollTop() > $header.height()) {
                        $mobileStick.addClass('it-fixed').css('top', getTop() + 'px');
                    }
                    else {
                        $mobileStick.removeClass('it-fixed').css('top', '');
                    }
                }
            };

        fixMobilePosition();

        $(window).resize(calcStick);
        $(window).scroll(fixMobilePosition);
        $(window).scroll(function() {
            if(!$stick.is(':visible') && stickShouldBeVisible()) {
                showStick();
            }
            else if($stick.is(':visible') && !stickShouldBeVisible()) {
                hideStick();
            }
        });

        $('.filters, .show-filters').click(SearchUIFull.showFullModal);
        SearchUIFull.objects.$cancelButton.click(SearchUIFull.closeFullModal);

        $(Search).on('searchDataChanged', this.updateSearchLabels);
        $(Search).on('searchDataChanged', this.updateSubmitButton);

        this.updateSearchLabels();
    },

    updateSubmitButton: function() {
        var $button = $('.search-submit-btn');

        if(Search.emptySearch()) {
            $button.removeClass('active');
            return;
        }

        if(Search.dataHasChanged()) {
            $button.addClass('active');
        }
        else {
            $button.removeClass('active');
        }
    },

    removeSearchFilter: function() {
        var $filter = $(this).parent(),
            type = $filter.data('type');

        switch (type) {
            case 'm':
                SearchUIInputs.location.remove('m');
                break;
            case 'd':
                SearchUIInputs.location.remove('d');
                break;
            case 'p':
                SearchUIInputs.price.remove();
                break;
            case 'a':
                SearchUIInputs.amenity.remove();
                break;
            case 't':
                SearchUIInputs.type.remove();
                break;
        }

        Search.setData(type, null);
    },

    /**
     * Generate search brief labels (short list of them) with delete button
     */
    updateSearchLabels: function() {
        var $holder = $('.search__all_filters'),
            $template = $holder.find('li').first(),
            labels = {
                m: 'Метро',
                d: 'Райони',
                p: 'Ціна',
                a: 'Зручності',
                t: 'Тип житла'
            };

        // clear holder
        $holder.html('');
        $template.appendTo($holder);
        $('.search__all_filters > li > span').unbind('click');

        for(var i in Search.data){
            if(Search.data.hasOwnProperty(i)) {
                var $elem = $template.clone(),
                    title = labels[i],
                    info = Search.getData(i, 'info');

                $elem.html($elem.html()
                    .replace('%title%', title)
                    .replace('%info%', info))
                    .data('type', i);

                $elem.removeAttr('style');

                $elem.appendTo($holder);
            }
        }

        $('.search__all_filters > li > span').click(SearchUIFull.removeSearchFilter);
    },

    showFullModal: function(e) {
        e.preventDefault();

        var n = SearchUIFull.objects;

        n.$stick.css({
            'overflow-y': 'scroll'
        });

        var height = n.$staticHolder.height(),
            $header = $('header'),
            getTop = function(){return ($header.css('position') == 'fixed')? $header.height() : 0},
            showStick = function(){
                n.$stick.css({
                    position: 'fixed',
                    top: getTop()
                });
                n.$stick.show();
            };

        n.$mask.show();
        n.$form.appendTo(n.$modalHolder);
        if(!n.$stick.is(':visible')) {
            showStick();
        }

        n.$staticHolder.css({height: height + 'px'});
        $('body').css({
            overflow: 'hidden'
        });
        $('.feed__row').css('z-index', '');
        n.$stick.css('height', '100%');

        n.$buttonsHolder.width(n.$modalHolder.width() + 'px');
        $(window).bind('resize.searchModalButtons', function() {
            n.$buttonsHolder.width(n.$modalHolder.width() + 'px');
        });

        n.$info.hide();
        n.$cancelButton.css('display', 'inline-block');
    },

    closeFullModal: function() {
        var n = SearchUIFull.objects,
            stickShouldBeVisible = function(){
                if(n.$staticHolder.is(':visible')) {
                    return (SearchUIFull.state !== 'asyncSearching' && SearchUIFull.objects.$mask.is(':visible'))
                        || ($(document).scrollTop() > (n.$staticHolder.offset().top + n.$staticHolder.height()));
                }

                return true;
            },
            hideStick = function(){
                n.$stick.hide();
            };

        n.$mask.hide();
        n.$form.appendTo(n.$staticHolder);
        $('.feed__row').css('z-index', '12');

        n.$staticHolder.css('height', '');
        n.$fields.css('height', '');
        n.$stick.css('height', '');
        n.$stick.css('overflow-y', '');

        if(!stickShouldBeVisible()) {
            hideStick();
        }

        $('body').css({
            'overflow-y': 'scroll'
        });
        $(window).unbind('resize.searchModalButtons');
        n.$buttonsHolder.css('width', '');

        n.$info.show();
        n.$cancelButton.hide();
    },

    showAsyncProgress: function() {
        SearchUIFull.state = 'asyncSearching';
        var n = SearchUIFull.objects;

        if(n.$mask.is(':visible')) {
            SearchUIFull.closeFullModal();
        }

        n.$mask.show();

        $('body').css({
            overflow: 'hidden'
        });
    },

    hideAsyncProgress: function() {
        var n = SearchUIFull.objects;

        if(SearchUIFull.state == 'asyncSearching') {
            SearchUIFull.state = '';
        }

        n.$mask.hide();

        $('body').css({
            'overflow-y': 'scroll'
        });

        $("html, body").animate({ scrollTop: 0 }, 500);
    },

    filtersClick: function() {
        var $button = $('.search__show_all > span'),
            $all = $('.search__additional');

        if(!$button || !$all) return;

        $button.click(function(){
            if(!$button.data('show')) {
                $button.data('show', $button.text())
            }

            if($all.is(':visible')) {
                $all.hide();
                $button.text($button.data('show'));
            }
            else {
                $all.show();
                $button.text($button.data('hide'));
            }
        });
    },


    allHiddenFiltersClick: function() {
        var $button = $('.search__show_hidden > span');

        if(!$button.data('show')) {
            $button.data('show', $button.text());
        }

        $button.click(function(){
            var $holder = $(this).parent().parent();

            if(!$(this).hasClass('active')) {
                $(this).text($(this).data('hide'));
                $(this).addClass('active');
                $holder.find('.search__tick-hidden').css('display', 'block');
            }
            else {
                $(this).text($(this).data('show'));
                $(this).removeClass('active');
                $holder.find('.search__tick-hidden').hide();
            }


        });
    }
};