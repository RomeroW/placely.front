var GA = {
    id: 'UA-74186202-1',

    init: function() {
        var cleanUrl = function() {
            if (/utm_/.test(location.search) && window.history.replaceState){
                // thx @cowboy for the revised hash param magic.
                var oldUrl = location.href;
                var newUrl = oldUrl.replace(/\?([^#]*)/, function(_, search) {
                    search = search.split('&').map(function(v) {
                        return !/^utm_/.test(v) && v;
                    }).filter(Boolean).join('&'); // omg filter(Boolean) so dope.
                    return search ? '?' + search : '';
                });

                if ( newUrl != oldUrl ) {
                    window.history.replaceState({},'', newUrl);
                }
            }
        };

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', this.id, 'auto');
        ga('send', 'pageview', {'hitCallback': cleanUrl});

        this.assignEvents();
    },

    assignEvents: function() {
        this.noticeEvents();
        this.authEvents();
        this.settingsEvents();
        this.partnersEvents();
        this.otherEvents();
    },

    /**
     * Notice events
     */
    noticeEvents: function() {
        $('#formCreateNotice').submit(function() {
            ga('send', 'event', 'Notices', 'create');
        });

        $('.contact__show, span.details').click(function() {
            ga('send', 'event', 'Notices', 'contacts')
        });
    },

    /**
     * All auth events
     */
    authEvents: function() {

        $('.auth__btn_open_modal').click(function(){
            ga('send', 'event', 'Auth', 'Open modal', 'Attempt');
        });

        $('.btn__social-vk-big').click(function(){
            ga('send', 'event', 'Auth', 'VK', 'Modal');
        });

        $('.btn__social-fb-big').click(function(){
            ga('send', 'event', 'Auth', 'FB', 'Modal');
        });

        $('.btn__social-vk').click(function(){
            ga('send', 'event', 'Auth', 'VK', 'Notice');
        });

        $('.btn__social-fb').click(function(){
            ga('send', 'event', 'Auth', 'FB', 'Notice');
        });
    },

    settingsEvents: function() {
        $('.settings-link').click(function(){
            ga('send', 'event', 'Settings', 'Avatar');
        });

        $('.contacts__link a').click(function(){
            ga('send', 'event', 'Settings', 'Notice');
        });
    },

    partnersEvents: function() {
        $('a.p').click(function () {
            ga('send', 'event', 'Partner', $(this).data('trgt'));
        });
    },

    otherEvents: function() {
        $('.sn_links a').click(function () {
            ga('send', 'event', 'Social Page', $(this).attr('href'));
        });
    }

};