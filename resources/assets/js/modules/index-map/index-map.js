var IndexMap = {

    settings: {
        count: 15
    },
    initializedHeight: false,
    initialized: false,
    centerCounted: false,
    openedInfowindow: null,
    markers: [],
    gmMarkers: [],
    activeMarkerAnchor: 0,
    lastActiveMarkerAnchor: 0,

    $slot: false,
    mapCanvas: false,

    /*
     * Module entry point
     */
    init: function() {
        IndexMap.bindUIActions();
    },

    /*
     * All UI related stuff here
     */
    bindUIActions: function() {
        IndexMap.mapInit();

        $(window).scroll(this.scrollMarkersCallback);

        IndexMap.updateFeedMarkerCallback();
    },

    updateFeedMarkerCallback: function() {
        if(!IndexMap.initialized) return;

        var $one = $('.feed__one');
        $one.unbind();
        $one.hover(function(){
                var marker = IndexMap.gmMarkers[$(this).data('id')];
                marker.setIcon('images/marker_red_contour.png');
            },
            function(){
                var marker = IndexMap.gmMarkers[$(this).data('id')];
                marker.setIcon('images/marker_red.png');
            });

        if(IndexMap.openedInfowindow) {
            IndexMap.openedInfowindow.close();
        }
    },

    calculateMapHeight: function() {
        return IndexMap.$slot.height() < document.body.clientHeight?
            IndexMap.$slot.height() : document.body.clientHeight - 70;
    },

    mapInit: function() {
        /**
         * Display map only on tablets and pc (display with > 739px)
         */
        if ( $(window).width() > 739) {
            IndexMap.initialized = true;
            IndexMap.$slot = $('.map');

            var height = IndexMap.calculateMapHeight();

            IndexMap.mapCanvas = document.getElementById('map-canvas');
            IndexMap.mapCanvas.style.height = height + 'px';
            IndexMap.initializedHeight = $(window).height();

            $(window).scroll(IndexMap.fixMap);
            $(window).on('resize', IndexMap.fitSize);

            var mapOptions = {
                center: new google.maps.LatLng(50.4023, 30.3563),
                zoom: 12,
                scrollwheel: false,
                zoomControl: true,
                mapTypeControl: false,
                streetViewControl: false,
                rotateControl: false
            };

            var map = new google.maps.Map(IndexMap.mapCanvas, mapOptions);
            IndexMap.$slot.data('map', map);

            google.maps.event.addListener(map, 'tilesloaded', IndexMap.fixMap);
            google.maps.event.addListenerOnce(map, 'projection_changed', function() {
                IndexMap.updateMarkers();
            });

        }
    },

    fixMap: function() {
        if(!IndexMap.initialized) return;

        var $fixed = $('#map-canvas');

        if ($(window).scrollTop() + 70 > IndexMap.$slot.position().top) {
            $fixed.css({
                'position': 'fixed',
                'top': 70 + 'px'
            });
        }
        else if ($(window).scrollTop() - 70 < IndexMap.$slot.position().top) {
            $fixed.css({
                'position': 'absolute',
                'top': 'auto'
            });
        }

        if($(window).scrollTop() + $(window).height() >  IndexMap.$slot.height() + IndexMap.$slot.position().top) {
            $fixed.css({
                'position': 'absolute',
                'top': (IndexMap.$slot.height() - $fixed.height() + IndexMap.$slot.offset().top) + 'px'
            });
        }
    },

    fitSize: function() {
        if(!IndexMap.initialized) return;

        if(IndexMap.initializedHeight) {
            var height = IndexMap.calculateMapHeight();

            if(IndexMap.initializedHeight == height) return;

            IndexMap.initializedHeight = height;
            IndexMap.mapCanvas.style.height = height + 'px';
        }
        IndexMap.fixMap();
    },

    scrollMarkersCallback: function() {
        if(!IndexMap.initialized) return;

        var $anchorFeedListItems = $('.feed__one:nth-child('+IndexMap.settings.count+'n)');

        var noone = 0;

        $anchorFeedListItems.each(function(){
            if($(this).offset().top < $(window).scrollTop() + $(window).height()) {
                IndexMap.activeMarkerAnchor = $anchorFeedListItems.index($(this)) + 1;
            }
            else {
                noone += 1;
            }

        });

        if(noone == $anchorFeedListItems.length)
            IndexMap.activeMarkerAnchor = 0;

        if(IndexMap.activeMarkerAnchor != IndexMap.lastActiveMarkerAnchor) {
            IndexMap.lastActiveMarkerAnchor = IndexMap.activeMarkerAnchor;

            //IndexMap.updateMarkers()
            Pages.updateMarkersCallback = function(index) {
                return function(){
                    IndexMap.updateMarkers(index);
                };
            }(IndexMap.lastActiveMarkerAnchor);

            IndexMap.updateMarkers(IndexMap.activeMarkerAnchor);
        }
    },


    updateMarkers: function(index) {
        var map = $('.map').data('map');
        index = typeof index !== 'undefined' ?  index : 0;

        if(typeof map === 'undefined') return;

        var $feedListItems = null;

        if(index == 0) {
            $feedListItems = $('.feed__one').slice(0, IndexMap.settings.count);
        }
        else {
            $feedListItems = $('.feed__one').slice(index*IndexMap.settings.count - 4, (index+1)*IndexMap.settings.count);
            if($feedListItems.length < IndexMap.settings.count)
                return;
        }

        if(IndexMap.gmMarkers.length) {
            for(var i in IndexMap.gmMarkers) {
                google.maps.event.clearInstanceListeners(IndexMap.gmMarkers[i]);
                IndexMap.gmMarkers[i].setMap(null);
            }
        }
        IndexMap.gmMarkers = [];
        IndexMap.markers = [];

        $feedListItems.each(function(i){
            IndexMap.markers.push({
                lat: $(this).data('lat'),
                lng: $(this).data('lng'),
                id: $(this).data('id'),
                description: $(this).data('description')
            });
        });

        if(IndexMap.markers.length == 0) {
            return;
        }

        var infowindow = new InfoBox({
            content: 'wow',
            alignBottom: true,
            //disableAutoPan: false,
            pixelOffset: new google.maps.Size(-147, -57),
            zIndex: null,
            boxStyle: {
                opacity: 1,
                width: "275px"
            },
            closeBoxURL: '',
            infoBoxClearance: new google.maps.Size(1, 1)
        });

        var latlngbounds = new google.maps.LatLngBounds();

        for(var i in IndexMap.markers) {
            var notice = IndexMap.markers[i],
                content = UIMapNotice.render(notice),
                marker = new google.maps.Marker({
                    position: notice,
                    map: map,
                    icon: 'images/marker_red.png',
                });

            marker.addListener('click', (function(marker,content,infowindow,notice) {
                return function() {
                    var $elem = $('.feed__item[data-id="'+notice.id+'"]').clone();
                    $elem.find('.data__metro').remove();
                    $elem.wrap('<div>');
                    $elem.parent().addClass('style-iw');
                    $elem = $elem.parent();
                    var data = $elem.wrap('<p>').parent().html();

                    infowindow.setContent(data);
                    infowindow.open(map,marker);
                    IndexMap.openedInfowindow = infowindow;
                };
            })(marker,content,infowindow,notice));

            IndexMap.gmMarkers[notice.id] = marker;
            latlngbounds.extend(new google.maps.LatLng(notice.lat, notice.lng));
        }

        google.maps.event.addListener(map, "click", function(event) {
            infowindow.close();
        });

        $(document).mouseup(function (e)
        {
            var container = $(".style-iw, #map-canvas");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                infowindow.close();
            }
        });

        /**
         * Add callback on notices link on the map infobox
         */
        $(document).off('click', '.feed_map_notice');
        $(document).on('click', '.feed_map_notice', IndexMap.noticePreviewClick);

        map.setZoom(IndexMap.calcZoom(latlngbounds, {width: $(window).width()/2 - 30, height: $(window).height() - 105}));
        IndexMap.map_recenter(map, latlngbounds.getCenter(), $(window).width()/4, -15);

        if(IndexMap.openedInfowindow) {
            IndexMap.openedInfowindow.close();
        }
    },

    noticeScrollOut: function() {
        var $activeNotice = $('.feed__one_title.active');

        if (!$activeNotice.length)
            return;

        $activeNotice.removeClass('active');
        var id = $activeNotice.parent().parent().data('id'),
            marker = IndexMap.gmMarkers[id];

        if (!marker)
            return;

        marker.setIcon('images/marker_red.png');
    },

    noticePreviewClick: function(){
        var id = $(this).attr('href').split('#')[1],
            $notice = $('[data-id="'+id+'"]');

        if(!$notice.length)
            return;

            //var $activeNotice = $notice.find('.feed__one_title');
            //$activeNotice.addClass('active');
            //
            //var marker = IndexMap.gmMarkers[id];
            //
            //if (marker)
            //    marker.setIcon('images/marker_red_contour.png');

        if(IndexMap.openedInfowindow)
            IndexMap.openedInfowindow.close();

        $('html, body').animate({
            scrollTop: $notice.offset().top - 90
        }, 200,function(){
            //setTimeout(function(){$(window).one('scroll', IndexMap.noticeScrollOut)}, 25);
        });
    },

    map_recenter: function (map, latlng,offsetx,offsety) {

        var point1 = map.getProjection().fromLatLngToPoint(
            (latlng instanceof google.maps.LatLng) ? latlng : map.getCenter()
        );
        var point2 = new google.maps.Point(
            ( (typeof(offsetx) == 'number' ? offsetx : 0) / Math.pow(2, map.getZoom()) ) || 0,
            ( (typeof(offsety) == 'number' ? offsety : 0) / Math.pow(2, map.getZoom()) ) || 0
        );

        var centerPoint = map.getProjection().fromPointToLatLng(new google.maps.Point(
            point1.x - point2.x,
            point1.y + point2.y
        ));

        if(!IndexMap.centerCounted) {
            map.setCenter(centerPoint);
            IndexMap.centerCounted = true;
        }
        else {
            map.panTo(centerPoint);
        }
    },

    calcZoom: function(bounds, mapDim) {
        var WORLD_DIM = { height: 256, width: 256 };
        var ZOOM_MAX = 15;

        function latRad(lat) {
            var sin = Math.sin(lat * Math.PI / 180);
            var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
            return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
        }

        function zoom(mapPx, worldPx, fraction) {
            return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
        }

        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();

        var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

        var lngDiff = ne.lng() - sw.lng();
        var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
        var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    }
};