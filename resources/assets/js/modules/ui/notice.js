var UINotice = {

    render: function(item) {

        var template = '';

        if(item.mainPhoto) {
            template = '<a class="feed__one feed__item feed__item-full" ' +
                'href="' + item.permalink + '" ' +
                'data-lat="' + item.lat + '" ' +
                'data-lng="' + item.lng + '" ' +
                'data-id="' + item.id + '" ' +
                'data-description="' + item.short_info + '">' +
                '<div class="notice__image" style="background-image: url(' + item.mainPhoto + ')">' +
                '<div class="notice__mask">';

            if (item.amenities.length) {
                template += '<ul class="image__amenities">';
                for(var k in item.amenities) {
                    var amenity = item.amenities[k];
                    template += '<li title="'+amenity.description+'"><i class="icon-'+amenity.icon+'"></i></li>';
                }
                template += '</ul>';
            }

            template += '<div class="image__short_info">' +
                '<div class="info__price">₴' + item.price + '</div>' +
                '<div class="info__type">' + item.type + '</div>' +
                '</div>' +
                '<div class="image__date">' + item.created_at + '</div>' +
                '<div class="image__photos"><i class="icon-camera"></i> ' + item.photos + '</div>' +
                '</div>' +
                '</div>' +
                '<div class="notice__data">'
                +
                this.renderStatus(item.status)
                +
                '<div class="data__address">' + item.address + '</div>' +
                '<div class="data__brief">' + item.brief + '</div>';

            if (item.metro_stations.length) {
                template += '<ul class="data__metro">';
                for (var j in item.metro_stations) {
                    var station = item.metro_stations[j];
                    template += '<li class="station station-'+station.type+'">' +
                        '<div class="station__title">'+station.title+'</div>' +
                        '<div class="station__timing">&nbsp;';

                    if(station.foot > 0) {
                        template += '<span class="foot" title="Пішки до станції">'+station.foot+'хв</span>';
                    }
                    if(station.foot > 0 && station.transport > 0) {
                        template += '<span> / </span>';
                    }
                    if(station.transport > 0) {
                        template += '<span class="transport" title="Транспортом до станції">'+station.transport+'хв</span>';
                    }
                    template += '</div>' +
                        '</li>';
                }
                template += '</ul>';
            }

            template += '</div>' +
                '</a>';
        }
        else {
            template = '<a class="feed__one feed__item feed__item-short" data-template="feed" ' +
                'href="' + item.permalink + '" ' +
                'data-lat="' + item.lat + '" ' +
                'data-lng="' + item.lng + '" ' +
                'data-id="' + item.id + '" ' +
                'data-description="' + item.short_info + '">' +
                '<div class="notice__data">' +
                '<div class="data__info">₴' + item.price + ', ' + item.type + '</div>'
                +
                this.renderStatus(item.status)
                +
                '<div class="data__brief">' + item.address + '. ' + item.brief + '</div>';

            if (item.metro_stations.length) {
                template += '<ul class="data__metro">';
                for (var j in item.metro_stations) {
                    var station = item.metro_stations[j];
                    template += '<li class="station station-'+station.type+'">' +
                        '<div class="station__title">'+station.title+'</div>' +
                        '<div class="station__timing">&nbsp;';

                    if(station.foot > 0) {
                        template += '<span class="foot" title="Пішки до станції">'+station.foot+'хв</span>';
                    }
                    if(station.foot > 0 && station.transport > 0) {
                        template += '<span> / </span>';
                    }
                    if(station.transport > 0) {
                        template += '<span class="transport" title="Транспортом до станції">'+station.transport+'хв</span>';
                    }
                    template += '</div>' +
                        '</li>';
                }
                template += '</ul>';
            }

            if (item.amenities.length) {
                template += '<ul class="data__amenities">';
                for(var k in item.amenities) {
                    var amenity = item.amenities[k];
                    template += '<li title="'+amenity.description+'"><i class="icon-'+amenity.icon+'"></i></li>';
                }
                template += '</ul>';
            }

            template += '<div class="data__time">'+item.created_at+'</div>' +
                '</div>' +
                '</a>';
        }

        return template;
    },

    renderStatus: function(status) {
        if(!status) return '';

        return '<div class="data__status">' +
            '<span class="status status-' + status.name + '" title="' + status.description + '">' + status.title + '</span>' +
            '</div>';
    }
};