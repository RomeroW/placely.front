var HiddenContacts = {

    $contact: false,

    /*
     * Module entry point
     */
    init: function () {
        this.$contact = $('.contact');

        this.bindUIActions();
        this.bindLogic();
    },

    /*
     * All UI related stuff here
     */
    bindUIActions: function () {
        var $header = $('header'),
            $footer = $('footer'),
            $gallery = $('#photo-gallery'),
            $holder = $('.columns-contact'),
            $contentHolder = $('.content__holder'),
            topFix = function () {
                if($gallery.length) {
                    return $gallery.offset().top + $gallery.height();
                }
                else {
                    return $header.height();
                }
            },
            getWidth = function(){
                return $holder.width();
            },
            $contactStick = $('.contact__info-mobile .contact__info-holder'),
            getTop = function(){return ($header.css('position') == 'fixed')? $header.height() : 0},
            fixMobilePosition = function() {
                if($contactStick.parent().is(':visible')) {
                    console.log(this.scrollTop);
                    if($(window).scrollTop() > topFix()) {
                        $contactStick.addClass('it-fixed').css('top', getTop() + 'px');
                    }
                    else {
                        $contactStick.removeClass('it-fixed').css('top', '');
                    }
                }
            };

        fixMobilePosition();
        $(window).scroll(fixMobilePosition);

        $('.details').click(function(e){
            e.preventDefault();

            var $btn = $(this);
            $btn.addClass('loading');

            $.ajax({
                url: $('.contact__show').data('url'),
                method: 'POST'
                //beforeSend: function( xhr ) {
                //    xhr.setRequestHeader("X-CSRF-TOKEN", $btn.data('csrf'));
                //}
            }).done(function(data){
                $btn.removeClass('loading');

                for (var key in data) {
                    // skip loop if the property is from prototype
                    if (!data.hasOwnProperty(key)) continue;

                    var val = data[key],
                        $holder = $('[data-hidden-contact="'+key+'"]');

                    if($holder.length) {
                        $holder.removeClass('value-hidden').html(val);
                    }
                }

                $('.contact__show').hide();
                HiddenContacts.$contact.addClass('contact-modal');
                $('body').addClass('menu-opened');
            });
        });

        $('.cross').click(function(){
            if(HiddenContacts.$contact.hasClass('contact-modal')) {
                HiddenContacts.$contact.removeClass('contact-modal');
                $('body').removeClass('menu-opened');
            }
        });

        var $stick = $('.contact'),
            height = $gallery.length?$gallery.height()-79:20;

        var stick = {
            currentState: null,
            make: {
                top : function() {
                    $stick
                        .css('top', '')
                        .css('width', '')
                        .css('margin-top', '')
                        .removeClass('contact-sticked');
                },
                fixed : function() {
                    $stick
                        .css('top', getTop() + 'px')
                        .css('margin-top', '2em')
                        .css('width', getWidth() + 'px')
                        .addClass('contact-sticked');
                },
                bottom : function() {
                    $stick
                        .css('top', '')
                        .css('margin-top', $contentHolder.offset().top + $contentHolder.height() - $stick.height() - 2 * $header.height() - ($gallery.length?$gallery.height():0) + 10 + 'px')
                        .css('width', '')
                        .removeClass('contact-sticked');
                }
            },
            check: {
                top : function() {
                    return $(window).scrollTop() < height;
                },
                fixed : function() {
                    return !stick.check.top() && !stick.check.bottom();
                },
                bottom : function() {
                    return $(window).scrollTop() + $stick.height() + 2 * $header.height() > $contentHolder.offset().top + $contentHolder.height();
                }
            },
            getState: function() {
                for(var key in stick.check) {
                    if(stick.check.hasOwnProperty(key)) {
                        if(typeof stick.check[key] == "function") {
                            if(stick.check[key]()) return key;
                        }
                    }
                }
            },
            makeState: function(state) {
                if(stick['make'] && typeof stick['make'][state] == "function") {
                    stick['make'][state]();
                }
            },
            action: function() {
                if(!$('.contact__info-mobile').is(':visible')) {
                    if(stick.currentState != stick.getState()) {
                        stick.currentState = stick.getState();
                        stick.makeState(stick.currentState);
                    }
                }
            }
        };

        stick.action();
        $(window).scroll(stick.action);

        $(window).resize(function(){
            if(HiddenContacts.$contact.hasClass('contact-sticked')) {
                HiddenContacts.$contact.width(getWidth());
            }
        });
    },

    bindLogic: function(){

        /*
         * "Show contacts" logic
         */
        $('.contact__show').click(function(){
            var $btn = $(this);
            $btn.css('opacity', '.5');

            $.ajax({
                url: $(this).data('url'),
                method: 'POST'
                //beforeSend: function( xhr ) {
                //    xhr.setRequestHeader("X-CSRF-TOKEN", $btn.data('csrf'));
                //}
            }).done(function(data){
                $btn.css('opacity', '1');

                for (var key in data) {
                    // skip loop if the property is from prototype
                    if (!data.hasOwnProperty(key)) continue;

                    var val = data[key],
                        $holder = $('[data-hidden-contact="'+key+'"]');

                    if($holder.length) {
                        $holder.removeClass('value-hidden').html(val);
                    }
                }

                $btn.hide();
            });
        });

        var $bookedButton = $('.object__booked');

        if($bookedButton.length) {
            Utils.Modal.confirm.init(false, function(){
                $.form($bookedButton.data('url'), {'_token': $bookedButton.data('csrf')}).submit();
            }, false);

            $bookedButton.click(Utils.Modal.confirm.run);
        }
    }
};