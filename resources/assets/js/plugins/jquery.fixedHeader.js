(function($) {
    $.fn.fixedHeader = function (options) {
        options = $.extend({}, {
            // classes defines collapsed header
            addClass: 'fixed header',
            removeClass: false,
            holder: false

            // if block specified - header will collapse after block
            //$collapseAnchor: $('.anchor')
        }, options);

        var $header = this;
        var $slot = options.$collapseAnchor;

        function fixHeader() {
            if ($(window).scrollTop() + $header.height() > $slot.position().top) {
                $header.addClass(options.addClass);
                $header.removeClass(options.removeClass);
                if(options.holder) {
                    $header.parent().css('z-index', 90);
                }
            }
            else if ($(window).scrollTop() + $header.height() < $slot.position().top) {
                $header.removeClass(options.addClass);
                $header.addClass(options.removeClass);
                if(options.holder) {
                    $header.parent().css('z-index', options.holder);
                }
            }
        }

        $(window).scroll(fixHeader);
        $(window).on('resize', fixHeader);

        fixHeader();

        return this;
    };
})(jQuery);
