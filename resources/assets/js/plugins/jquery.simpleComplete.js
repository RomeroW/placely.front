(function($) {
    $.fn.simpleComplete = function(options) {

        /**
         * Initialize all required stuff
         */
        options = $.extend({}, {
            source: null,
            hintItem: function( ul, item ) {
                return $( "<li>" )
                    .append( item.label )
                    .appendTo( ul );
            },
            select: function(event, ui) {}
        }, options);

        var $element = $(this[0]);

        /**
         * Initialize jquery ui autocomplete
         */
        $element.autocomplete({
            minLength: 1,
            source: function(request, response) {
                var results = $.ui.autocomplete.filter(options.source, request.term);

                response(results.slice(0, 10));
            },
            focus: function( event, ui ) {
                $element.val( ui.item.label );
                return false;
            },
            select: options.select
        }).keypress(function(e) {
            if (e.keyCode == '13') {
                e.preventDefault();
            }
        }).autocomplete( "instance" )._renderItem = options.hintItem;
    };
})(jQuery);