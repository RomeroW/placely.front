(function($) {
    $.fn.googleMaps = function (mapOptions) {
        var $element = this[0];

        var map = new google.maps.Map($element, $.extend({}, {
            center: new google.maps.LatLng(44.5403, -78.5463),
            zoom: 8
        }, mapOptions));

        this.data('map', map);

        return this;
    };
})(jQuery);