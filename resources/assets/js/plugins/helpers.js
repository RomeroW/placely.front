function equals(a, b) {
    var orderByKeys = function(obj) {
        var keys = Object.keys(obj),
            i, len = keys.length,
            ordered = {};

        keys.sort();

        for (i = 0; i < len; i++) {
            var k = keys[i],
                value = obj[k];

            if(value instanceof Array) {
                value = value.sort();
            }

            ordered[k] = obj[k];
        }

        return ordered;
    };

    return JSON.stringify(orderByKeys(a)) == JSON.stringify(orderByKeys(b));
}

// cookies functions
var Cookie = {
    set: function (cname, cvalue, exdays) {
        var expires = '';
        if(typeof exdays != 'undefined') {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            expires = "expires="+d.toUTCString();
        }
        document.cookie = cname + "=" + cvalue + ";path=/;" + expires;
    },

    get: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    },

    check: function (cname) {
        var value = Cookie.get(cname);
        return value != "";
    }
};


// device functions
var Device = {
    mobileWidth: 641,

    isMobile: function() {
        return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) <= Device.mobileWidth;
    }
};