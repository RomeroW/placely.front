(function($) {
    $.fn.photoList = function(options) {
        options = $.extend({}, {
            // classes defines collapsed header
            $oneHolder: 'select',
            $listHolder: 'select'
        }, options);

        var $one = options.$oneHolder;
        var $list = options.$listHolder;

        $list.find('li').click(function(){
            $list.find('li').removeClass('active');
            $(this).addClass('active');

            $one.find('img').attr('src', $(this).data('url'));

            if ( $(window).width() > 739) {
                $(this).parent().parent().scrollTop($(this).offset().top - $(this).parent().offset().top - 30);
            } else {
                $(this).parent().parent().scrollLeft($(this).offset().left - $(this).parent().offset().left - 30);
            }
        });

        function fixMobileSlide() {
            if ( $(window).width() > 739) {
                $list.parent().scrollLeft(0);
                $list.width('80%');
            } else {
                $list.width($list.find('li').length * ($list.find('li').first().width() + 14) + 'px');
                $list.parent().height('100px');
                $list.parent().scrollTop(0);
            }
        }
        fixMobileSlide();
        $(window).on('resize', fixMobileSlide);

        return this;
    };
})(jQuery);