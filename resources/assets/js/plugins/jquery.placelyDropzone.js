(function($) {
    $.fn.placelyDropzone = function(options){

        var previewNode = document.querySelector("#dropzoneTemplate");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        options = $.extend({}, {
            url: false, // Set the url manually
            maxFilesize: 5,
            thumbnailWidth: 95,
            thumbnailHeight: 95,
            parallelUploads: 20,
            maxFiles: 50,
            previewTemplate: previewTemplate,
            previewsContainer: false, // Define the container to display the previews,
            $mainPreview: false, // Define the container to main photo ID,
            success: function(file, response) {
                $(file.previewElement)
                    .removeClass('loading')
                    .find('.progress')
                    .remove();
                $(file.previewElement).data('id', response.data.id);
                $(file.previewElement).find('[dz-image-input]')
                    .val(response.data.id)
                    .attr('name', element.attr('dz-input')+'[]');

                $(file.previewElement).find('img').attr('src', response.data.url);
                var $mainStatus = $(file.previewElement).find('.mainStatus');
                $mainStatus.text($mainStatus.data('select-main'));

                if(!this.options.$mainPreview.val() || this.options.$mainPreview.val() == response.data.id) {
                    Preview.setMain(this, $(file.previewElement));
                }

                var dz = this;
                $(file.previewElement).click(function(){Preview.selectMain(dz, $(this))});
            }
        }, options);

        var element = this;

        // Object for work with preview
        var Preview = {
            selectMain: function(dz, $el) {
                if($el.hasClass('mainPreview')) return;

                this.setMain(dz, $el);
            },
            setMain: function(dz, $el) {
                dz.options.$mainPreview.val($el.data('id'));

                $(dz.previewsContainer).find('li').each(function(index){
                    var $mainStatus = $(this).find('.mainStatus');
                    $mainStatus.text($mainStatus.data('select-main'));
                    $(this).removeClass('mainPreview');
                });

                $el.addClass('mainPreview');
                var $mainStatus = $el.find('.mainStatus');
                $mainStatus.text($mainStatus.data('main'));
            },
            deleteMain: function(dz, $el) {
                if(!$el.hasClass('mainPreview')) return;
                dz.options.$mainPreview.val('');

                var $items = $(dz.previewsContainer).find('li');

                if($items.length > 0) {
                    Preview.setMain(dz, $items.first());
                }
            }
        };

        this.dropzone(options);
        this[0].dropzone
            .on("addedfile", function(file) {
                // Hookup the start button
                $(file.previewElement).addClass('loading');
            })
            .on("removedfile", function(file) {
                var data = {
                    attachmentId: $(file.previewElement).data('id'),
                    createSessionHash: $('#createSessionHash').val()
                },
                    dz = this,
                    $el = $(file.previewElement);

                $.ajax({
                    method: 'DELETE',
                    url: options.deleteUrl,
                    data: data
                }).fail(function(msg){
                    console.log(msg);
                });

                Preview.deleteMain(dz, $el);
            })
            .on('maxfilesexceeded', function() {
                $('.form__photos div.fields__validation_error-photo')
                        .text('Перевищена допустима кількість фото.')
                        .fadeTo(300, 1).delay(3000).fadeTo(300, 0);
            })

            .on("sending", function(file, xhr, formData) {
                // Will send the filesize along with the file as POST data.
                formData.append("createSessionHash", $('#createSessionHash').val());
            })
            .on("error", function(file) {
                var $errorMessage = $('.form__photos div.fields__validation_error-photo');

                if (file.type.indexOf('image') < 0) {
                    $errorMessage.text('Дозволені тільки зображення.')
                        .fadeTo(300, 1).delay(3000).fadeTo(300, 0);
                } else if (file.size / 1000000 >= options.maxFilesize) {
                    $errorMessage.text('Перевищено максимальний розмір фото.')
                        .fadeTo(300, 1).delay(3000).fadeTo(300, 0);
                } else {
                    $errorMessage.text('Дозволені тільки зображення.')
                        .fadeTo(300, 1).delay(3000).fadeTo(300, 0);
                }

                $(file.previewElement).remove();
            })
            .on("totaluploadprogress", function(progress) {
                $('[data-dz-uploadprogress]').css({width: progress*0.75 + "%"});
            });

        /**
         * Get preloaded images (attr dz-preload) and put them to dropzone
         * image holder as uploaded images
         */
        $('[dz-preload]').each(function(){
            if($(this).data('id') == undefined)
                return;

            var data = {
                id: $(this).data('id'),
                url: $(this).data('url')
            };

            element[0].dropzone.emit("addedfile", data);
            element[0].dropzone.emit("success", data, {
                data: data
            });
            element[0].dropzone.emit("complete", data);
        });

        return this;
    };
})(jQuery);