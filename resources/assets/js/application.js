var Application = {
    initPage: function() {
        var ns = Pages,
            controller = document.body.getAttribute("data-page");

        /**
         * Runs JS code for entire application
         */
        if(ns['all'] && typeof ns['all']['init'] == "function") {
            ns['all']['init']();
        }

        /**
         * Runs JS code for specific controller
         */
        if ( controller !== "" && ns[controller] && typeof ns[controller]['init'] == "function" ) {
            ns[controller]['init']();
        }
    },
    init: function() {
        Utils.Modal.init();
        Application.initPage();
    }
};

Application.init();