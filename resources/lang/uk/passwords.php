<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль має мати мінімум 6 символів довжини, і співпадати з повторним вводом',
    'reset' => 'Пароль був змінений!',
    'sent' => 'Ми відправили посилання для зміни паролю вам на email!',
    'token' => 'Токен зміни паролю не правильний.',
    'user' => "Користувача з таким email'ом не існує.",

];
