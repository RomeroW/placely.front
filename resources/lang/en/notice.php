<?php

return [
    'status' => [
        'pending' => [
            'title' => 'moderation',
            'description' => 'notice is moderating'
        ],
        'unauthorized' => [
            'title' => 'unauthorized',
            'description' => 'notice belongs to user who is not authorized'
        ],
        'published' => [
            'title' => 'published',
            'description' => 'notice is published, clients see it'
        ],
        'booked' => [
            'title' => 'booked',
            'description' => 'place is booked, users do not see it'
        ],
        're-pending' => [
            'title' => 'moderation',
            'description' => 'notice is moderating'
        ],
        'trash' => [
            'title' => 'trash',
            'description' => 'notice trashed'
        ],
    ]
];