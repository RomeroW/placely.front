<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following strings related to whole project
    |
    */

    'index_titles' => [
        'index' => 'Last notices',
        'search' => 'Search results',
        'my' => 'My notices'
    ],

    'empty_feed' => [
        'search' => [
            'header' => 'No results found',
            'description' => 'you can try another query'
        ],
        'container' => [
            'header' => 'This notices don\'t exist',
            'description' => 'it will be in some time...'
        ]
    ],

    'phone' => 'Phone',
    'skype' => 'Skype',

    'apartment' => 'Apartments',
    'room' => 'Room',

    'other' => 'Other',

    'apartments' => 'apartments',
    'rooms' => 'rooms',

    'room-n-' => '',
    'room-n-1' => 'one-room',
    'room-n-2' => 'double-room',
    'room-n-3' => 'three-room',
    'room-n-4' => 'many-room',

    'district' => 'District',
    'microdistrict' => 'Microdistrict',

    'm_sq' => 'm<sup>2</sup>'

];
