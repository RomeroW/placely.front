@extends('app.layout')

@section('title', _('Налаштування профілю —'))

@section('page', 'settings')

@section('specialHeaderClasses', 'overmap')

@section('head')
    <script type="text/javascript">
        @include('app.partials.validation-errors')
    </script>
@endsection

@section('content')
    <div class="fixed__holder-new"></div>

    <form method="post" action="{{url('settings')}}">
        <div class="form__general">
            <div class="fields__validation_error">Деякі поля не заповнені, чи заповнені не правильно.</div>
            <div class="row form__content">
                <div class="fields__title">Налаштування профілю</div>

                <div class="data-stored" style="opacity: {{ $saved?'1':'0' }};">
                    Налаштування профілю збережені
                </div>

                <div class="person__holder" style="margin-bottom: 3em">
                    <div class="person__avatar animated">
                        <img src="{{ Auth::user()->icon }}"/>
                    </div>
                    <div class="person__name">{{ Auth::user()->name }}</div>
                    <div class="person__social">(авторизований через <a href="{{ Auth::user()->getAccountLink() }}" target="_blank">{{ Auth::user()->getSocialName() }}</a>)</div>
                </div>

                <div class="row">
                    <div class="medium-2 columns">&nbsp;</div>
                    <div class="medium-4 columns">
                        <fieldset>
                            <div class="fields__row nbm">
                                <div class="floating-data" data-item="phone">
                                    <div class="floating-label" style="opacity: 1">Телефон</div>
                                    <input type="text" class="floating-input-masked" placeholder="+38 (___) ___ __ __"
                                           name="phone"
                                           value="{{ (old('phone'))?:Auth::user()->phone }}"/>

                                    <div class="floating-validation">&nbsp;</div>
                                </div>
                            </div>
                            <div class="fields__row nbm">
                                <div class="floating-data" data-item="skype">
                                    <div class="floating-label">Skype</div>
                                    <input type="text" class="floating-input" placeholder="Skype"
                                           name="skype"
                                           value="{{ old('skype')?:((Auth::user()->skype)?Auth::user()->skype:'') }}"/>
                                    <div class="floating-validation">&nbsp;</div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="medium-4 columns">
                        <div class="description" style="margin-top: 1em">
                            Ці контакти будуть відображатися у Ваших оголошеннях.
                            Користувачі їх побачать по кліку на кнопку "Показати контакти"
                        </div>
                    </div>
                    <div class="medium-2 columns">&nbsp;</div>
                </div>
            </div>
        </div>
        <div class="form__submit">
            <div class="row form__content">
                <button type="submit" class="btn__submit-notice">Зберегти</button>
            </div>
        </div>

        {!! csrf_field() !!}

    </form>
@endsection