@extends('app.layout')

@section('title', $notice->getTitle() . ' —')
@section('page', 'one')

@section('head')
    {{--OpenGraph--}}
    <meta property="og:title" content="{{ $notice->getTitle() }}">
    <meta property="og:description" content="{{ $notice->getBrief() }}">
    <meta property="og:locale" content="uk_UA">
    <meta property="og:type" content="product">
    <meta property="og:url" content="{{ $notice->getPermalink() }}">
    @if($link = $notice->getImageForSocialNetworks())
    <meta property="og:image" content="{{ $link }}">
    <meta property="og:image:height" content="420">
    <meta property="og:image:width" content="800">
    @endif
    <meta property="product:price:amount" content="{{ $notice->price }}">

    {{--Schema.org--}}
    <script type="application/ld+json">
    {   "@context": "http://schema.org",
        "@type": "Product",
        "name": "{{ $notice->getMetaName() }}",
        "description": "{{ $notice->generateBrief(true) }}",
        @if($preview = $notice->mainAttachment)
            "image": "{{ $preview->getImageUrl('r100x100') }}",
        @endif
        "offers": {
            "@type": "Offer",
            "price": "{{ $notice->price }}",
            "priceCurrency": "UAH",
            "availableAtOrFrom": {
                "@type": "Place",
                "address": {
                    "@type": "PostalAddress",
                    "addressLocality": "Kyiv",
                    "addressCountry": "UA",
                    "streetAddress": "{{ $notice->getAddress() }}"
                }
            }
        }
    }
    </script>

    <script type="application/ld+json">
        {!! $microdata !!}
    </script>

    <meta property="description" content="{{ $notice->generateBrief(true) }}">

    <script type="text/javascript">
        var oneLat = {{ $notice->map_lat }};
        var oneLng = {{ $notice->map_lng }};
    </script>
@endsection

@section('content')

    <div class="modal" style="display: none">
        @include('app.parts.modal-login')
        @include('app.parts.modal-confirm')
    </div>

    <div class="column-small-collapse fixed__holder"></div>

    @if($notice->attachments->count())
        <div id="photo-gallery" class="photo__gallery">
            <div class="gallery__content">
                @foreach($notice->attachments as $attachment)
                    <a class="gallery__item fancybox" rel="gallery" href="{{$attachment->getImageUrl()}}">
                        <img src="{{$attachment->getImageUrl('rx440')}}"/>
                    </a>
                @endforeach
            </div>
            <div class="gallery__overlay"></div>
            <div class="gallery__navigation">
                <div class="nav__left">
                    <div class="gallery__arrow gallery__arrow-left">
                        <a href="#">
                            <span class="left"></span>
                        </a>
                    </div>
                </div>
                <div class="nav__right">
                    <div class="gallery__arrow gallery__arrow-right">
                        <a href="#">
                            <span class="right"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="gallery__title">
                <strong>{{$notice->getTitle()}}</strong>
                <span>{!! $notice->getInfo() !!}</span>
            </div>
        </div>
    @endif

    <div class="contact__info-mobile">
        <span class="details">Показати контакти</span>
    </div>

    <div class="top">
        <div class="row content__holder">
            <div class="medium-1 columns">&nbsp;</div>
            <div class="medium-7 columns">
                @if($notice->status == \App\Notice::STATUS_BOOKED)
                    @if($notices = App\Notice::nQuery([
                            'limit' => 4,
                            ['price', '>=', $notice->price - 500],
                            ['price', '<=', $notice->price + 500]
                        ]))
                        <div class="sug__data">
                            <div class="data__title">Цей об’єкт зданий. Пропонуємо глянути</div>
                            <div class="data__notices data__notices--masonry">
                                @each('app.notice.feed', $notices, 'notice')
                                <div style="clear: both"></div>
                            </div>
                        </div>
                    @endif
                @endif
                <div class="row notice notice__title @if($notice->attachments->count()) hide-gallery-on-mobile @endif">{{$notice->getTitle()}}</div>
                <div class="row notice notice__date">{{$notice->getDate()}}</div>
                <div class="row notice notice__details @if($notice->attachments->count()) hide-gallery-on-mobile @endif">{!! $notice->getInfo() !!}</div>
                <div class="row notice notice__text">
                    <p><em>{{ $notice->brief }}</em></p>
                    {!! $notice->getDescription() !!}
                </div>
                @if($stations = $notice->getStations())
                    <div class="row notice notice__stations">
                        @foreach($stations as $station)
                            <div class="notice__station">
                                <div class="station__big-{{$station['type']}}"></div>
                                <div class="station__big_title">{{$station['title']}}</div>
                                @if(isset($station['info']))
                                    <div class="station__big_comments">{!! $station['info'] !!}</div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif

                <div id="new-map-canvas"></div>

                @if(count($notice->highlights))
                    <div class="row notice notice__highlights">
                        <div class="highlights__title">
                            Зручності
                        </div>
                        <div class="highlights__list">
                            <ul class="highlights">
                                @foreach($notice->highlights as $highlight)
                                    <li class="highlights__item"><i class="icon-{{ $highlight->icon }}"></i><span>{{ $highlight->title }}</span></li>
                                @endforeach
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                @endif
                <div style="margin-bottom: 1em">&nbsp;</div>
            </div>
            <div class="medium-3 columns columns-contact">
                <div class="contact__wrapper">
                    @include('app.parts.contact')
                </div>
            </div>
            <div class="medium-1 columns"></div>
        </div>

        @if($notices = App\Notice::nQuery([
                                    'limit' => 4,
                                    'rand' => true,
                                    ['rooms', '=', $notice->rooms],
                                    ['type', '=', $notice->type]
                                ]))
            <div class="sug__holder">
                <div class="row">
                    <div class="sug__data">
                        <div class="data__title">{{trans('general.other')}} {{trans('general.room-n-' . ($notice->rooms?($notice->rooms>=4?4:$notice->rooms):''))}} {{trans('general.' . $notice->type . 's')}}</div>
                        <div class="data__notices data__notices-3">
                            @each('app.notice.feed', $notices, 'notice')
                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('footer-scripts')
    <link rel="stylesheet" href="{{asset('css/perfect-scrollbar.min.css')}}" />
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANg000iSUrqzrlhqWBTjVzZGMTM3aJVgg"></script>
@endsection