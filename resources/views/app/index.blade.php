@extends('app.layout')
@if($title = (isset($isSearch) && $isSearch)? 'Пошук — ' : '')@endif
@section('title', $title)
@section('postTitle', $title?'':' — довгострокова оренда житла в Києві')
@section('page', 'index')
@section('head')
    <style>
        #map-canvas {
            position: absolute;
            width: 100%;
        }
    </style>

    @include('app.partials.meta-static')

    @if(isset($micro))
        <script type="application/ld+json">
            {!! $micro !!}
        </script>

        {{--@ Remake in nearest future @--}}
        <meta property="description" content="{!! json_decode($micro)->description !!}">
    @endif

    <script type="text/javascript">
        window.gmMarkers = [];
        window.asyncSearch = {{ isset($asyncSearch)? 'true' : 'false' }};
        @if(isset($searchString)) window.searchString = '{!! $searchString !!}';@endif
    </script>
@endsection

@section('content')

    <div class="modal" style="display: none">
        @include('app.parts.modal-login')
        @include('app.parts.modal-search')
    </div>


    @if (app()->request->route() && app()->request->route()->getName()!= 'index')
    <div class="column-small-collapse fixed__holder"></div>
    @endif

    @if(app()->request->route() && app()->request->route()->getName()== 'my')
        @include('app.parts.my-landing')
    @endif

    <div id="map-canvas" style="background-color: #e5e3df"></div>

    <div class="map"  style="background-color: #e5e3df">
        <div class="feed__holder">

            <div class="row">
                <div class="medium-6 no-padding feed__row" style="z-index: 12">
                    @if(isset($isSearch) && $isSearch)
                        @include('app.parts.full-search')
                    @endif
                    <div class="feed__wrapper" id="feed-wrapper">
                        @if(!isset($isSearch) || !$isSearch)
                            <div class="feed__title">
                                {{ $noticesTitle }}
                            </div>
                        @endif
                        <div class="feed__list">
                            @if(count($notices))
                                @each('app.notice.feed', $notices, 'notice')
                            @endif
                        </div>
                        @include('app.parts.no-records')
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANg000iSUrqzrlhqWBTjVzZGMTM3aJVgg"></script>
@endsection