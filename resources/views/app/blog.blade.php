@extends('app.layout')

@section('page', 'blogItem')

@section('head')
    <link href='https://fonts.googleapis.com/css?family=Lora&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
@endsection

@section('specialHeaderClasses', 'overmap')

@section('content')

    <div class="modal" style="display: none">
        @include('app.parts.modal-login')
    </div>

    <div class="column-small-collapse fixed__holder"></div>

    <div class="row blog">
        <div class="medium-7 medium-centered columns">
            @include('pages.' . $page)
        </div>
    </div>
@endsection