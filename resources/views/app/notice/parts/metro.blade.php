@if ($metro = \App\MetroStation::find($id))
    <li class="{{ $metro->icon }}">
        <div class="txt">{{ $metro->title }}</div>
        <div class="timer"></div>
        <div class="cross"></div>
        <input type="hidden" class="stationId" name="metro[{{ $id }}][id]" value="{{ $id }}">
        <input type="hidden" class="onFoot" name="metro[{{ $id }}][foot]" value="{{ $timing['foot'] }}">
        <input type="hidden" class="onTransport" name="metro[{{ $id }}][transport]" value="{{ $timing['transport'] }}">
    </li>
@endif