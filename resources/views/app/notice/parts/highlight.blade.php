@if ($highlight = \App\Highlight::find($highlightId))
    <li>
        <div class="txt"><i class="icon-{{ $highlight->icon }}"></i><span>{{ $highlight->title }}</span></div>
        <div class="cross"></div>
        <input type="hidden" name="highlight[]" value="{{ $highlightId }}">
    </li>
@endif