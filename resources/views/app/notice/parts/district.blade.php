@if ($district = \App\District::find($districtId))
    <li data-district="{{ $district->getType($district->type) }}">
        <div class="txt">{{ $district->title }} <span>({{ $district->getType($district->type) }})</span></div>
        <div class="cross"></div>
        <input type="hidden" name="districts[]" value="{{ $districtId }}">
    </li>
@endif