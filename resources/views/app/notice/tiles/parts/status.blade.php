@if($notice->belongsToAuthorized() && app()->request->route()->getName() == 'my')
<div class="data__status">
    <span class="status status-{{$notice->status}}" title="{{trans('notice.status.'.$notice->status.'.description')}}">
        {{trans('notice.status.'.$notice->status.'.title')}}
    </span>
</div>
@endif