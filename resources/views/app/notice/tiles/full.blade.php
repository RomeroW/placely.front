{{-- @var $notice App\Notice Notice model --}}

<a class="feed__one feed__item feed__item-full"
     href="{{$notice->getPermalink()}}"
     data-lat="{{$notice->map_lat}}"
     data-lng="{{$notice->map_lng}}"
     data-id="{{$notice->id}}"
     data-description="{{$notice->getShortInfo()}}">

    <div class="notice__image" style="background-image: url({{$preview->getImageUrl('r500x260')}})">
        <div class="notice__mask">
            @if($amenities = $notice->previewAmenities)
            <ul class="image__amenities">
                @foreach ($amenities as $amenity)
                <li title="{{$amenity->title}}"><i class="icon-{{$amenity->icon}}"></i></li>
                @endforeach
            </ul>
            @endif
            <div class="image__short_info">
                <div class="info__price">₴{{$notice->price}}</div>
                <div class="info__type">{{$notice->getShortType()}}</div>
            </div>
            <div class="image__date">{{$notice->getDate()}}</div>
            @if($attachments = $notice->attachments)
                <div class="image__photos"><i class="icon-camera"></i> {{count($attachments)}}</div>
            @endif
        </div>
    </div>
    <div class="notice__data">

        @include('app.notice.tiles.parts.status')

        <div class="data__address">{{$notice->getAddress()}}</div>
        <div class="data__brief">{{$notice->getBrief()}}</div>
        @if($stations = $notice->getStations())
        <ul class="data__metro">
            @foreach($stations as $station)
            <li class="station station-{{$station['type']}}">
                <div class="station__title">{{$station['title']}}</div>
                <div class="station__timing">&nbsp;
                    @if($station['foot'])
                    <span class="foot" title="Пішки до станції">{{$station['foot']}}хв</span>
                    @endif
                    @if($station['foot'] && $station['transport'])
                    <span>/</span>
                    @endif
                    @if($station['transport'])
                    <span class="transport" title="Транспортом до станції">{{$station['transport']}}хв</span>
                    @endif
                </div>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
</a>