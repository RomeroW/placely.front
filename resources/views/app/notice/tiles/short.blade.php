<a class="feed__one feed__item feed__item-short"
   href="{{$notice->getPermalink()}}"
   data-lat="{{$notice->map_lat}}"
   data-lng="{{$notice->map_lng}}"
   data-id="{{$notice->id}}"
   data-description="{{$notice->getShortInfo()}}">

    <div class="notice__data">
        <div class="data__info">₴{{$notice->price}}, {{$notice->getShortType()}}</div>
        @include('app.notice.tiles.parts.status')
        <div class="data__brief">{{$notice->getAddress()}}. {{$notice->getBrief()}}</div>
        @if($stations = $notice->getStations())
        <ul class="data__metro">
            @foreach($stations as $station)
            <li class="station station-{{$station['type']}}">
                <div class="station__title">{{$station['title']}}</div>
                <div class="station__timing">&nbsp;
                    @if($station['foot'])
                    <span class="foot" title="Пішки до станції">{{$station['foot']}}хв</span>
                    @endif
                    @if($station['foot'] && $station['transport'])
                    <span>/</span>
                    @endif
                    @if($station['transport'])
                    <span class="transport" title="Транспортом до станції">{{$station['transport']}}хв</span>
                    @endif
                </div>
            </li>
            @endforeach
        </ul>
        @endif
        @if($amenities = $notice->previewAmenities)
        <ul class="data__amenities">
            @foreach ($amenities as $amenity)
            <li title="{{$amenity->title}}"><i class="icon-{{$amenity->icon}}"></i></li>
            @endforeach
        </ul>
        @endif
        <div class="data__time">{{$notice->getDate()}}</div>
    </div>
</a>