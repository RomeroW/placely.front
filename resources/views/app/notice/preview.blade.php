<html>
<meta charset="utf-8">
<link rel="stylesheet" href="{{asset('css/project.min.css')}}">
</html>
<body style="position: relative; height: 100%">
<div class="preview" style="background-image: url({{$preview}})">
    <div class="preview__mask">
        <div class="preview__logo">
            <img src="{{ asset('images/whitelogo.png') }}"/>
        </div>
        <div class="preview__price">
            ₴{{ $notice->price }}/<span>міс.</span>
        </div>
        <div class="preview__heading">
            {{$notice->getAddress()}}
        </div>
        <div class="preview__type">
            {{$notice->getShortType()}}
        </div>
        @if($amenities = $notice->previewAmenities)
            <ul class="preview__amenities">
                @foreach ($amenities as $amenity)
                    <li title="{{$amenity->title}}"><i class="icon-{{$amenity->icon}}"></i></li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
</body>