{{--@var $notice App\Notice Notice model--}}

@if($preview = $notice->mainAttachment)
    @include('app.notice.tiles.full')
@else
    @include('app.notice.tiles.short')
@endif