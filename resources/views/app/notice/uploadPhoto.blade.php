@if ($photo = \App\Attachment::find($photoId))
    <div dz-preload style="display: none" data-url="{{ $photo->getImageUrl('r95x95') }}" data-id="{{ $photo->id }}"></div>
@endif