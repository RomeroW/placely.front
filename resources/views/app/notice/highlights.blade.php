@if($class = (count($notice->highlights) > 1 && count($notice->highlights) <= 8)? '7' : '3' )@endif
@if($class = (count($notice->highlights) > 8)? '12' : $class )@endif
<div class="row notice notice__highlights">
    <div class="highlights__title">
        Зручності
    </div>
    <div class="highlights__list">
        <div class="medium-12 medium-centered columns">
            <div class="medium-{{ $class }} medium-centered columns">
                <ul class="h{{ $class }}">
                    @foreach($notice->highlights as $highlight)
                        <li class="highlights__item"><i class="icon-{{ $highlight->icon }}"></i><span>{{ $highlight->title }}</span></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>