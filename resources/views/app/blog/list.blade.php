@extends('app.layout')

@section('title', 'Блог про нерухомість  —')
@section('page', 'blogItem')

@section('head')
    <link href='https://fonts.googleapis.com/css?family=Lora&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
@endsection

@section('specialHeaderClasses', 'overmap')

@section('content')

    <div class="modal" style="display: none">
        @include('app.parts.modal-login')
    </div>

    <div class="column-small-collapse fixed__holder fixed__holder-small"></div>

    <div class="blog__header">
        <div class="blog__mask">
            <div class="blog__head">Блог Placely про нерухомість</div>
        </div>
    </div>

    <div class="row blog">
        <div class="large-7 medium-9 medium-centered columns">
            @foreach($posts as $post)
                <div class="item">
                    <a class="item__header" href="{{$post->getPermalink()}}">
                        {{$post->title}}
                    </a>
                    <div class="item__content">
                        {!! $post->getShort() !!}
                    </div>
                    <div class="item__date">
                        {{ (new Date($post->created_at))->format('F j, Y') }}
                    </div>
                </div>
            @endforeach
        </div>

        <div class="large-7 medium-9 medium-centered columns">
            @include('app.blog.partials.social-big-like')
        </div>
    </div>
@endsection