<div class="social__share">
    {{--<a href="#" class="share share-placely">--}}
        {{--Підписатися на Placely--}}
        {{--</a>--}}
    <a href="http://www.facebook.com/sharer/sharer.php?u={{$post->getPermalink()}}" class="share share-facebook">
        <i class="icon-facebook-official"></i> Поділитися у Facebook
    </a>
    <a href="http://vkontakte.ru/share.php?url={{$post->getPermalink()}}" class="share share-vk">
        <i class="icon-vkontakte"></i> Поділитися Вконтакті
    </a>
</div>