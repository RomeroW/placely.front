@extends('app.layout')

@section('title', $post->title . ' —')
@section('page', 'blogItem')

@section('head')
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700' rel='stylesheet' type='text/css'>

    <meta property="og:title" content="{{ $post->title }}">
    <meta property="og:description" content="{{ $post->getShort() }}">
    {{--<meta property="og:locale" content="uk_UA">--}}
    {{--<meta property="og:type" content="product">--}}
    <meta property="og:url" content="{{ url('/blog/' . $post->slug) }}">
    <meta property="og:image" content="{{ url('/blog/preview/' . $post->slug . '.jpg') }}">
    <meta property="og:image:height" content="420">
    <meta property="og:image:width" content="800">
@endsection

@section('specialHeaderClasses', 'overmap')

@section('content')
    <div class="modal" style="display: none">
        @include('app.parts.modal-login')
    </div>

    <div class="column-small-collapse fixed__holder fixed__holder-small"></div>

    <div class="blog post__header">
        <div class="row">
            <div class="large-7 medium-9 medium-centered columns">
                <div class="post__link">
                    <a href="{{url('/blog')}}">Блог Placely</a>
                </div>
                <h1>{{$post->title}}</h1>
            </div>
        </div>
    </div>
    <div class="row blog">
        <div class="large-7 medium-9 medium-centered columns">
            <div class="social__share social__share-top">
                {{--<a href="#" class="share share-placely">--}}
                    {{--Підписатися на Placely--}}
                {{--</a>--}}
                <a href="http://www.facebook.com/sharer/sharer.php?u={{$post->getPermalink()}}" class="share share-facebook">
                    <i class="icon-facebook-official"></i> Поділитися у Facebook
                </a>
                <a href="http://vkontakte.ru/share.php?url={{$post->getPermalink()}}" class="share share-vk">
                    <i class="icon-vkontakte"></i> Поділитися Вконтакті
                </a>
            </div>
            <div class="content">
                {!! $post->render() !!}
            </div>
        </div>
        <div class="large-7 medium-9 medium-centered columns social__block">

            @include('app.blog.partials.social-share-buttons')
            @include('app.blog.partials.social-big-like')

            <div class="social__comments">
                <div class="fb-comments" data-href="{{$post->getPermalink()}}" data-width="100%" data-numposts="5"></div>
            </div>

        </div>

    </div>
@endsection