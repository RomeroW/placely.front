@if (count($errors) > 0)
    var validationErrors = {
    @foreach ($errors->getMessages() as $field => $errorList)
        {{ $field }}: "{{ implode(',', $errorList) }}"@if(array_keys($errors->getMessages())[count($errors)-1] != $field),@endif

    @endforeach
    };
@endif