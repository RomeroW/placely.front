<li id="dropzoneTemplate">
    <div class="progress">
        <div class="progress-bar">
            <span style="width: 0%;" data-dz-uploadprogress></span>
        </div>
    </div>
    <img data-dz-thumbnail/>
    <input type="hidden" dz-image-input/>
    <div class="mainStatus" data-main="Головне фото" data-select-main="Клікніть, щоб зробити головним"></div>
    <div class="cross" data-dz-remove></div>
</li>