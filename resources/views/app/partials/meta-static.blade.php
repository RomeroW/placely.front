@if(isset($meta))
  @foreach($meta as $key => $value)
    @foreach($value as $tag)
      <meta property="{{ $tag['property'] }}" content="{{ $tag['content'] }}">
    @endforeach
  @endforeach
@endif