@extends('app.layout')

@section('title', _('Нове оголошення —'))

@section('page', 'form')

@section('head')

    <meta property="og:image" content="{{url('images/ogimage.jpg')}}">
    <meta property="og:image:height" content="315">
    <meta property="og:image:width" content="600">


    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Website",
            "name": "Placely — створити оголошення",
            "image": "http://www.placely.io/images/logon2.png",
            "url": "http://www.placely.io/create",
            "description": "Довгострокова оренда житла (квартири, кімнати) в Києві, без посередників, від власників."
        }
    </script>

    <script type="text/javascript">
        var config = {
            attachment: "{{url('attachment')}}"
        };
        var metros = {!! $metros !!};
        var facilities = {!! $facilities !!};
        var districts = {!! $districts !!};
        var streets = {!! $streets !!};

        @if(old('mapLng') && old('mapLat'))
        var mapPosition = {
            lat: {{old('mapLat')}},
            lng: {{old('mapLng')}}
        };
        @endif

        @if(old('type'))
        var switchType = "{{ old('type') }}";
        @endif

        @include('app.partials.validation-errors')
    </script>
@endsection

@section('content')
    <div class="modal" style="display: none;">
        <div class="modal__holder modal__am">
            <div class="modal__head">
                <span>Зручності</span> <span class="description">(8 обрано)</span>

                <div class="cross"></div>
            </div>
            <div class="modal__body">
                <div class="row">
                    <div class="medium-12 columns am__all">
                        <div class="am__holder-all">
                            @foreach($amenities as $amenity)
                                <div
                                        class="am__item {{ (is_array(old('highlight')) && in_array($amenity->id, old('highlight')))? 'active' : '' }}"
                                        title="{{ $amenity->description }}" data-id="{{ $amenity->id }}">
                                    <div class="txt"><i
                                                class="icon-{{ $amenity->icon }}"></i><span>{{ $amenity->title }}</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal__footer">
                <button class="btn__fine">Готово</button>
            </div>
        </div>
        <div class="modal__holder modal__map">
            <div class="modal__head">
                <span>Уточніть позицію на карті</span>

                <div class="cross"></div>
            </div>
            <div class="modal__body">
                <div id="map-modal"></div>
            </div>
            <div class="modal__footer">
                <button class="btn__fine">Готово</button>
            </div>
        </div>
        <div class="modal__holder modal__station">
            <div class="modal__head">
                {{--<span class="station__small-red">Політехнічний інститут</span>--}}
                <span></span>

                <div class="cross"></div>
            </div>
            <div class="modal__body">
                <div class="fields__validation_error-metro">Максимальна кількість символів - 2</div>
                <div class="fields__row" data-val-timing data-val-length="[0, 2]">
                    <label>
                        <span>Пішки</span>
                        <input type="text" class="modalOnFoot" placeholder="Хвилин" data-only-time>
                    </label>
                </div>
                <div class="fields__row" data-val-timing data-val-length="[0, 2]">
                    <label>
                        <span>Транспортом</span>
                        <input type="text" class="modalOnTransport" placeholder="Хвилин" data-only-time>
                    </label>
                </div>
            </div>
            <div class="modal__footer-borderless">
                <button class="btn__fine">Готово</button>
            </div>
        </div>
        @include('app.parts.modal-login')
    </div>

    <div class="column-small-collapse fixed__holder-new"></div>

    <form method="post" action="{{url('create')}}" id="formCreateNotice">
        <input type="hidden" name="createSessionHash" id="createSessionHash"
               value="{{ old('createSessionHash')?: $createSessionHash }}">

        <div class="form__head">
            <div class="row form__content">
                <div class="form__title">Додати нове оголошення</div>
                <div class="form__select_holder no-select">
                    <div class="form__select_item active" data-id="apartment">Квартира</div>
                    <div class="form__select_item" data-id="room">Кімната</div>
                </div>
                <input type="hidden" id="type" name="type" value="apartment"/>
            </div>
        </div>
        <div class="form__general">
            <div class="fields__validation_error">Деякі поля не заповнені, чи заповнені не правильно.</div>
            <div class="row form__content">
                <div class="fields__title">Загальна інформація</div>
                <fieldset>
                    <div class="fields__row nbm">
                        <div class="floating-data" data-item="price" data-val-required data-val-length='[1, null]'>
                            <div class="floating-label">Ціна *</div>
                            <input type="text" class="floating-input px" data-only-digits placeholder="Ціна *"
                                   name="price"
                                   value="{{ old('price') }}"/>

                            <div class="floating-postfix">грн</div>
                            <div class="floating-validation">&nbsp;</div>
                        </div>
                    </div>
                    <div class="fields__row nbm" data-id="apartment">
                        <div class="floating-data" data-val-optional data-val-length="[0, 2]">
                            <div class="floating-label">К-сть кімнат</div>
                            <input type="text" class="floating-input" data-only-digits placeholder="К-сть кімнат"
                                   name="rooms"
                                   value="{{ old('rooms') }}"/>

                            <div class="floating-validation">&nbsp;</div>
                        </div>
                    </div>
                    <div class="fields__row nbm row" data-id="apartment">
                        <div class="medium-4 columns">
                            <div class="fields__row nbm">
                                <div class="floating-data" data-val-optional data-val-length="[0, 4]">
                                    <div class="floating-label">Загальна площа</div>
                                    <input type="text" class="floating-input px" data-only-digits
                                           placeholder="Загальна площа"
                                           data-target="switch" name="total_space" value="{{ old('total_space') }}"/>

                                    <div class="floating-postfix">м<sup>2</sup></div>
                                    <div class="floating-validation">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="medium-4 columns">
                            <div class="fields__row nbm">
                                <div class="floating-data" data-val-optional data-val-length="[0, 4]">
                                    <div class="floating-label">Ефективна площа</div>
                                    <input type="text" class="floating-input px" data-only-digits
                                           placeholder="Ефективна площа"
                                           name="effective_space" value="{{ old('effective_space') }}"/>

                                    <div class="floating-postfix">м<sup>2</sup></div>
                                    <div class="floating-validation">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="medium-4 columns">
                            <div class="fields__row nbm" data-id="apartment">
                                <div class="floating-data" data-val-optional data-val-length="[0, 4]">
                                    <div class="floating-label">Житлова площа</div>
                                    <input type="text" class="floating-input px" data-only-digits
                                           placeholder="Житлова площа"
                                           name="living_space" value="{{ old('living_space') }}"/>

                                    <div class="floating-postfix">м<sup>2</sup></div>
                                    <div class="floating-validation">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields__row nbm disabled" data-id="room">
                        <div class="floating-data" data-val-optional data-val-length="[0, 4]">
                            <div class="floating-label">Площа</div>
                            <input type="text" class="floating-input px" data-only-digits data-target="switch" disabled
                                   placeholder="Площа" name="total_space" value="{{ old('total_space') }}"/>

                            <div class="floating-postfix">м<sup>2</sup></div>
                            <div class="floating-validation">&nbsp;</div>
                        </div>
                    </div>
                    <div class="fields__row">
                        <div class="floating-data" data-item="description" style="width: 100%">
                            <div class="floating-label" id="description-label">Опис</div>
                            <div class="de">
                                <div class="de__item">
                            <textarea rows="2" name="brief" data-max="140" class="floating-input"
                                      data-label="description-label" placeholder="Опис"
                                      id="ta-brief">{{ old('brief') }}</textarea>

                                    <div class="de__info">
                                        <div class="left">Бріф (Ваш шанс знайти саме свого клієнта)</div>
                                        <div class="right counter" data-count="ta-brief"><span>0</span> / 140</div>
                                    </div>
                                </div>
                                <div class="de__item">
                            <textarea rows="6" id="ta-description" name="description" class="floating-input"
                                      data-label="description-label" data-max="2500">{{ old('description') }}</textarea>

                                    <div class="de__info">
                                        <div class="left">Опис</div>
                                        <div class="right counter" data-count="ta-description"><span>0</span> / 2500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="floating-validation">&nbsp;</div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="form__address">
            <div class="row form__content">
                <div class="fields__title">Адреса</div>
                <div class="fields__description">Почніть вводити назву вулиці та оберіть із отриманого списку</div>
                <fieldset>
                    <div class="fields__row">
                        <div class="floating-data" data-item="street" data-val-required data-val-autocomplete
                             style="width: 270px; display: inline-block; margin-right: 1em">
                            <div class="floating-label">Вулиця *</div>
                            <input type="text" class="floating-input" placeholder="Вулиця *" name="street" id="street"
                                   value="{{ old('street') }}"/>
                            <input type="hidden" id="streetId" name="street_id" value="{{ old('street_id') }}"/>

                            <div class="floating-validation" style="position: absolute">&nbsp;</div>
                        </div>
                        <div class="floating-data" style="width: 140px; display: inline-block">
                            <div class="floating-label">Номер будинку</div>
                            <input type="text" class="floating-input" placeholder="Номер будинку" id="house"
                                   name="building"
                                   value="{{ old('building') }}"/>

                            <div class="floating-validation" style="position: absolute">&nbsp;</div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div id="new-map-canvas"></div>
        <input id="mapLat" name="mapLat" type="hidden"/>
        <input id="mapLng" name="mapLng" type="hidden"/>

        <div id="correct-map"><span>мітка стала не правильно?</span></div>

        <div class="form__highlights">
            <div class="row form__content">
                <div class="fields__title">Зручності</div>
                <div class="fields__description">
                    <div id="am__select" class="am__select">Вибрати</div>
                </div>
                {{--<fieldset>--}}
                {{--<div class="fields__row">--}}
                {{--<input type="text" placeholder="Зручність" id="highlight"/>--}}
                {{--</div>--}}
                {{--</fieldset>--}}
                <ul class="highlights__list">
                    @each('app.notice.parts.highlight', old('highlight'), 'highlightId')
                </ul>
            </div>
        </div>

        <div class="form__metro_stations">
            <div class="row form__content">
                <div class="fields__title">Найближчі станції метро</div>
                <fieldset>
                    <div class="fields__row">
                        <input type="text" placeholder="Станція метро" id="metro"/>
                    </div>
                </fieldset>
                <ul class="metro__list">
                    @if(old('metro'))
                        @foreach(old('metro') as $stationId => $timing)
                            @include('app.notice.parts.metro', ['id' => $stationId, 'timing' => $timing])
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>

        <div class="form__photos">
            <div class="row form__content">
                <div class="fields__title">Фото</div>
                <div class="fields__row">
                    <div class="photo__holder" dz-input="photos">
                        Перетягніть фото сюди чи клікніть, щоб вибрати</br>
                        (максимальний розмір одного фото: 5 МБ)
                        <div class="fields__validation_error-photo">Дозволені тільки зображення.</div>
                    </div>
                </div>
                @each('app.notice.parts.photo', old('photos'), 'photoId')

                <input type="hidden" name="mainPreview" value="{{old('mainPreview')}}"/>
                <ul class="photo__list">
                    @include('app.partials.dropzone-preview-template')
                </ul>
            </div>
        </div>

        <div class="form__districts">
            <div class="row form__content">
                <div class="fields__title">Район</div>
                <div class="fields__description">Можете обрати адміністративний район, а потім мікрорайон зі списку
                </div>
                <fieldset>
                    <div class="fields__row">
                        <select id="districts">
                            <option selected="selected" disabled="disabled">Оберіть зі списку</option>
                        </select>
                    </div>
                </fieldset>
                <ul class="districts__list">
                    @each('app.notice.parts.district', old('districts'), 'districtId')
                </ul>
            </div>
        </div>

        <div class="form__submit">
            <div class="row form__content">
                <button type="submit" class="btn__submit-notice">Розмістити</button>
            </div>
        </div>

        {!! csrf_field() !!}

    </form>
@endsection

@section('footer-scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANg000iSUrqzrlhqWBTjVzZGMTM3aJVgg"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{asset('js/dropzone.min.js')}}"></script>
@endsection
