<div class="search">
    <div id="search-modal-holders">
        @include('app.parts.search-modals.location')
        @include('app.parts.search-modals.price')
        @include('app.parts.search-modals.specify')
    </div>


    <div class="row search__fast">
        <div class="search__title">
            Житло в Києві без посередників
        </div>
        <div class="search__description">
            довгострокова оренда
        </div>
        <div class="search__form">
            <div class="mobile-collapsed">
                <form method="get" action="search" async-action="notice/search">
                    <div class="search__group">
                        <div class="search__input_holder search__input_holder-location">
                            <input type="hidden" name="location"/>
                            <div class="search__input search__input-district trigger-location">
                                <span class="icon-location"><span class="placeholder" data-placeholder="Локація">Локація</span></span>
                            </div>
                        </div>

                        <div class="search__input_holder search__input_holder-price">
                            <div class="search__input search__input-price trigger-price">
                                <span class="icon-price"><span class="placeholder" data-placeholder="Ціна">Ціна</span></span>
                            </div>
                        </div>

                        <div class="search__input_holder search__input_holder-specify">
                            <div class="search__input search__input-specify trigger-specify">
                                <span class="icon-blank"><span class="placeholder" data-placeholder="Уточнити">Уточнити</span></span>
                            </div>
                        </div>

                        <input type="submit" class="search-submit-btn" id="search-submit" value="Шукати"/>
                    </div>
                </form>
            </div>
            <div class="mobile-only">
                <div class="search__fake_holder">
                    <div class="search__fake_input">Що шукати?</div>
                    <div class="search__fake_button">Шукати</div>
                </div>
            </div>
        </div>
        {{--<div class="search__extended_link"><a href="/search">розширений пошук</a></div>--}}
    </div>
</div>