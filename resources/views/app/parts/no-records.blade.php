<div class="feed__empty {{ !count($notices)? 'feed__empty-visible' : '' }}">
    @if($type = isset($asyncSearch)? 'search' : 'container') @endif
    <div class="h1">
        {{ trans('general.empty_feed.'.$type.'.header') }}
    </div>
    <div class="d1">
        {{ trans('general.empty_feed.'.$type.'.description') }}
    </div>
</div>
