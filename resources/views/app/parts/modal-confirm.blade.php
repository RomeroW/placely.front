<div class="modal__holder modal__confirm">
    <div class="modal__head">
        <span>Підтвердіть дію</span>
        <div class="cross"></div>
    </div>
    <div class="modal__body">
        <div class="confirm__modal_description">
            Ви впевнені? Скасувати цю дію буде неможливо
        </div>
    </div>
    <div class="modal__footer">
        <div class="btn btn-confirm" id="yes">Підтвердити</div>
        <div class="btn btn-no" id="no">Відмінити</div>
    </div>
</div>