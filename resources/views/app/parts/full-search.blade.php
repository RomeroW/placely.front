<div class="search__info-mobile">
    <div class="search__info-holder">
        <div class="left">Знайдено оголошень: <span class="results-count">{{ $count }}</span></div>
        <div class="right"><a class="filters" href="#">всі фільтри</a></div>
    </div>
</div>
<div class="search__stick mobile-modal">
    <div class="search__info-holder">
        <div class="left">Знайдено оголошень: <span class="results-count">{{ $count }}</span></div>
        <div class="right"><a class="filters" href="#">всі фільтри</a></div>
    </div>
    <div class="stick__holder"></div>
</div>
<div class="search__layout"></div>
<div class="search__full-form">
    <div class="search__full">
        <div id="search-modal-holders">
            @include('app.parts.search-modals.location')
            @include('app.parts.search-modals.price')
        </div>
        <div class="search-title">Знайдено оголошень: <span class="results-count">{{ $count }}</span></div>
        <div class="search-fields">
            <div class="search__form row">
                <div class="medium-8 columns">
                    <div class="search__input_holder search__input_holder-location">
                        <div class="search__input search__input-district trigger-location">
                            <span class="icon-location"><span class="placeholder" data-placeholder="Локація">Локація</span></span>
                        </div>
                    </div>
                </div>
                <div class="medium-4 columns">
                    <div class="search__input_holder search__input_holder-price">
                        <div class="search__input search__input-price trigger-price">
                            <span class="icon-price"><span class="placeholder" data-placeholder="Ціна">Ціна</span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search__block row">
                @if($t = explode(',',app()->request->get('t')))@endif
                <div class="medium-3 columns"><div class="title">Тип житла</div></div>
                <div class="medium-9 columns">
                    <div class="search__type">
                        <div class="search__tick-holder">
                            <label class="search__tick">
                                <input type="checkbox" {{ (is_array($t) && in_array('0', $t))?'checked':'' }}
                                       name="t" value="0"><span></span><br/>
                                Кімната
                            </label>
                        </div>
                        <div class="search__tick-holder">
                            <label class="search__tick">
                                <input type="checkbox" {{ (is_array($t) && in_array(1, $t))?'checked':'' }}
                                       name="t" value="1"><span></span><br/>
                                1к.<br/>квартира
                            </label>
                        </div>
                        <div class="search__tick-holder">
                            <label class="search__tick">
                                <input type="checkbox" {{ (is_array($t) && in_array(2, $t))?'checked':'' }}
                                       name="t" value="2"><span></span><br/>
                                2к.<br/>квартира
                            </label>
                        </div>
                        <div class="search__tick-holder">
                            <label class="search__tick">
                                <input type="checkbox" {{ (is_array($t) && in_array(3, $t))?'checked':'' }}
                                       name="t" value="3"><span></span><br/>
                                3к.<br/>квартира
                            </label>
                        </div>
                        <div class="search__tick-holder">
                            <label class="search__tick">
                                <input type="checkbox" {{ (is_array($t) && in_array(4, $t))?'checked':'' }}
                                       name="t" value="4"><span></span><br/>
                                +4к.<br/>квартира
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            @if($amenities = \App\Highlight::orderBy('weight', 'desc')->get())
                @if($a = explode(',',app()->request->get('a')))@endif
                <div class="search__block only-full row">
                    <div class="medium-3 columns"><div class="title">Зручності</div></div>
                    <div class="medium-9 columns">
                        <div class="row">
                            <div class="medium-6 small-6 columns">
                                @if($hc = 1)@endif
                                @for($i = 0; $i < count($amenities); $i += 2)
                                    <label class="search__tick{{($i > 4 && $hc++)?' search__tick-hidden':''}}" title="{{ $amenities[$i]->description }}">
                                        <input type="checkbox" {{ (is_array($a) && in_array($amenities[$i]->id, $a))?'checked':'' }} name="a" value="{{ $amenities[$i]->id }}"/><span></span>{{ $amenities[$i]->title }}</label>
                                @endfor
                            </div>
                            <div class="medium-6 small-6 columns">
                                @for($i = 1; $i < count($amenities); $i += 2)
                                    <label class="search__tick{{($i > 5 && $hc++)?' search__tick-hidden':''}}" title="{{ $amenities[$i]->description }}">
                                        <input type="checkbox" {{ (is_array($a) && in_array($amenities[$i]->id, $a))?'checked':'' }} name="a" value="{{ $amenities[$i]->id }}"/><span></span>{{ $amenities[$i]->title }}</label>
                                @endfor
                            </div>
                            <div style="clear: both"></div>
                            <div class="search__show_hidden">
                                <span data-hide="сховати">+ {{ $hc - 1 }} зручностей</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="search__full_start">
            <div class="left">
                <div class="search__show_all">
                    <span class="show-filters">Всі фільтри</span>
                    <ul class="search__all_filters">
                        <li style="display: none;" data-type="%type%">%title% (%info%) <span class="cross"></span></li>
                    </ul>
                </div>
            </div>
            <div class="right">
                <div class="btn btn-cancel">Сховати</div>
                <div class="btn search-submit-btn" async-action="notice/search">Шукати</div>
            </div>
        </div>
    </div>
</div>