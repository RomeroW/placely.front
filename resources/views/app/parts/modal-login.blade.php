<div class="modal__holder modal__auth">
    <div class="modal__head">
        <span>Авторизація</span>
        <div class="cross"></div>
    </div>
    <div class="modal__body">
        <div class="auth__modal_description">
            Ми об’єднуємо людей без посередників, і ми віримо
            в те, що авторизація через соціальні мережі сприятиме
            максимальній прозорості
        </div>
    </div>
    <div class="modal__footer">
        <a class="btn__social-vk-big" href="/auth/vk">ВКонтакте</a>
        <a class="btn__social-fb-big" href="/auth/fb">Facebook</a>
    </div>
</div>