@if($price = app()->request->get('p'))@endif
<div class="search__modal search__modal-price" id="price">
    <div class="sp_item_holder">
        @foreach([
            ':3000' => 'до 3000 грн',
            ':7000' => 'до 7000 грн',
            ':12000' => 'до 12000 грн',
            ':15000' => 'до 15000 грн',
        ] as $data => $label)
        <div class="sp_item {{ ($price && $price == $data)?'active':'' }}" data-price="{{ $data }}">{{ $label }}</div>
        @endforeach
    </div>
    <div class="sp_diapason_holder">
        @if($price = explode(':', $price))@endif
        <input type="text" data-price-input="from" placeholder="від" value="{{ (isset($price[0]) && $price[0])?$price[0]:'' }}"/>
            &mdash;
            <input data-price-input="to" type="text" id="price-to" value="{{ (isset($price[1]) && $price[1])?$price[1]:'' }}" placeholder="до"/>
    </div>
</div>