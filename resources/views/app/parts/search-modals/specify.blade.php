{{--@if($price = app()->request->get('p'))@endif--}}
<div class="search__modal search__modal-specify" id="specify">
    <div class="medium-5 columns">
        <div class="sp_item_header">Тип житла</div>
        <div class="sp_item_holder">
            <div class="sp_item" data-t data-search-s="t:0">кімната</div>
            <div class="sp_item" data-t data-search-s="t:1">1-кімнатна</div>
            <div class="sp_item" data-t data-search-s="t:2">2-кімнатна</div>
            <div class="sp_item" data-t data-search-s="t:3">3-кімнатна</div>
            <div class="sp_item" data-t data-search-s="t:4">4+ кімнатна</div>
        </div>
    </div>
    <div class="medium-7 columns">
        @if($amenities = \App\Highlight::orderBy('weight', 'desc')->limit(5)->get())
            <div class="sp_item_header">Зручності</div>
            <div class="sp_item_holder">
                @foreach($amenities as $amenity)
                <div class="sp_item" data-a data-search-s="a:{{ $amenity->id }}">{{ $amenity->title }}</div>
                @endforeach
            </div>
        @endif
    </div>
</div>