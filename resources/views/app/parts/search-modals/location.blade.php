@if($stations = \App\MetroStation::prepareForSearch())@endif @if($districts = \App\District::prepareForSearch())@endif @if($m = explode(',',app()->request->get('m')))@endif @if($d = explode(',',app()->request->get('d')))@endif
<div class="search__modal search__modal-location" id="location">
    <ul class="sl__tabs active">
        <li class="active" data-s-target="metro">Метро</li>
        <li data-s-target="district">Район</li>
    </ul>
    <ul class="sl__tabs sl__tabs-small active" data-s-listen="metro">
        @foreach($stations['branches'] as $branchType => $branchName)
            <li {!! (array_keys($stations['branches'])[0] == $branchType)? 'class="active"' : '' !!} data-s-target="metro-{{ $branchType }}">
                <div class="sl__metro-root-{{ $branchType }}">{{ $branchName }}</div></li>
        @endforeach
    </ul>
    <ul class="sl__tabs sl__tabs-small" data-s-listen="district">
        @foreach($districts['types'] as $dType => $dName)
            <li {!! (array_keys($districts['types'])[0] == $dType)? 'class="active"' : '' !!} data-s-target="district-{{ $dType }}">{{ $dName }}</li>
        @endforeach
    </ul>
    <ul class="sl__frames">
        <li class="active" data-s-listen="metro">
            @foreach($stations['stations'] as $sType => $sItems)
                <div class="sl__metro_holder-{{ $sType }}{{ (array_keys($stations['stations'])[0] == $sType)? ' active' : '' }}" data-s-listen="metro-{{ $sType }}">
                    @foreach($sItems as $mId => $mTitle)
                        <div class="sl__metro-child {{ (is_array($m) && in_array($mId, $m))?'active':'' }}" data-m data-search="m:{{ $mId }}"> {{ $mTitle }}</div>
                    @endforeach
                </div>
            @endforeach
        </li>
        <li data-s-listen="district">
            @foreach($districts['items'] as $dType => $dItems)
                <div class="sl__district-holder{{ (array_keys($districts['types'])[0] == $dType)? ' active' : '' }}" data-s-listen="district-{{ $dType }}">
                    @foreach($dItems as $dId => $dTitle)
                        <div class="sl__district-child {{ (is_array($d) && in_array($dId, $d))?'active':'' }}" data-d data-search="d:{{ $dId }}">{{ $dTitle }}</div>
                    @endforeach
                </div>
            @endforeach
        </li>
    </ul>
</div>