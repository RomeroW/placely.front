<div class="resp-menu mobile-only"
@if (app()->request->route() && app()->request->route()->getName() == 'index')
    style="z-index: 103"
        @endif
        >
    <div class="resp-menu-holder" style="position: absolute; left: -100vw;">
        <ul>
            <li class="auth__resp">
                <div class="auth__resp_holder">
                    @if(Auth::check())
                        <div class="auth__holder">
                            <div class="auth__logo"><img src="{{ Auth::user()->icon }}"/></div>
                            <div class="auth__body">
                                <div class="auth__info">
                                    <div class="auth__name">{{ Auth::user()->name }}</div>
                                    <div class="auth__logout-holder">
                                        <a class="btn__logout" href="/logout/{{ csrf_token() }}"></a>
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both"></div>
                        </div>
                    @endif
                    <a class="btn__resp_notice-add" href="/create">Додати оголошення</a>
                </div>
            </li>
            @if($notices = App\User::getAuthNoticesCount())
                <li><a href="/my"><div class="l">Мої оголошення</div> <div class="rc">{{ $notices }}</div><div style="clear: both"></div></a></li>
            @endif
            @if(Auth::check())
                <li><a href="{{ url('settings') }}">Налаштування</a></li>
            @endif
            <li><a href="{{ url('/blog') }}">Блог</a></li>
            <li><a href="{{ url('/blog/about-us') }}">Про нас</a></li>
            <li><a href="{{ url('/blog/how-it-works') }}">Як це працює</a></li>
            <li class="sn_links">
                <a href="https://vk.com/placely" target="_blank"><i class="icon-vkontakte"></i></a>
                <a href="https://www.facebook.com/placely" target="_blank"><i class="icon-facebook-official"></i></a>
            </li>
        </ul>
    </div>
</div>