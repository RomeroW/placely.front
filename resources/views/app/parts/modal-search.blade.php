<div class="modal__holder modal__search">
    <div class="modal__head">
        <span>Пошук</span>
        <div class="cross"></div>
    </div>
    <div class="modal__body">
        <form method="get" action="search" async-action="notice/search">
            <div class="search__input_holder search__input_holder-location">
                <input type="hidden" name="location"/>
                <div class="search__input search__input-district trigger-location">
                    <span class="icon-location"><span class="placeholder" data-placeholder="Локація">Локація</span></span>
                </div>
            </div>

            <div class="search__input_holder search__input_holder-price">
                <div class="search__input search__input-price trigger-price">
                    <span class="icon-price"><span class="placeholder" data-placeholder="Ціна">Ціна</span></span>
                </div>
            </div>

            <div class="search__input_holder search__input_holder-specify">
                <div class="search__input search__input-specify trigger-specify">
                    <span class="icon-blank"><span class="placeholder" data-placeholder="Уточнити">Уточнити</span></span>
                </div>
            </div>

            {{--<input type="submit" id="search-submit" value="Шукати"/>--}}
        </form>
    </div>
    <div class="modal__footer">
        <button class="btn__fine search-submit-btn">Шукати</button>
    </div>
</div>