<div class="contact @if($notice->attachments->count()) contact-to_gallery @endif">
    <div class="contact-box">
        <div class="cross"></div>
        <div class="contact__title">
            ₴{{ $notice->price }}<span>/</span><span class="small">міс.</span>
        </div>

        @if($notice->belongsToAuthorized())
            <div class="contact__status">
            <span class="status status-{{$notice->status}}">
                {{ trans('notice.status.' . $notice->status . '.title') }}
            </span>
            </div>
            @if($notice->isModerating())
                <div class="info">
                    <div class="row info__row">
                        <div class="info__lead">Оголошення на модерації</div>
                        <div class="info__desc">Як тільки ми впевнимося, що із введеними даними все добре, люди побачать його</div>
                    </div>
                </div>
            @elseif($notice->isUnauthorized())
                <div class="info">
                    <div class="row info__row-incognito">
                        <div class="info__lead">Схоже Ви не авторизовані...</div>
                        <div class="info__desc">Авторизуйтеся, щоби люди знайшли автора оголошення</div>
                        <div class="info__buttons">
                            <a class="btn__social-vk" href="/auth/vk">ВКонтакте</a>
                            <a class="btn__social-fb" href="/auth/fb">Facebook</a>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        @if($user = $notice->user)
            <div class="person__holder">
                <div class="person__avatar animated">
                    <img src="{{ $user->icon }}"/>
                </div>
                <div class="person__name">{{ $user->name }}</div>
            </div>

            @if(!$notice->belongsToAuthorized())
                @if($notice->status == \App\Notice::STATUS_PUBLISHED)
                    <div class="contact__list">
                        @foreach($user->getAvailableContacts() as $contact)
                            <div class="item">
                                <div class="title">{{ $contact->title }}</div>
                                <div class="value value-hidden" data-hidden-contact="{{ $contact->type }}">{{ $contact->mask }}</div>
                            </div>
                        @endforeach
                    </div>

                    <div class="contact__show" data-csrf="{{ csrf_token() }}" data-url="{{ route('noticecontacts', ['id' => $notice->id]) }}">
                        Показати контакти
                    </div>
                @elseif(in_array($notice->status, [\App\Notice::STATUS_REPENDING, \App\Notice::STATUS_BOOKED]))
                    <div class="current_state">
                        На даний момент цей об’єкт зданий
                    </div>
                @endif
            @else
                @if($user->hasNoContacts())
                    <div class="add__contacts">
                        <div class="description">
                            не вказано контактів окрім соц. мережі
                        </div>
                        <div class="contacts__link"><a href="{{ url('settings') }}">додати контакти</a></div>
                    </div>
                @endif
                @if(in_array($notice->status, [\App\Notice::STATUS_PUBLISHED, \App\Notice::STATUS_BOOKED]))
                    <div class="notice__statistic">
                        <div class="notice__views">
                            <div class="view__stat">
                                <div class="stat__value">{{ $notice->views }}</div>
                                <div class="stat__title">Переглядів</div>
                            </div>
                            <div class="view__stat">
                                <div class="stat__value">{{ $notice->contact_views }}</div>
                                <div class="stat__title">Відкриття контактів</div>
                            </div>
                        </div>
                        @if($notice->status == \App\Notice::STATUS_PUBLISHED)
                            <div class="object__booked" data-csrf="{{ csrf_token() }}" data-url="{{ route('noticebooked', ['id' => $notice->id]) }}">
                                Об’єкт здано
                            </div>
                            <div class="description">
                                Натисніть, коли здасте об’єкт. Оголошення відразу ж прибереться зі списку публікованих
                            </div>
                        @elseif($notice->status == \App\Notice::STATUS_BOOKED)
                            <div class="object__booked" data-csrf="{{ csrf_token() }}" data-url="{{ route('noticepublish', ['id' => $notice->id]) }}">
                                Публікувати знову
                            </div>
                            <div class="description">
                                Оголошення відправляється на модерацію, лічильники обнуляються
                            </div>
                        @endif
                    </div>
                @endif
            @endif
        @endif
    </div>

    <div class="fb-page-holder" style="opacity: 0; width: 100%">
        <div class="fb-page" data-href="https://www.facebook.com/placely/" data-small-header="false" style="margin-top: 2em"
             data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
            <div class="fb-xfbml-parse-ignore">
                <blockquote cite="https://www.facebook.com/placely/"><a
                            href="https://www.facebook.com/placely/">placely.io - довгострокова оренда житла
                        в Києві без посередників</a></blockquote>
            </div>
        </div>
    </div>
</div>