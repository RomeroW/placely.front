<header class="wrapper fixed animated @yield('specialHeaderClasses')">
    <div class="row">
        <div class="logo__holder small-4 tab-3 medium-7 columns">
            <div class="medium-3 small-padding columns">
                <a href="/"><div class="logo"></div></a>
            </div>
            <nav class="tab-collapsed column-small-collapse medium-9 columns">
                <ul>
                    <li><a href="{{ url('/blog') }}">Блог</a></li>
                    <li><a href="{{ url('/blog/about-us') }}">Про нас</a></li>
                    <li><a href="{{ url('/blog/how-it-works') }}">Як це працює</a></li>
                    <li class="sn_links">
                        <a href="https://vk.com/placely" target="_blank"><i class="icon-vkontakte"></i></a>
                        <a href="https://www.facebook.com/placely" target="_blank"><i class="icon-facebook-official"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
        @if(!Auth::check())
            <div class="auth__empty small-padding small-4 tab-7 medium-5 columns">
                <div id="mobile-menu" class="nav__btn_open_menu mobile-only"></div>

                <div class="btn__notice-add right mobile-collapsed"><a href="/create">Додати оголошення</a></div>
                <div id="open-modal-auth" class="auth__btn_open_modal">
                    <div class="icon"></div>
                    <div class="text">Увійти</div>
                </div>

                @if($notices = App\User::getGuestNoticesCount())
                    <nav class="nav__notifications">
                        <ul class="mobile-collapsed">
                            <li><a href="/my">Мої оголошення</a><span>{{ $notices }}</span></li>
                        </ul>
                    </nav>
                @endif
            </div>
        @else
            <div class="small-padding small-4 tab-7 medium-5 columns right-elements">
                <div class="auth__empty mobile-only">
                    <div id="mobile-menu" class="nav__btn_open_menu"></div>
                </div>
                <div class="auth__holder mobile-collapsed">
                    <div class="auth__logo">
                        <a class="settings-link" href="{{ url('settings') }}">
                            <i class="icon-cog"></i>
                        </a>
                        <img src="{{ Auth::user()->icon }}"/>
                    </div>
                    <div class="auth__body">
                        <div class="auth__info">
                            <div class="auth__name" title="Зміни">{{ Auth::user()->name }}</div>
                            <div class="auth__logout-holder">
                                <a class="btn__logout" href="/logout/{{ csrf_token() }}"></a>
                            </div>
                        </div>
                        <div class="btn__notice-add"><a href="/create">Додати оголошення</a></div>
                    </div>
                </div>
                <nav class="nav__notifications">
                    <ul class="mobile-collapsed">
                        <li><a href="/my">Мої оголошення</a>@if($notices = App\User::getAuthNoticesCount())<span>{{ $notices }}</span>@endif </li>
                    </ul>
                </nav>
            </div>
        @endif
    </div>
</header>