<div class="row my__landing">
    <div class="medium-4 columns my__arg">
        <div class="heading">Просте керування</div>
        <div class="text">Здали місце? Одна кнопка - і більше Вас не турбують</div>
        <div class="image">
            <img src="{{ url("/images/control.png") }}"/>
        </div>
    </div>
    <div class="medium-4 columns my__arg">
        <div class="heading">Статистика</div>
        <div class="text">Слідкуйте за популярністю свого оголошення</div>
        <div class="image">
            <img src="{{ url("/images/statistic.png") }}"/>
        </div>
    </div>
    <div class="medium-4 columns my__arg">
        <div class="heading">Просте повернення</div>
        <div class="text">Хочете повернути оголошення? Ми завжди раді :)</div>
        <div class="image">
            <img src="{{ url("/images/getback.png") }}"/>
        </div>
    </div>
</div>