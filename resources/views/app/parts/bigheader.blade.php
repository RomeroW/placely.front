<div class="header__holder">
    @include('app.parts.newheader')
</div>

<div class="header__land">
    <div class="header__image" style="background-image: url(https://i.onthe.io/smngoz647os3pp538.be2b0ccc.jpg)"></div>
    <div class="header__mask">
        <div class="row header__content">
            @include('app.parts.short-search')
        </div>
    </div>
</div>