<!doctype html>
<html lang="en" prefix="og: http://ogp.me/ns#
    fb: http://ogp.me/ns/fb#
    @if (app()->request->route() && app()->request->route()->getName() == 'one')
        product: http://ogp.me/ns/product#">
    @else
         website: http://ogp.me/ns/website#">
    @endif
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <title>@yield('title') Placely @yield('postTitle')</title>
    <link rel="stylesheet" href="{{ rev_asset('css/project.min.css') }}" />
    <link rel='shortcut icon' type='image/x-icon' href='{{ url('favicon.ico') }}' />
    @if(!env('ALLOW_ROBOTS'))<meta name="robots" content="noindex">
    @endif
    @yield('head')
</head>
<body data-page="@yield('page')">
<div id="fb-root"></div>

<div class="fb__popup" style="display: none; opacity: 0; right: -300px;">
    <div class="popup__main">
        <div class="cross">×</div>
        <strong>Placely</strong>
        <span>Натисніть "Подобається", щоби не пропустити оголошення у Facebook</span>
    </div>
    <div class="popup__action">
        <div class="fb-like" data-href="https://www.facebook.com/placely/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
    </div>
</div>

@if (app()->request->route() && app()->request->route()->getName() == 'index')
    @include('app.parts.bigheader')
@else
    @include('app.parts.header')
@endif

@include('app.parts.menu')

@yield('content')

<footer>
    <div class="row">
        <div class="medium-3 columns">
            <div class="footer__title">&nbsp;</div>
            <div class="footer__data">
                <a href="{{ url('/blog') }}">Блог</a><br/>
                <a href="{{ url('/blog/about-us') }}">Про нас</a><br/>
                <a href="{{ url('/blog/how-it-works') }}">Як це працює</a><br/>
                <a href="{{ url('/blog/copyright-holder') }}">Правовласниками</a><br/>
                <a href="{{ url('create') }}">Додати оголошення</a><br/>
            </div>
        </div>
        <div class="medium-3 columns">
            <div class="footer__title">
                Партнери
            </div>
            <div class="footer__data">
                <a href="https://vk.com/arenda_bez_brokera" target="_blank" class="p" data-trgt="dd-vk"><i class="icon-vkontakte" style="font-size: .75em;opacity: .75;"></i> Оренда без посередників</a></br>
                <a href="https://www.facebook.com/groups/arenda.bez.brokera" target="_blank" class="p" data-trgt="dd-fb"><i class="icon-facebook-official" style="font-size: .75em;opacity: .75;"></i> Оренда без посередників</a>
            </div>
        </div>
        <div class="medium-3 columns">
            <div class="footer__title">
                Партнери
            </div>
            <div class="footer__data">
                <a href="https://www.facebook.com/groups/446424965482548" target="_blank" class="p" data-trgt="fv-vk"><i class="icon-facebook-official" style="font-size: .75em;opacity: .75;"></i>Оренда в Києві без посередників</a></br>
            </div>
        </div>
        <div class="medium-3 columns">
            <div class="footer__title">
                Ми в соціальних мережах
            </div>
            <div class="footer__data">
                <a href="https://vk.com/placely" target="_blank"><i class="icon-vkontakte" style="font-size: 1.5em"></i></a>
                <a href="https://www.facebook.com/placely" target="_blank"><i class="icon-facebook-official" style="font-size: 1.5em"></i></a>
            </div>
            <div class="footer__title">
                Контакти
            </div>
            <div class="footer__data">
                support: <a href="mailto:support@placely.io">support@placely.io</a><br/>
                dev: <a href="mailto:dev@placely.io">dev@placely.io</a><br/>
            </div>
        </div>
    </div>
</footer>

<!-- SCRIPTS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
@yield('footer-scripts')
<script src="{{rev_asset('js/app.min.js')}}"></script>
</body>
</html>
