@extends('app.layout')

@section('title', _('такої сторінки не існує'))

@section('specialHeaderClasses', 'overmap')

@section('content')
    <div class="modal" style="display: none">
        @include('app.parts.modal-login')
    </div>

    <div class="fixed__holder-new"></div>

    <div class="row nf__row">
        <div class="nf__status">404</div>
        <div class="nf__info">Шкода, але за таким запитом у нас нічого немає :(</div>

        @if($notices = App\Notice::nQuery())
            <div class="nf__data">
                <div class="data__title">Пропонуємо глянути</div>
                <div class="data__notices">
                    @each('app.notice.feed', $notices, 'notice')
                    <div style="clear: both"></div>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('footer-scripts')
    <script src="{{asset('js/app.min.js')}}"></script>
@endsection
