<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Romalytvynenko\Blog\database\migrations\CreatePostsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        parent::up();

        Schema::table('posts', function (Blueprint $table) {
            $table->integer('views')->after('user_id');
            $table->text('description')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        parent::down();
    }
}
