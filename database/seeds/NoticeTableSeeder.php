<?php

use App\Notice;
use Illuminate\Database\Seeder;

class NoticeTableSeeder extends Seeder
{
    private $attachments = [
        '2ewmmj22amik8adfm',
        '2ewmmj34kqltec3gag',
        '2ewmmj2f4sbkmh29ig',
        '2ewmmj4pa2lb1jfva',
        '2ewmmj72e6ro2l6v3',
        '2ewmmj226vkkvei4lg',
        '2ewmmj23h89mm5farg',
        '2ewmmj5o17je8q0m7'
    ];

    private $users = [13, 14, 17, 18];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 10; $i++) {
            $this->createNotice($faker);
        }

        echo "\n";

    }

    /**
     * Generate random notice
     *
     * @param \Faker\Generator $faker
     */
    private function createNotice(Faker\Generator $faker)
    {
        // user id
        $randUserId = $this->users[mt_rand(0, count($this->users) - 1)];

        // description
        $randDescription = [];
        for($i = 0; $i < mt_rand(1, 5); $i++) {
            $randDescription[] = implode(" ", $faker->paragraphs($faker->numberBetween(3, 8)));
        };
        $randDescription = implode("\n", $randDescription);

        // price
        $randPrice = $faker->numberBetween(30, 150) * 100;

        // street, building
        $randStreetId = $faker->numberBetween(1, 1551);

        $buildingPrefixes = ['', '', '', '', '', '', '', '', '', '', '', '', 'А', 'Б', 'В', 'Г', 'Д'];
        $randBuilding = $faker->numberBetween(1, 200) . $buildingPrefixes[mt_rand(0, count($buildingPrefixes) - 1)];

        // lat, lng
        $randLat = $faker->randomFloat(5, 50.3769, 50.5269);
        $randLng = $faker->randomFloat(5, 30.3394, 30.6511);

        // type
        $types = [
            'apartment',
            'apartment',
            'apartment',
            'room'
        ];
        $typeId = mt_rand(0, 3);

        $randType = $types[$typeId];

        // size
        // rooms
        $randTotalSpace = $faker->numberBetween(8, 20) * 10;
        $randEffectiveSpace = 0;
        $randLivingSpace = 0;
        $randRooms = 0;

        if($randType == 'apartment') {
            $randEffectiveSpace = $faker->numberBetween(8, 20) * 10;
            $randLivingSpace = $faker->numberBetween(8, 20) * 10;
            $randRooms = $faker->numberBetween(1, 5);
        }

        // metro
        $randMetros = [];
        $count = mt_rand(0, 2);
        for($i = 0; $i < $count; $i++) {
            $randMetro = [
                'id' => $faker->numberBetween(521, 572),
                'foot' => $faker->numberBetween(2, 15),
                'transport' => $faker->numberBetween(2, 15)
            ];
            $randMetros[] = $randMetro;
        }

        // highlight
        $randHighlights = [];
        $count = mt_rand(1, 10);
        for($i = 0; $i < $count; $i++) {
            $randHighlights[] = $faker->numberBetween(5, 38);
        }

        // districts
        $randDistricts = [];
        $scenario = mt_rand(0, 2);
        switch($scenario) {
            case 1:
                $randDistricts[] = $faker->numberBetween(134, 143);
                break;
            case 2:
                $randDistricts[] = $faker->numberBetween(144, 264);
                break;
            default:
                break;
        }

        // attachments
        $randAttachments = [];
        if(mt_rand(0, 5) > 0) {
            $arr = array_rand($this->attachments, mt_rand(1, 5));
            $arr = is_array($arr)? $arr : [$arr];

            foreach($arr as $attachmentKey) {
                $randAttachments[] = $this->attachments[$attachmentKey];
            }
        }

        $brief = '';
        if(mt_rand(0, 2) > 0) {
            $brief = $faker->text(140);
        }

        /**
         * STORE GENERATED DATA TO DATABASE
         */

        // notice
        $notice = new Notice([
            'user_id' => $randUserId,
            'description' => $randDescription,
            'total_space' => $randTotalSpace,
            'effective_space' => $randEffectiveSpace,
            'living_space' => $randLivingSpace,
            'map_lat' => $randLat,
            'map_lng' => $randLng,
            'brief' => $brief,
            'rooms' => $randRooms,
            'price' => $randPrice,
            'type' => $randType,
            'status' => Notice::STATUS_PUBLISHED,
            'street_id' => $randStreetId,
            'building' => $randBuilding
        ]);
        $notice->save();

        // metro
        foreach ($randMetros as $metro) {
            $metroRelation = new \App\MetroNotice([
                'metro_id' => $metro['id'],
                'notice_id' => $notice->id,
                'foot' => $metro['foot'],
                'transport' => $metro['transport']
            ]);
            $metroRelation->save();
        }

        // districts
        foreach($randDistricts as $districtId) {
            $districtRelation = new \App\DistrictNotice([
                'notice_id' => $notice->id,
                'district_id' => $districtId
            ]);
            $districtRelation->save();
        }

        // highlights
        foreach ($randHighlights as $highlightId) {
            $highlightRelation = new \App\HighlightNotice([
                'notice_id' => $notice->id,
                'higlight_id' => $highlightId
            ]);
            $highlightRelation->save();
        }

        // attachments
        $attachmentIds = [];
        foreach ($randAttachments as $attachmentHash) {
            $attachmentRelation = new \App\Attachment([
                'notice_id' => $notice->id,
                'hash' => $attachmentHash
            ]);
            $attachmentRelation->save();
            $attachmentIds[] = $attachmentRelation->id;
        }
        if(rand(0,6) && count($attachmentIds)) {
            $notice->main_attachment_id = $attachmentIds[array_rand($attachmentIds)];
            $notice->save();
        }

        echo '.';
    }
}
