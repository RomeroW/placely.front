<?php

use Illuminate\Database\Seeder;

class StreetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../streets.json'), true);

        foreach ($data as $street) {
            DB::table('streets')->insert([
                'name' => $street['name'],
                'real_name' => $street['real_name']
            ]);
        }
    }
}
