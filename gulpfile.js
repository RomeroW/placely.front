(function() {
    "use strict";

    var gulp = require('gulp'),
        less = require('gulp-less'),
        concat = require('gulp-concat'),
        uglify = require('gulp-uglify'),
        rename = require('gulp-rename'),
        cssshrink = require('gulp-cssshrink'),
        image = require('gulp-image'),
        sass = require('gulp-sass'),
        rev = require('gulp-rev');

    var assetsFolder = 'resources/assets',
        publicFolder = 'public';

    /*
     * Compile foundation (runs only once on gulp start)
     */
    gulp.task('sass', function() {
        return gulp.src(assetsFolder + '/sass/**/*.scss')
            .pipe(sass())
            .pipe(concat('foundation.css'))
            .pipe(cssshrink())
            .pipe(gulp.dest(assetsFolder + '/build/css'));
    });

    gulp.task('less', function(){
        return gulp.src(assetsFolder + '/less/*.less')
            .pipe(less())
            .pipe(concat('project.css'))
            .pipe(cssshrink())
            .pipe(gulp.dest(assetsFolder + '/build/css'));
    });

    /*
     * Concat css provided by scss and less tasks
     */
    gulp.task('initCssAssemble', ['sass', 'less'], function() {
        return gulp.src(assetsFolder + '/build/css/*.css')
            .pipe(concat('project.min.css'))
            .pipe(cssshrink())
            .pipe(gulp.dest('public/css'))
    });

    /*
     * Concat css provided by scss and less tasks
     */
    gulp.task('incrementLessAssemble', ['less'], function() {
        return gulp.src(assetsFolder + '/build/css/*.css')
            .pipe(concat('project.min.css'))
            .pipe(cssshrink())
            .pipe(gulp.dest('public/css'))
    });

    /*
     * Concat css provided by scss and less tasks
     */
    gulp.task('incrementSassAssemble', ['sass'], function() {
        return gulp.src(assetsFolder + '/build/css/*.css')
            .pipe(concat('project.min.css'))
            .pipe(cssshrink())
            .pipe(gulp.dest('public/css'))
    });

    /*
     * Assemble js
     */
    gulp.task('js', function() {
        return gulp.src([

            // We load dropzone old way
            '!' + assetsFolder + '/js/vendor/dropzone.js',

            assetsFolder + '/js/vendor/**/*.js',
            assetsFolder + '/js/plugins/*.js',

            // Document ready wrapper starts here
            assetsFolder + '/js/helper/app-head.js.template',

            assetsFolder + '/js/modules/**/*.js',

            assetsFolder + '/js/utils.js',
            assetsFolder + '/js/pages.js',
            assetsFolder + '/js/application.js',

            // Document ready wrapper ends here
            assetsFolder + '/js/helper/app-end.js.template'

        ]).pipe(concat('app.min.js'))
            .pipe(uglify())
            //.pipe(rev())
            .pipe(gulp.dest('public/js'));
            //.pipe(rev.manifest())
            //.pipe(gulp.dest(assetsFolder));
    });

    /*
     * Compress all images
     */
    gulp.task('image', function () {
        gulp.src(assetsFolder + '/images/*')
            .pipe(image())
            .pipe(gulp.dest('public/images'));
    });

    gulp.task('revision', function(){
        // revision of app.min.js
        // @todo: revision all minified files without hash
        gulp.src([publicFolder + '/js/app.min.js',
            publicFolder + '/css/project.min.css'],
            {base: publicFolder})
            .pipe(gulp.dest(publicFolder))
            .pipe(rev())
            .pipe(gulp.dest(publicFolder))
            .pipe(rev.manifest())
            .pipe(gulp.dest(assetsFolder));
    });

    gulp.task('watch', function() {
        // Watch .less files
        gulp.watch(assetsFolder + '/less/**/*.less', ['incrementLessAssemble']);

        // Watch .sass files
        gulp.watch(assetsFolder + '/sass/**/*.scss', ['incrementSassAssemble']);

        // Watch .js files
        gulp.watch(assetsFolder + '/js/**/*.js', ['js']);
    });

    gulp.task('default', ['initCssAssemble', 'js', 'watch']);
})();