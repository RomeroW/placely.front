# index page (/)

## Without any optimizing

### not authorized
0.46	select * from `notices` where `status` = published order by `id` desc limit 5
438.32	select count(`id`) as aggregate from `notices` where `status` = published
0.35	select * from `streets` where `streets`.`id` = 380 and `streets`.`id` is not null limit 1
0.87	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101061
70.87	select * from `attachments` where `attachments`.`notice_id` = 101061 and `attachments`.`notice_id` is not null
0.19	select * from `streets` where `streets`.`id` = 471 and `streets`.`id` is not null limit 1
0.2	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101060
51.09	select * from `attachments` where `attachments`.`notice_id` = 101060 and `attachments`.`notice_id` is not null
0.09	select * from `streets` where `streets`.`id` = 858 and `streets`.`id` is not null limit 1
0.23	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101059
51.47	select * from `attachments` where `attachments`.`notice_id` = 101059 and `attachments`.`notice_id` is not null
0.11	select * from `streets` where `streets`.`id` = 668 and `streets`.`id` is not null limit 1
0.19	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101058
50.78	select * from `attachments` where `attachments`.`notice_id` = 101058 and `attachments`.`notice_id` is not null
0.21	select * from `streets` where `streets`.`id` = 265 and `streets`.`id` is not null limit 1
0.88	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101057
50.17	select * from `attachments` where `attachments`.`notice_id` = 101057 and `attachments`.`notice_id` is not null
---
716.48


### authorized
0.35	select * from `notices` where `status` = published order by `id` desc limit 5
411.13	select count(`id`) as aggregate from `notices` where `status` = published
0.38	select * from `streets` where `streets`.`id` = 380 and `streets`.`id` is not null limit 1
0.45	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101061
72.14	select * from `attachments` where `attachments`.`notice_id` = 101061 and `attachments`.`notice_id` is not null
0.22	select * from `streets` where `streets`.`id` = 471 and `streets`.`id` is not null limit 1
0.21	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101060
51.22	select * from `attachments` where `attachments`.`notice_id` = 101060 and `attachments`.`notice_id` is not null
0.08	select * from `streets` where `streets`.`id` = 858 and `streets`.`id` is not null limit 1
0.21	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101059
50.02	select * from `attachments` where `attachments`.`notice_id` = 101059 and `attachments`.`notice_id` is not null
0.19	select * from `streets` where `streets`.`id` = 668 and `streets`.`id` is not null limit 1
0.3	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101058
50.45	select * from `attachments` where `attachments`.`notice_id` = 101058 and `attachments`.`notice_id` is not null
0.18	select * from `streets` where `streets`.`id` = 265 and `streets`.`id` is not null limit 1
0.24	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` = 101057
51.39	select * from `attachments` where `attachments`.`notice_id` = 101057 and `attachments`.`notice_id` is not null
0.35	select * from `users` where `users`.`id` = 18 limit 1
428	select count(*) as aggregate from `notices` where `user_id` = 18
---
1117.51

## Eager Loading

### not authorized
0.41	select * from `notices` where `status` = published order by `id` desc limit 5
0.48	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` in (101061, 101060, 101059, 101058, 101057)
0.07	select * from `streets` where `streets`.`id` in (380, 471, 858, 668, 265)
60.26	select * from `attachments` where `attachments`.`notice_id` in (101061, 101060, 101059, 101058, 101057)
418.87	select count(`id`) as aggregate from `notices` where `status` = published
---
480.09

### authorized
0.36	select * from `notices` where `status` = published order by `id` desc limit 5
0.49	select `metro`.*, `metro_notices`.`notice_id` as `pivot_notice_id`, `metro_notices`.`metro_id` as `pivot_metro_id`, `metro_notices`.`foot` as `pivot_foot`, `metro_notices`.`transport` as `pivot_transport` from `metro` inner join `metro_notices` on `metro`.`id` = `metro_notices`.`metro_id` where `metro_notices`.`notice_id` in (101061, 101060, 101059, 101058, 101057)
0.26	select * from `streets` where `streets`.`id` in (380, 471, 858, 668, 265)
61.17	select * from `attachments` where `attachments`.`notice_id` in (101061, 101060, 101059, 101058, 101057)
419.35	select count(`id`) as aggregate from `notices` where `status` = published
0.27	select * from `users` where `users`.`id` = 18 limit 1
407.85	select count(*) as aggregate from `notices` where `user_id` = 18
---
889.75

#### summary
old -> eager -> indexes
not authorized: 716.48  -> 480.09 -> ...
    authorized: 1117.51 -> 889.75 -> ...


# one (/{id})
