# Placely Web Application
(@todo: make it all in english)

## First Install
1. Clone this repository.
2. Configure your web server to look to the project's folder.
3. Do ```composer install``` in project's folder.
4. Import the ```database/placely.sql``` to your database.
5. Create .env using .env.example.
6. Make ```storage``` directory writable by server.
7. Make sure that your APP_KEY value in .env is 32 symbols length.
...
10. PROFIT!1

Don't  forget to set APP_DEBUG=false and APP_ENV=production in production environment

## Deploying
For deploying use bash script ```deploy-it```. So, when you commit anything deploy it using next bash command:
```sh
./deploy-it
```
It will pulls from master and generate revision for static.

## Using PHP built-in web-server
For a quick test without the real web server you should use php built-in web server. Run: 
```sh
php artisan serve
```
To specify the port use the following command:
```sh
php artisan serve --port=8080
```

## Cron jobs
```
* * * * * php artisan schedule:run >> /dev/null 2>&1
```

## Front-end automation
We use gulp (http://gulpjs.com/) to compose all front end assets together.

All assets are located in ```resources/assets``` folder. 
- ```sass/```: "Foundation" sources;
- ```less/```: application's styles;
- ```js/```: application's javascript; 

```gulpfile.js``` which is located in application's root contains next tasks:
- *default*: default task, runs next tasks: *initCssAssemble*, *js*, *watch*
- *sass*: compile all ```**/*.sass``` files into ```foundation.css``` and store it to the temp folder.
- *less*: compile all ```**/*.less``` files into ```project.css``` and store it to the temp folder.
- ***initCssAssemble***: concates all ```*.css``` files from the temp folder, after *sass* and *less* tasks finished, and store it to ```public/css/project.min.css```.
- *incrementLessAssemble*: concat all ```*.css``` files from the temp folder, after *less* task finished, and store it to ```public/css/project.min.css```.
- *incrementSassAssemble*: concat all ```*.css``` files from the temp folder, after *sass* task finished, and store it to ```public/css/project.min.css```.
- ***js***: concat and uglify js from ```js``` folder, and store it to ```public/js/app.min.js``` file.
- ***watch***: watching ```sass```, ```less``` and ```js``` files changes, and if the file change runs appropriate task: *incrementSassAssemble*, *incrementLessAssemble*, *js*.

## Front-end architecture
Веб інтерфейс placely складається із сторінко різного типу. І для деяких з них є унікальний js код, який треба виконати. Сторінки ідентифікуються за допомогою атрибуту data-page в елементу body.

Для всього JS існує вхідна точка - ```application.js```. Там визначається який код запустити для сторінки (body[data-page] атрибут). Сам код, що виконуватиметься для сторінки знаходиться в ```pages.js```. Код сторінки це властивості глобального об’єку Pages. В кожної з них має бути метод init(). В ньому і будується вже вся логіка.

В проекті також є плагіни jquery (кастомна карта, автокомпліт, і т.д.) - вони теж винесені в окремі файлики, і знаходяться в js/plugins.
 
Ну і в головному layout(html) підключений один скомпільований js файл (app.min.js), в якому знаходиться конкатенований і мініфікований js проекту.

### Passing data that shoudn't be displayed from frontend
(@todo)